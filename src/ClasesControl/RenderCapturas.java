package ClasesControl;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import ClasesEntidad.Captura;
import ClasesEntidad.Carpeta;

public class RenderCapturas extends DefaultTreeCellRenderer {

	   /**
	 * 
	 */
	
	private Color colortxt;
	
	private static final long serialVersionUID = 1L;
	
	public RenderCapturas(String colortxt){
		this.colortxt=Color.decode(colortxt);
	}

	@Override
	    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
	    boolean leaf, int row, boolean hasFocus) {
	        super.getTreeCellRendererComponent(tree, value, selected,expanded, leaf, row, hasFocus);
	        //altura de cada nodo
	        tree.setRowHeight(20);

	        setOpaque(true);     
	        //color de texto
	        setForeground( Color.black );
	        if( selected )
	            setForeground( Color.decode("#008080"));        
	        //-- Asigna iconos
	        // si value es la raiz
	        if ( tree.getModel().getRoot().equals( (DefaultMutableTreeNode) value ) ) {
	            //setIcon( ((Proyecto)((DefaultMutableTreeNode) value).getUserObject()).getIcon() );
	        } 
	        else if( ((DefaultMutableTreeNode) value).getUserObject() instanceof Carpeta) 
	        {
	        	setForeground(colortxt);
	        	if( selected )
		            setForeground( Color.decode("#008080")); 
	        	
	            setIcon( ((Carpeta)((DefaultMutableTreeNode) value).getUserObject()).getIcon() );            
	        }
	        else if( ((DefaultMutableTreeNode) value).getUserObject() instanceof Captura) 
	        {
	            setIcon( ((Captura)((DefaultMutableTreeNode) value).getUserObject()).getIcon() );            
	        }
	        
	        return this;
	}
	

}
