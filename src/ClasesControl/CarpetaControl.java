package ClasesControl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import ClasesEntidad.Grupo;
import ClasesEntidad.Medicion;
import ClasesEntidad.Proyecto;
import ClasesEntidad.Sesion;
import ClasesEntidad.Recursos.CapturaAudio;
import ClasesFrontera.EsysM;


public class CarpetaControl {

	private Proyecto proyecto;
	private DefaultMutableTreeNode root;
	private int s=-1, g=-1;
	private DefaultTreeModel modelo;

	public void CrearProyecto(String direccion) {
		proyecto=new Proyecto();
		proyecto.setNombre(direccion);
	}
	
	public Proyecto getProyecto() {
		return proyecto;
	}
	
	public void AgregarSesion(String nombre, String color) {
		Sesion sesion= new Sesion(nombre, color);
		proyecto.addSesion(sesion);
	}
	
	public Sesion getSesion(String id) {
		int pos= Buscar(id, proyecto.getSesiones());
		if (pos>-1) {
		return proyecto.getSesion(pos);}
		else {
			return null;
		}
	}
	
	public ArrayList<Sesion> getSesiones() {
		return proyecto.getSesiones();
	}
	
	public void AgregarGrupo(String nombre, Sesion sesion, String color) {
		Grupo grupo= new Grupo(nombre, color);
		sesion.addGrupo(grupo);
	}
	
	public Grupo getGrupo(String id,Sesion sesion) {
		if (sesion != null) {
			int pos = Buscar(id, sesion.getGrupos());
			if (pos == -1) {
				return null;
			} else {
				return sesion.getGrupo(pos);
			}
		} else {
			return null;
		}
	}
	
	public void AgregarMedicion(String nombre, Sesion sesion, Grupo grupo, String color, CapturaAudio[] dispositivo, char[] canal) {
			
		Medicion medicion= new Medicion(nombre,color, dispositivo, canal);
		grupo.addMedicion(medicion);
		
	}
	
	public Medicion getMedicion(String id, Sesion sesion,Grupo grupo) {		
		if (sesion!=null && grupo!=null) {
		int posG= Buscar(grupo.getNombre(), sesion.getGrupos());
		int posM= Buscar(id, sesion.getGrupo(posG).getMediciones());
		return sesion.getGrupo(posG).getMedicion(posM);}
		else {
			return null;
		}
	}

	
	public void sincronizarProyecto() {
		
		root = new DefaultMutableTreeNode(proyecto); 
		
		for(Sesion sesion : proyecto.getSesiones() )
        {
			DefaultMutableTreeNode SesionNode = new DefaultMutableTreeNode( sesion );   
            for(Grupo grupo : sesion.getGrupos() )
            {
            	 DefaultMutableTreeNode GrupoNode = new DefaultMutableTreeNode( grupo ); 
            	 
            	 for(Medicion medicion : grupo.getMediciones() )
                 {
                     GrupoNode.add( new DefaultMutableTreeNode(medicion));
                 }
            	 
                SesionNode.add(GrupoNode);
            }
            root.add(SesionNode);
        }
		
		modelo = new DefaultTreeModel( root );         
		EsysM.tree.setModel(modelo);
		
	}
	

	
	public void abrirPath(int row) {
				
		TreePath nameTree = EsysM.tree.getPathForRow(row);

		Sesion sesion = getSesion(nameTree.getPathComponent(1).toString().substring(3));

		if (sesion != null) {

			for (Grupo grupo : sesion.getGrupos()) {

				ArrayList<Medicion> mediciones = grupo.getMediciones();

				if (mediciones.size() > 0) {
					EsysM.tree.expandRow(row);
					EsysM.tree.expandRow(row + 1);
					EsysM.tree.expandRow(row + 2);
					
				} else {
					EsysM.tree.expandRow(row);
					EsysM.tree.expandRow(row + 1);
				}
			}
		}

	}
	

	public void SincronizarColor(boolean alerta, char tipo) {
		EsysM.tree.setCellRenderer( new RenderMediciones(alerta,tipo));
	}
	

	public static int Buscar(String nombre, ArrayList lista) {
		int posicion=-1;
		int tipo=0;
		
		for (int i=0; i<lista.size();i++) {
			
			if ((lista.get(i).toString().charAt(0) == 'S') | (lista.get(i).toString().charAt(0) == 'M')) {

				tipo = 3;
			} else if (lista.get(i).toString().charAt(0) == 'G') {
				tipo = 5;
			} else if (lista.get(i).toString().charAt(0) == 'C') {
				tipo = 7;
			} else if (lista.get(i).toString().charAt(0) == 'I') {
				tipo = 5;
			}
			
			
			if (lista.get(i).toString().substring(tipo).equals(nombre)) {
				posicion = i;
				break;
			}
		}
		
		return posicion;
	}
	

}
