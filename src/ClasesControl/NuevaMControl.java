package ClasesControl;

import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.DataLine;
import javax.swing.JDialog;

import ClasesEntidad.Medicion;
import ClasesEntidad.Sesion;
import ClasesEntidad.Recursos.CapturaAudio;
import ClasesEntidad.Recursos.Dispositivo;
import ClasesFrontera.ColorDialog;
import ClasesFrontera.ColorFrame;
import ClasesFrontera.EsysM;
import ClasesFrontera.NuevaMedicion;
import ClasesFrontera.Recursos.Alerta;
import ClasesEntidad.Grupo;

public class NuevaMControl {

	private Medicion medicion;
	private char[] canal= new char[2];
	public String nombre="Default 0";
	public static Sesion sesion;
	public static Grupo grupo;
	public static String nombreM; 
	private char tipo='S';
	CapturaAudio[] dispositivos= new CapturaAudio[2]; 	
	private String color="#000000";
	public static ColorDialog colord;
	
	
	public NuevaMControl() {
		SelectDisp();
		SelectChanel();
		TipoMedicion();
		Sesion_Grupo();		
		colorF();
		
		Random randon = new Random(); 
		
		float r = randon.nextFloat(); 
		float g = randon.nextFloat(); 
		float b = randon.nextFloat(); 
		
		Color mColor = new Color(r, g, b); 
		
		NuevaMedicion.ColorS.setBackground(mColor);
		
		color=ColorFrame.toHexString(mColor);
	}

	public void colorF() {

		NuevaMedicion.ColorS.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				ColorDialog DialogC;
				DialogC = new ColorDialog(Color.decode(color));
				DialogC.setVisible(true);
				
				NuevaMedicion.ColorS.setBackground(Color.decode(DialogC.getColor()));
				color=DialogC.getColor();

			}
		});

	}
	
	private void SelectDisp() {
		NuevaMedicion.DispositivosS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtDispositvos.setForeground(Color.black);
				
				if (NuevaMedicion.DispositivosS.getSelectedIndex()>0) {
					NuevaMedicion.ChanelS.removeAllItems();
					NuevaMedicion.ChanelS.addItem("Left");
					NuevaMedicion.ChanelS.addItem("Right");
				
					NuevaMedicion.Sesiones.setSelectedIndex(1);
					
					String name=(String) NuevaMedicion.DispositivosS.getSelectedItem();
					dispositivos[0]=null;
					dispositivos[1]= EsysMControl.dispositivosCap.get(BuscarLine(name,EsysMControl.dispositivosCap));
					
					if(!NuevaMedicion.txtNombre.getText().isEmpty()) {
					nombre= NuevaMedicion.txtNombre.getText();	}
				}
				else {
					NuevaMedicion.ChanelS.removeAllItems();
				}
				
			}
		});
		
		NuevaMedicion.DispositivosD1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtReferencia.setForeground(Color.black);
				
				if (NuevaMedicion.DispositivosD1.getSelectedIndex() > 0) {
					NuevaMedicion.ChanelD1.removeAllItems();
					NuevaMedicion.ChanelD1.addItem("Left");
					NuevaMedicion.ChanelD1.addItem("Right");
					
					NuevaMedicion.Sesiones.setSelectedIndex(1);
					
					String name=(String) NuevaMedicion.DispositivosD1.getSelectedItem();
					dispositivos[0]= EsysMControl.dispositivosCap.get(BuscarLine(name,EsysMControl.dispositivosCap));
					
					if(!NuevaMedicion.txtNombreD.getText().isEmpty()) {
					nombre= NuevaMedicion.txtNombreD.getText();	
					}
					
				} else {
					NuevaMedicion.ChanelD1.removeAllItems();
				}
			}
		});
		
		NuevaMedicion.DispositivosD2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtMedicion.setForeground(Color.black);
				
				if (NuevaMedicion.DispositivosD2.getSelectedIndex() > 0) {
					NuevaMedicion.ChanelD2.removeAllItems();

					NuevaMedicion.ChanelD2.addItem("Left");
					NuevaMedicion.ChanelD2.addItem("Right");
					
					NuevaMedicion.Sesiones.setSelectedIndex(1);
					
					String name=(String) NuevaMedicion.DispositivosD2.getSelectedItem();
					dispositivos[1]= EsysMControl.dispositivosCap.get(BuscarLine(name,EsysMControl.dispositivosCap));
					
					if(!NuevaMedicion.txtNombreD.getText().isEmpty()) {
					nombre= NuevaMedicion.txtNombreD.getText();	}
					
				} else {
					NuevaMedicion.ChanelD2.removeAllItems();
				}
			}
		});
	}

	private void SelectChanel() {
		NuevaMedicion.ChanelS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtCanal.setForeground(Color.black);
				
				if (NuevaMedicion.ChanelS.getSelectedIndex()==0) {
					canal[1]='L';
				}else {
					canal[1]='R';
				}
			}
		});
		
		NuevaMedicion.ChanelD1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtcanal1.setForeground(Color.black);
				NuevaMedicion.txtcanal2.setForeground(Color.black);
				
				if (NuevaMedicion.ChanelD1.getSelectedIndex()==0) {
					canal[0]='L';
				}else {
					canal[0]='R';
				}
			}
		});
		
		NuevaMedicion.ChanelD2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				NuevaMedicion.txtcanal1.setForeground(Color.black);
				NuevaMedicion.txtcanal2.setForeground(Color.black);
				
				if (NuevaMedicion.ChanelD2.getSelectedIndex()==0) {
					canal[1]='L';
				}else {
					canal[1]='R';
				}
			}
		});
	}
	
	private void TipoMedicion() {
		NuevaMedicion.btnSimple.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Random randon = new Random(); 
				
				float r = randon.nextFloat(); 
				float g = randon.nextFloat(); 
				float b = randon.nextFloat(); 
				
				Color mColor = new Color(r, g, b); 
				
				NuevaMedicion.ColorS.setBackground(mColor);
				
				color=ColorFrame.toHexString(mColor);

				if (NuevaMedicion.btnSimple.isSelected()) {
					NuevaMedicion.btnDoble.setSelected(false);
					NuevaMedicion.SimplePanel.setVisible(true);
					NuevaMedicion.DoblePanel.setVisible(false);
				} else {
					NuevaMedicion.btnDoble.setSelected(true);
					NuevaMedicion.SimplePanel.setVisible(false);
					NuevaMedicion.DoblePanel.setVisible(true);
				}
			}
		});
		
		NuevaMedicion.btnDoble.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Random randon = new Random(); 
				
				float r = randon.nextFloat(); 
				float g = randon.nextFloat(); 
				float b = randon.nextFloat(); 
				
				Color mColor = new Color(r, g, b); 
				
				NuevaMedicion.ColorD.setBackground(mColor);
				
				color=ColorFrame.toHexString(mColor);

			if (NuevaMedicion.btnDoble.isSelected()) {
				NuevaMedicion.btnSimple.setSelected(false);
				NuevaMedicion.SimplePanel.setVisible(false);
				NuevaMedicion.DoblePanel.setVisible(true);
			}else {
				NuevaMedicion.btnSimple.setSelected(true);
				NuevaMedicion.SimplePanel.setVisible(true);
				NuevaMedicion.DoblePanel.setVisible(false);
			}
			}
		});
		
	}

	private void Sesion_Grupo() {
		NuevaMedicion.Sesiones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (NuevaMedicion.Sesiones.getSelectedIndex() > 0) {

					String nameS = (String) NuevaMedicion.Sesiones.getSelectedItem();

					Sesion sesion = EsysM.contenido.getSesion(nameS);

					NuevaMedicion.Grupos.removeAllItems();
					for (Grupo grupo : sesion.getGrupos()) {
						NuevaMedicion.Grupos.addItem(grupo.getNombre());
					}
				} else {
					NuevaMedicion.Grupos.removeAllItems();
				}

			}
		});
	}

	public void Guardar_Cerrar() {
		NuevaMedicion.btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Alerta alerta = null;
				boolean estado=true;
				
				
				if (NuevaMedicion.btnSimple.isSelected() && dispositivos[1] == null) {

					try {
						alerta = new Alerta('L');
						alerta.Mensaje("Porfavor Escoge el Dispositivo y canal de captura");
						alerta.setVisible(true);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					estado = false;
					NuevaMedicion.txtDispositvos.setForeground(Color.red);
					NuevaMedicion.txtCanal.setForeground(Color.red);
					
				} else if (!NuevaMedicion.btnSimple.isSelected() && dispositivos[0] == null ) {

					try {
						alerta = new Alerta('L');
						alerta.Mensaje("Porfavor Escoge los dispositivos y canales de captura");
						alerta.setVisible(true);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					estado = false;
					NuevaMedicion.txtReferencia.setForeground(Color.red);
					NuevaMedicion.txtMedicion.setForeground(Color.red);
					NuevaMedicion.txtcanal1.setForeground(Color.red);
					NuevaMedicion.txtcanal2.setForeground(Color.red);
					
				} 
				
				if ( dispositivos[0]!=null &&  dispositivos[0].equals(dispositivos[1]) && canal[0]==canal[1]) {
					try {
						alerta = new Alerta('L');
						alerta.Mensaje(
								"Si el dispositivo de Referencia y medici�n son el mismo, es necesario que sus canales sean distintos");
						alerta.setVisible(true);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					estado = false;
					NuevaMedicion.txtcanal1.setForeground(Color.red);
					NuevaMedicion.txtcanal2.setForeground(Color.red);
					
				} else {

					if (NuevaMedicion.Sesiones.getSelectedIndex() == 1
							&& NuevaMedicion.Grupos.getSelectedIndex() == 0) {

						try {
							alerta = new Alerta('P');
							alerta.CaracteristicasP(NuevaMedicion.SGError);
							alerta.Mensaje(
									"Tu medici�n ser� almacenada en la Sesi�n y Grupo por defecto, para realizar un cambio presiona Mu�strame, de lo contrario continua.");
							alerta.setVisible(true);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} // jdialog de alerta y ayuda

						estado = alerta.confirmar();

					} else if (NuevaMedicion.Grupos.getItemCount() == 0) {
						try {
							alerta = new Alerta('P');
							alerta.CaracteristicasP(NuevaMedicion.SGError);
							alerta.Mensaje("Por favor escoge uan sesi�n y un grupo para ubicar tu medici�n");
							alerta.setVisible(true);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				
				
				if (estado==true) {
				
				String nombreS=(String)NuevaMedicion.Sesiones.getSelectedItem();
				
				sesion=EsysM.contenido.getSesion((String)NuevaMedicion.Sesiones.getSelectedItem());
				
				grupo=EsysM.contenido.getGrupo((String)NuevaMedicion.Grupos.getSelectedItem(), sesion);
				
				int posm=CarpetaControl.Buscar(nombre, grupo.getMediciones());
								
					String nameBusqueda = nombre;

					if (posm > -1) {
						
						for (Medicion mediciones : grupo.getMediciones()) {

							char data = mediciones.getNombre().charAt(mediciones.getNombre().length() - 1);
							if (Character.isDigit(data)) {
								int dato = Integer.parseInt(
										"" + mediciones.getNombre().charAt(mediciones.getNombre().length() - 1)) + 1;
								nameBusqueda = mediciones.getNombre().substring(0, mediciones.getNombre().length() - 1) + dato;
							}
						}
						nombre=nameBusqueda;

					} 
					
				nombreM=nombre;
				

				EsysM.contenido.AgregarMedicion(nombre, sesion, grupo, color, dispositivos, canal);	
				
				EsysM.contenido.sincronizarProyecto();	

				EsysMControl.nuevaM.setVisible(false);}
				
			}
		});
	}
	
	public static int BuscarLine(String nombre, ArrayList lista) {
		int posicion = -1;
		int tipo = 0;

		for (int i = 0; i < lista.size(); i++) {

			if (lista.get(i).toString().equals(nombre)) {
				posicion = i;
				break;
			}
		}

		return posicion;
	}
}
