package ClasesControl;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import ClasesEntidad.Captura;
import ClasesEntidad.Carpeta;
import ClasesEntidad.Medicion;
import ClasesEntidad.Proyecto;
import ClasesEntidad.ProyectoC;
import ClasesEntidad.Sesion;
import ClasesFrontera.EsysM;

public class CapturaControl {

	private ProyectoC proyecto;
	
	public void CrearProyecto(String direccion) {
		proyecto=new ProyectoC();
		proyecto.setNombre(direccion);
	}
	
	public ProyectoC getProyecto() {
		return proyecto;
	}
	
	public void AgregarCarpeta(String nombre, String color) {
		Carpeta carpeta= new Carpeta(nombre, color);
		proyecto.addCarpeta(carpeta);
	}
	
	public Carpeta getCarpeta(String id) {
		int pos= Buscar(id, proyecto.getCarpetas());
		return proyecto.getCarpeta(pos);
	}
	
	public ArrayList<Carpeta> getCarpetas(){
		return proyecto.getCarpetas();
	}
	
	public void AgregarCaptura(String nombre, Carpeta carpeta, String color) {
		Captura captura= new Captura(nombre, color);
		carpeta.addCaptura(captura);
	}
	
	public Captura getCaptura(String id,Carpeta carpeta) {
		int pos= Buscar(id, carpeta.getCapturas());
		return carpeta.getCaptura(pos);
	}
	
	
	public void sincronizarProyecto() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(proyecto); 
		
		for(Carpeta carpeta : proyecto.getCarpetas())
        {
            DefaultMutableTreeNode CarpetaNode = new DefaultMutableTreeNode( carpeta );   
            
            for(Captura captura : carpeta.getCapturas() )
            {
            	 
               CarpetaNode.add( new DefaultMutableTreeNode( captura));
            }
            
            root.add(CarpetaNode);
        }
		
		DefaultTreeModel modelo = new DefaultTreeModel( root );        
        EsysM.treeC.setModel(modelo);        
	}
	
	public void abrirPath(int row) {
		
		TreePath nameTree = EsysM.treeC.getPathForRow(row);

		Carpeta carpeta = getCarpeta(nameTree.getPathComponent(1).toString());

		if (carpeta != null) {

			for (Carpeta carpetas : EsysM.contenidoC.getCarpetas()) {

				ArrayList<Captura> captura = carpetas.getCapturas();

				if (captura.size() > 0) {
					EsysM.treeC.expandRow(row);
					EsysM.treeC.expandRow(row + 1);
					
				} else {
					EsysM.treeC.expandRow(row);
				}
			}			
		}

	}
	
	public void SincronizarColor(String Colortxt) {
		EsysM.treeC.setCellRenderer(new RenderCapturas(Colortxt));
	}

	public static int Buscar(String nombre, ArrayList lista) {
		int posicion=0;
		
		for (int i=0; i<lista.size();i++) {
			if (lista.get(i).toString().equals(nombre)) {
				posicion=i;
				break;
			}
		}
		
		return posicion;
	}

}
