package ClasesControl;

import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.border.Border;
import javax.swing.tree.TreePath;

import ClasesControl.Recursos.RecursosControl;
import ClasesEntidad.Sesion;
import ClasesEntidad.Medicion;
import ClasesEntidad.Proyecto;
import ClasesEntidad.ProyectoC;
import ClasesEntidad.Recursos.CapturaAudio;
import ClasesEntidad.Recursos.Dispositivo;
import ClasesEntidad.Recursos.FiltroDisp;
import ClasesEntidad.Recursos.FuenteAudio;
import ClasesFrontera.ColorFrame;
import ClasesFrontera.EsysM;
import ClasesFrontera.NuevaMedicion;
import ClasesFrontera.Recursos.Alerta;
import ClasesFrontera.Recursos.Recursos;
import ClasesEntidad.Captura;
import ClasesEntidad.Carpeta;
import ClasesEntidad.Grupo;

public class EsysMControl {

	private int TipoM, TipodB, TipoHz;
	public static int TreeCont, TreeContC;
	public static Sesion sesion;
	public static Grupo grupo;
	public static Medicion medicion;
	public static Carpeta carpeta;
	public static Captura captura;
	public static int pathRow, pathRowC;
    public static ArrayList<CapturaAudio> dispositivosCap= new ArrayList<CapturaAudio>();
    public static ArrayList<FuenteAudio> dispositivosRep= new ArrayList<FuenteAudio>();
    public static NuevaMedicion nuevaM;
    private CapturaAudio[] dispo;
    private char[] canal;
    private int pos,posg,posm;
    private boolean iniciadoMonitor=false;
    private CapturaAudio disp=null,dispM=null,dispR=null;
    private Medicion medicion1=null, medicion2=null;
	
	public EsysMControl() {
		minimizarMediciones(); 
		maximizarGenerador();
		Medicion();
		Captura();
		AbrirtipoMedicion();
		AbrirtipoMagnitud();
		AbrirtipoEjeX();
		SelecTipoMedicion();
		SelecTipoMagnitud();
		SelecTipoEjeX();
		SelectTHD();
		SelectCoherence();
		
		Monitor();
		
		EsysM.MostrarMS.setVisible(false);
		EsysM.MostrarMD.setVisible(false);
		EsysM.SesionGrupoPanel.setVisible(false);	
		EsysM.THDpanel.setVisible(false);
		
		EsysM.tree.expandPath(EsysM.tree.getPathForRow(1));
		EsysM.tree.setSelectionPath(EsysM.tree.getPathForRow(1));
		TreeCont=3;
		pathRow=2;
		
		TreePath nameTree=EsysM.tree.getPathForRow(pathRow);	
		DataTree(nameTree);	
		
		TreeContC=2;
		pathRowC=1;
		
		EsysM.treeC.setSelectionPath(EsysM.treeC.getPathForRow(1));
		TreePath nameTreeC=EsysM.treeC.getPathForRow(pathRowC);	
		DataTreeC(nameTreeC);	
		
	}
	
	public void dimensiones() {
		EsysM.Escritorio.addHierarchyBoundsListener(new HierarchyBoundsAdapter() {
			@Override
			public void ancestorResized(HierarchyEvent arg0) {
				
			if (EsysM.frmEsysm.getHeight()>=EsysM.altoIni || EsysM.frmEsysm.getWidth()>=EsysM.anchoIni ) {
				EsysM.menuBar.setBounds(0, 0, EsysM.frmEsysm.getWidth(), 26);
				EsysM.porDefecto.setBounds(0, 52, 180, (int) (EsysM.frmEsysm.getHeight()));
				EsysM.Barradivision1.setBounds(0, EsysM.frmEsysm.getHeight()-(EsysM.altoIni-257), 180, 5);
				EsysM.tree.setBounds(0, 0, 170, EsysM.Barradivision1.getY()-EsysM.qPane.getY());   
				EsysM.qPane.setBounds(10, 10, 168, EsysM.Barradivision1.getY()-EsysM.qPane.getY());  
				EsysM.Monitoreo.setBounds(EsysM.frmEsysm.getWidth()-186, 25, 170, EsysM.Grafica.getHeight()+EsysM.BarrasupDerecha.getHeight());
				EsysM.treeC.setBounds(0, 0, 170, EsysM.Monitoreo.getHeight()-EsysM.qPaneC.getY()-5);
				EsysM.qPaneC.setBounds(10, 334, 150, EsysM.Monitoreo.getHeight()-EsysM.qPaneC.getY()-5); 
				EsysM.MostrarMS.setBounds(0, EsysM.frmEsysm.getHeight()-(EsysM.altoIni-262), 180, 163);
				EsysM.MostrarMD.setBounds(0, EsysM.frmEsysm.getHeight()-(EsysM.altoIni-262), 180, 163);
				EsysM.GeneradorPanel.setBounds(0, EsysM.frmEsysm.getHeight()-(EsysM.altoIni-424), 180, 137);
				EsysM.SesionGrupoPanel.setBounds(0, EsysM.frmEsysm.getHeight()-(EsysM.altoIni-262), 180, 163);
				EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27);
				EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
				EsysM.EjexPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.64), 25, 160, 27);

				EsysM.btnAbajoTipo.setSelected(false);
				EsysM.btnAbajoTipo1.setSelected(false);
				EsysM.btnAbajoTipo2.setSelected(false);
				
				EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
				EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				EsysM.txtTipoEjeX.setForeground(new Color(0, 128, 128));
				
				ImageIcon MaxOff= new ImageIcon(this.getClass().getResource("/ClasesFrontera/Recursos/Imagenes/mas.png"));
				ImageIcon MaxEscOff=new ImageIcon( MaxOff.getImage().getScaledInstance(EsysM.btnGenerador.getWidth(), EsysM.btnGenerador.getHeight(), java.awt.Image.SCALE_SMOOTH));
				EsysM.btnGenerador.setIcon(MaxEscOff);
				
				if (!EsysM.porDefecto.isVisible()) {
					EsysM.Grafica.setBounds(0, 52, EsysM.menuBar.getWidth()-197,EsysM.frmEsysm.getHeight()-(65+EsysM.menuBar.getHeight()));	
				}
				else {
					EsysM.Grafica.setBounds(181, 52, EsysM.menuBar.getWidth()-EsysM.porDefecto.getWidth()-188,EsysM.frmEsysm.getHeight()-(65+EsysM.menuBar.getHeight()));
				}
			}
			
			}
		});
	}
	
	public void colorF() {

		EsysM.ColorM.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Color color=Color.BLACK;
				if (TreeCont==4) {
					color=medicion.getColor();
				}				
				ColorFrame colorf = new ColorFrame(color,'M',EsysM.ColorM);
				EsysM.Escritorio.add(colorf);
				colorf.show();
			}
		});
		
		EsysM.ColorM2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				Color color=Color.BLACK;
				if (TreeCont==4) {
					color=medicion.getColor();
				}
				ColorFrame colorf = new ColorFrame(color,'M', EsysM.ColorM2);
				EsysM.Escritorio.add(colorf);
				colorf.show();
			}
		});
		
		EsysM.ColorSG.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				Color color=Color.BLACK;
				if (TreeCont==3) {
					color=grupo.getColor();
				}else if (TreeCont==2) {
					color=sesion.getColor();
				}
				
				ColorFrame colorf = new ColorFrame(color,'M',EsysM.ColorSG);
				EsysM.Escritorio.add(colorf);
				colorf.show();
			}
		});
		
		EsysM.ColorC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				Color color=Color.BLACK;
				if (TreeContC==3) {
					color=captura.getColor();
				}else if (TreeContC==2) {
					color=carpeta.getColor();
				}
				
				ColorFrame colorf = new ColorFrame(color, 'C',null);
				EsysM.Escritorio.add(colorf);
				colorf.show();
			}
		});

	}
	
	private void minimizarMediciones() {
		EsysM.Minimizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (EsysM.porDefecto.isVisible()) {			
					EsysM.porDefecto.setVisible(false);
					ImageIcon MaxOff= new ImageIcon(this.getClass().getResource("/ClasesFrontera/Recursos/Imagenes/mas.png"));
					ImageIcon MaxEscOff=new ImageIcon( MaxOff.getImage().getScaledInstance(EsysM.Minimizar.getWidth(), EsysM.Minimizar.getHeight(), java.awt.Image.SCALE_SMOOTH));
					EsysM.Minimizar.setIcon(MaxEscOff);
					
					EsysM.Grafica.setBounds(0, 52, EsysM.frmEsysm.getWidth()-EsysM.Monitoreo.getWidth()-16,EsysM.frmEsysm.getHeight()-(27+EsysM.menuBar.getHeight()));
					
				}
				else {
					EsysM.porDefecto.setVisible(true);
				    ImageIcon MiniOff= new ImageIcon(this.getClass().getResource("/ClasesFrontera/Recursos/Imagenes/minimizar.png"));
					ImageIcon MiniEscOff=new ImageIcon( MiniOff.getImage().getScaledInstance(EsysM.Minimizar.getWidth(), EsysM.Minimizar.getHeight(), java.awt.Image.SCALE_SMOOTH));
					EsysM.Minimizar.setIcon(MiniEscOff);
					
					EsysM.Grafica.setBounds(181, 52, EsysM.frmEsysm.getWidth()-EsysM.Monitoreo.getWidth()-197,EsysM.frmEsysm.getHeight()-(27+EsysM.menuBar.getHeight()));
				}
				
			}
		});
	}
	
	private void Medicion() {
		
		EsysM.txtNombreSG.addActionListener(new java.awt.event.ActionListener() {
	    	@Override
	    	public void actionPerformed(java.awt.event.ActionEvent evt) {
	    	
				if (TreeCont==3) {
					grupo.setNombre(EsysM.txtNombreSG.getText());
				}else if (TreeCont==2) {
					sesion.setNombre(EsysM.txtNombreSG.getText());
				}
				
				EsysM.contenido.sincronizarProyecto();
				
				int pos = CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones()) + 1;

				int posG = CarpetaControl.Buscar(grupo.getNombre(), sesion.getGrupos()) + 1;
				
				EsysM.contenido.abrirPath(pos);

				EsysM.tree.setSelectionRow(pos + posG);

				TreePath nameTree = EsysM.tree.getPathForRow(pos + posG);
				TreeCont = nameTree.getPathCount();

				DataTree(nameTree);
				
	    	}
	    });
		
		EsysM.NombreM.addActionListener(new java.awt.event.ActionListener() {
	    	@Override
	    	public void actionPerformed(java.awt.event.ActionEvent evt) {

				medicion.setNombre(EsysM.NombreM.getText());
				
				EsysM.contenido.sincronizarProyecto();
				
				int pos = CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones()) + 1;

				int posG = CarpetaControl.Buscar(grupo.getNombre(), sesion.getGrupos()) + 1;

				int posM = CarpetaControl.Buscar(medicion.getNombre(), grupo.getMediciones()) + 1;

				EsysM.contenido.abrirPath(pos);

				EsysM.contenido.abrirPath(pos + posG);

				EsysM.tree.setSelectionRow(pos + posG + posM);

				TreePath nameTree = EsysM.tree.getPathForRow(pos + posG + posM);
				TreeCont = nameTree.getPathCount();

				DataTree(nameTree);
		
	    	}
	    });
		
		EsysM.NombreMD.addActionListener(new java.awt.event.ActionListener() {
	    	@Override
	    	public void actionPerformed(java.awt.event.ActionEvent evt) {

				medicion.setNombre(EsysM.NombreMD.getText());

				EsysM.contenido.sincronizarProyecto();

				int pos = CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones()) + 1;

				int posG = CarpetaControl.Buscar(grupo.getNombre(), sesion.getGrupos()) + 1;

				int posM = CarpetaControl.Buscar(medicion.getNombre(), grupo.getMediciones()) + 1;

				EsysM.contenido.abrirPath(pos);

				EsysM.contenido.abrirPath(pos + posG);

				EsysM.tree.setSelectionRow(pos + posG + posM);

				TreePath nameTree = EsysM.tree.getPathForRow(pos + posG + posM);
				TreeCont = nameTree.getPathCount();

				DataTree(nameTree);
	    	}
	    });
		
		EsysM.btnBorrarSG.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    	    			
	    		if (TreeCont==3) {

	    			sesion.ClearGrupo(grupo.getNombre());   

				}else if (TreeCont==2) {

				
						Proyecto proyecto = EsysM.contenido.getProyecto();
						proyecto.ClearSesion(sesion.getNombre());

				}
				
				EsysM.contenido.sincronizarProyecto();
				System.out.println(pathRow-2);
				EsysM.contenido.abrirPath(pathRow-2);
				
				
	    	
	    	}
	    });
		
		EsysM.borrarMS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				grupo.ClearMedicion(medicion.getNombre());
				EsysM.contenido.sincronizarProyecto();
				EsysM.contenido.abrirPath(pathRow-2);
			}
		});
		
		EsysM.borrarMD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				grupo.ClearMedicion(medicion.getNombre());

				EsysM.contenido.sincronizarProyecto();
				EsysM.contenido.abrirPath(pathRow-2);
			}
		});
		
		EsysM.tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				EsysM.contenido.SincronizarColor(false, '.');
				
				pathRow=EsysM.tree.getLeadSelectionRow();
				TreePath nameTree=EsysM.tree.getPathForRow(pathRow);
				TreeCont=nameTree.getPathCount(); 
				
				if (EsysM.tree.isExpanded(nameTree)) {
				EsysM.tree.collapsePath(nameTree);	
				}else {
				EsysM.tree.expandPath(nameTree);}
				
				DataTree(nameTree);
				
				
					
			}
		});
	}
	
	private void DataTree(TreePath nameTree) {
		
		if (nameTree.getPathCount() == 4 ) {

			String nMedicion = nameTree.getPathComponent(3).toString().substring(3);
			String nGrupo= nameTree.getPathComponent(2).toString().substring(5);
			String nSesion= nameTree.getPathComponent(1).toString().substring(3);
			//String nProyecto= name.getPathComponent(0).toString();
			
			sesion=EsysM.contenido.getSesion(nSesion);
			grupo=EsysM.contenido.getGrupo(nGrupo, sesion);
			medicion= EsysM.contenido.getMedicion(nMedicion, sesion, grupo);
			
			dispo=(CapturaAudio[]) medicion.getDisp();
			
			
			if (dispo[0]==null) {
				
				if (EsysM.GeneradorPanel.getY()>(EsysM.Barradivision1.getY()+EsysM.Barradivision1.getHeight())){
				EsysM.MostrarMD.setVisible(false);
				EsysM.MostrarMS.setVisible(true);
				EsysM.SesionGrupoPanel.setVisible(false);}
				
				EsysM.ColorM.setBackground(medicion.getColor());
				EsysM.NombreM.setText(medicion.getNombre());
				
				
				if (iniciadoMonitor == true) {
					if (!medicion.getActivo()) {
						disp.suspender(true);
						EsysM.monitorS.setSelected(false);
					} else {
						disp.suspender(false);
						EsysM.monitorS.setSelected(true);
					}
				}
				
				EsysM.THDpanel.setVisible(true);
				EsysM.CoherencePanel.setVisible(false);
				
				EsysM.txtEspectrograma.setEnabled(true);
				EsysM.txtFuncionDeTransferencia.setEnabled(false);
				
				EsysM.DispositivoM.setText("Dispositivo: "+dispo[1].getNombre().substring(0, dispo[1].getNombre().length()-10));
				EsysM.CanalM.setText("Canal: "+Character.toString(medicion.getCanalM()));
				
			} else {
				if (EsysM.GeneradorPanel.getY()>(EsysM.Barradivision1.getY()+EsysM.Barradivision1.getHeight())){
				EsysM.MostrarMS.setVisible(false);
				EsysM.MostrarMD.setVisible(true);
				EsysM.SesionGrupoPanel.setVisible(false);}
				
				EsysM.ColorM2.setBackground(medicion.getColor());
				EsysM.NombreMD.setText(medicion.getNombre());
				
				
				if (iniciadoMonitor == true) {
					if (!medicion2.getActivo()) {
						if (dispM.equals(dispR)) {
							dispM.suspender(true);
							System.out.println("igual");
						}else {
							dispM.suspender(true);
							dispR.suspender(true);
						}

						EsysM.monitorD.setSelected(false);
					} else {
						if (dispM.equals(dispR)) {
							dispM.suspender(false);
						}else {
							dispM.suspender(false);
							dispR.suspender(false);
						}
						EsysM.monitorD.setSelected(true);
					}
				}
				
				EsysM.THDpanel.setVisible(false);
				EsysM.CoherencePanel.setVisible(true);
				
				EsysM.txtFuncionDeTransferencia.setEnabled(true);
				EsysM.txtEspectrograma.setEnabled(false);
				
				EsysM.ReferenciaData.setText("Ref: "+dispo[0].getNombre().substring(0, 16)+"\n"+"Canal: "+Character.toString(medicion.getCanalR()));
				EsysM.MedicionData.setText("Med: "+dispo[1].getNombre().substring(0, 16)+"\n"+"Canal: "+Character.toString(medicion.getCanalM()));
				
			}
		}else if(nameTree.getPathCount() == 3) {


			String nSesion= nameTree.getPathComponent(1).toString().substring(3);
			sesion=EsysM.contenido.getSesion(nSesion);
			String nGrupo= nameTree.getPathComponent(2).toString().substring(5);
			
			EsysM.ngrupostxt.setText("N� Grupos: --");
			
			grupo= EsysM.contenido.getGrupo(nGrupo, sesion);
			
			pos=CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones());
			posg=CarpetaControl.Buscar(grupo.getNombre(), sesion.getGrupos());			
    		
			
			if (pos==0 && posg==0) {
				EsysM.btnBorrarSG.setEnabled(false);
			} else if (pos==0 && posg>0){
				EsysM.btnBorrarSG.setEnabled(true);
			}
			
			EsysM.ColorSG.setBackground(grupo.getColor());
			
			EsysM.txtNombreSG.setText(grupo.getNombre());
			
			ArrayList<Medicion> mediciones=grupo.getMediciones();
			EsysM.nmedicionestxt.setText("N� Mediciones:"+" "+mediciones.size());
			
			if (EsysM.GeneradorPanel.getY()>(EsysM.Barradivision1.getY()+EsysM.Barradivision1.getHeight())){
			EsysM.MostrarMS.setVisible(false);
			EsysM.MostrarMD.setVisible(false);
			EsysM.SesionGrupoPanel.setVisible(true);}
			
			EsysM.THDpanel.setVisible(false);
			
		}else if(nameTree.getPathCount() == 2) {
			
			Border borde = BorderFactory.createLineBorder(Color.white, 1);
			EsysM.tree.setBorder(borde);
			
			String nSesion= nameTree.getPathComponent(1).toString().substring(3);
			sesion=EsysM.contenido.getSesion(nSesion);

			
			pos=CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones());		

			
			if (pos==0) {
				EsysM.btnBorrarSG.setEnabled(false);
			} else {
				EsysM.btnBorrarSG.setEnabled(true);}
			
			EsysM.ColorSG.setBackground(sesion.getColor());
			
			EsysM.txtNombreSG.setText(sesion.getNombre());
			
			int contM=0;
			for (Grupo gn:sesion.getGrupos()) {
				ArrayList<Medicion> mn= gn.getMediciones();
				contM=contM+mn.size();
			}
			
			ArrayList<Grupo> grupos=sesion.getGrupos();
			EsysM.ngrupostxt.setText("N� Grupos:"+" "+grupos.size());
			
			EsysM.nmedicionestxt.setText("N� Mediciones:"+" "+contM);
			
			if (EsysM.GeneradorPanel.getY()>(EsysM.Barradivision1.getY()+EsysM.Barradivision1.getHeight())){
			EsysM.MostrarMS.setVisible(false);
			EsysM.MostrarMD.setVisible(false);
			EsysM.SesionGrupoPanel.setVisible(true);}
			
			EsysM.THDpanel.setVisible(false);
			
		}else {
		
			EsysM.MostrarMS.setVisible(false);
			EsysM.MostrarMD.setVisible(false);
			EsysM.SesionGrupoPanel.setVisible(false);	
			EsysM.THDpanel.setVisible(false);
		}
	}
	
	private void Captura() {
		
		EsysM.BorrarCap.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    	
	    		System.out.println(TreeContC);
	    		
	    		if (TreeContC==3) {
					
	    			carpeta.ClearCaptura(captura.getNombre()); 
	    			
				}else if (TreeContC==2) {
					
					ProyectoC proyecto=EsysM.contenidoC.getProyecto();
					System.out.println(carpeta.getNombre());
					proyecto.ClearCarpeta(carpeta.getNombre());
				}
				
				EsysM.contenidoC.sincronizarProyecto();
				EsysM.treeC.setSelectionPath(EsysM.treeC.getPathForRow(pathRowC-1));
	    	
	    	}
	    });
		
		EsysM.treeC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				EsysM.contenidoC.SincronizarColor("#000000");
				
				pathRowC=EsysM.treeC.getLeadSelectionRow();
				TreePath nameTreeC=EsysM.treeC.getPathForRow(pathRowC);
				TreeContC=nameTreeC.getPathCount(); 
				
				if (EsysM.treeC.isExpanded(nameTreeC)) {
					EsysM.treeC.collapsePath(nameTreeC);	
					}else {
					EsysM.treeC.expandPath(nameTreeC);}
				
				DataTreeC(nameTreeC);
			}
		});
	}
	
	public static void DataTreeC(TreePath nameTreeC) {
	
		 if(nameTreeC.getPathCount() == 3) {

				String nCaptura = nameTreeC.getPathComponent(2).toString();
				String nCarpeta= nameTreeC.getPathComponent(1).toString();

				carpeta = EsysM.contenidoC.getCarpeta(nCarpeta);
				captura = EsysM.contenidoC.getCaptura(nCaptura, carpeta);

			}else if(nameTreeC.getPathCount() == 2) {
				
				String nCarpeta = nameTreeC.getPathComponent(1).toString();
				carpeta = EsysM.contenidoC.getCarpeta(nCarpeta);
				
			}
	}
	
	private void maximizarGenerador() {
		EsysM.btnGenerador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				int posicionx = EsysM.MostrarMS.getX();
				int posiciony = EsysM.MostrarMS.getY();
				int ancho = EsysM.MostrarMS.getWidth();
				int alto = EsysM.MostrarMS.getHeight() + EsysM.BarraGenerador.getHeight()+30;
				
				if (EsysM.GeneradorPanel.getY()!=posiciony) {
					
					try {
						DispReproducir();
					} catch (LineUnavailableException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					if (TreeCont==4) {
						if (dispo[0]==null) {
							EsysM.MostrarMS.setVisible(false);
						} else {
							EsysM.MostrarMD.setVisible(false);
						}
					}
					else if (TreeCont==3 || TreeCont==2) {
						EsysM.SesionGrupoPanel.setVisible(false);
					}

					EsysM.GeneradorPanel.setBounds(posicionx, posiciony, ancho, alto);

					ImageIcon MiniOff = new ImageIcon(this.getClass().getResource("/ClasesFrontera/Recursos/Imagenes/minimizar.png"));
					ImageIcon MiniEscOff = new ImageIcon(MiniOff.getImage().getScaledInstance(EsysM.btnGenerador.getWidth(),EsysM.btnGenerador.getHeight(), java.awt.Image.SCALE_SMOOTH));
					EsysM.btnGenerador.setIcon(MiniEscOff);
					
				} else {
					EsysM.GeneradorPanel.setBounds(0, EsysM.frmEsysm.getHeight() - (EsysM.altoIni - 424), EsysM.BarraGenerador.getWidth(), EsysM.BarraGenerador.getHeight());
					ImageIcon MaxOff = new ImageIcon(this.getClass().getResource("/ClasesFrontera/Recursos/Imagenes/mas.png"));
					ImageIcon MaxEscOff = new ImageIcon(MaxOff.getImage().getScaledInstance(EsysM.btnGenerador.getWidth(),EsysM.btnGenerador.getHeight(), java.awt.Image.SCALE_SMOOTH));
					EsysM.btnGenerador.setIcon(MaxEscOff);
					
					if (TreeCont==4) {
						if (dispo[0]==null) {
							EsysM.MostrarMS.setVisible(true);
						} else  {
							EsysM.MostrarMD.setVisible(true);
						}
					}
					else if (TreeCont==3 || TreeCont==2) {
						EsysM.SesionGrupoPanel.setVisible(true);
					}
					
					
				}

			}
		});
	}

	public static void DispReproducir() throws LineUnavailableException {

		if (dispositivosRep.size() == 0) {
			
			FiltroDisp filtro = new FiltroDisp(); // Objeto tipo FiltroDisp para acceder a dispositivos de Reproducci�n o fuente
			
			filtro.Reproduccion();
			
			EsysM.DispGenerador.removeAllItems();// Borrar todas las filas del JcomBox DISPOSITIVOS

			EsysM.DispGenerador.setMaximumRowCount(dispositivosRep.size() + 1);

			EsysM.DispGenerador.addItem("Dispositivos de Reproducci�n");
			for (int i = 0; i < dispositivosRep.size(); i++) {
				if (dispositivosRep.get(i).isHabilitado()) {
				EsysM.DispGenerador.addItem(dispositivosRep.get(i).getNombre());}
			}
			
		} else {

			EsysM.DispGenerador.removeAllItems();// Borrar todas las filas del JcomBox DISPOSITIVOS

			EsysM.DispGenerador.setMaximumRowCount(dispositivosRep.size() + 1);

			EsysM.DispGenerador.addItem("Dispositivos de Reproducci�n");
			for (int i = 0; i < dispositivosRep.size(); i++) {
				if (dispositivosRep.get(i).isHabilitado()) {
				EsysM.DispGenerador.addItem(dispositivosRep.get(i).getNombre());}
			}
		}
	}

	private void AbrirtipoMedicion() {
	    EsysM.btnAbajoTipo.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {

				if (EsysM.btnAbajoTipo.getHeight()<30 && EsysM.btnAbajoTipo.isSelected()) {
					
					EsysM.txtTipoDeMedicion.setForeground(Color.black);
				    EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27+EsysM.TipoPanelOpcion.getHeight());   
				} else {
					EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27);
					EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
				}
			}
		});

	    EsysM.btnAbajoTipo.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseEntered(MouseEvent arg0) {
	    		
	    		if (EsysM.btnAbajoTipo.getHeight()<30 && !EsysM.btnAbajoTipo.isSelected()) {
	    		EsysM.txtTipoDeMedicion.setForeground(Color.black);}
	    	}
	    	@Override
	    	public void mouseExited(MouseEvent e) {
	    		if (EsysM.btnAbajoTipo.getHeight()<30 && !EsysM.btnAbajoTipo.isSelected()) {
	    		 EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));}
	    	}
	    });  
  

	}
	
	private void AbrirtipoMagnitud() {
	    EsysM.btnAbajoTipo1.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {

				if (EsysM.btnAbajoTipo1.getHeight()<30 && EsysM.btnAbajoTipo1.isSelected()) {
					
					EsysM.txtTipoMagnitud.setForeground(Color.black);
				    EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27+EsysM.MagPanelOpcion.getHeight());   

				} else {
					EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
					EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				}
			}
		});

	    EsysM.btnAbajoTipo1.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseEntered(MouseEvent arg0) {
	    		
	    		if (EsysM.btnAbajoTipo1.getHeight()<30 && !EsysM.btnAbajoTipo1.isSelected()) {
	    		EsysM.txtTipoMagnitud.setForeground(Color.black);}
	    	}
	    	@Override
	    	public void mouseExited(MouseEvent e) {
	    		if (EsysM.btnAbajoTipo1.getHeight()<30 && !EsysM.btnAbajoTipo1.isSelected()) {
	    		 EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));}
	    	}
	    });  
  

	}
	
	private void AbrirtipoEjeX() {
	    EsysM.btnAbajoTipo2.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {

				if (EsysM.btnAbajoTipo2.getHeight()<30 && EsysM.btnAbajoTipo2.isSelected()) {
					
					EsysM.txtTipoEjeX.setForeground(Color.black);
				    EsysM.EjexPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.64), 25, 160, 27+EsysM.HzPanelOpcion.getHeight());   

				} else {
					EsysM.EjexPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.64), 25, 160, 27);
					EsysM.txtTipoEjeX.setForeground(new Color(0, 128, 128));
				}
			}
		});

	    EsysM.btnAbajoTipo2.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseEntered(MouseEvent arg0) {
	    		
	    		if (EsysM.btnAbajoTipo2.getHeight()<30 && !EsysM.btnAbajoTipo2.isSelected()) {
	    		EsysM.txtTipoEjeX.setForeground(Color.black);}
	    	}
	    	@Override
	    	public void mouseExited(MouseEvent e) {
	    		if (EsysM.btnAbajoTipo2.getHeight()<30 && !EsysM.btnAbajoTipo2.isSelected()) {
	    		 EsysM.txtTipoEjeX.setForeground(new Color(0, 128, 128));}
	    	}
	    });  
  

	}

	private void SelecTipoMedicion() {
		EsysM.txtRta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtRta.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipoM != 1) {
					EsysM.txtRta.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipoM = 1;
				EsysM.txtTipoDeMedicion.setText(EsysM.txtRta.getText());
				EsysM.txtRta.setForeground(Color.BLACK);
				EsysM.txtEspectrograma.setForeground(new Color(0, 128, 128));
				EsysM.txtFuncionDeTransferencia.setForeground(new Color(0, 128, 128));
				EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27);
				EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo.setSelected(false);
			}
		});

		EsysM.txtEspectrograma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtEspectrograma.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipoM != 2) {
					EsysM.txtEspectrograma.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipoM = 2;
				EsysM.txtTipoDeMedicion.setText(EsysM.txtEspectrograma.getText());
				EsysM.txtEspectrograma.setForeground(Color.BLACK);
				EsysM.txtRta.setForeground(new Color(0, 128, 128));
				EsysM.txtFuncionDeTransferencia.setForeground(new Color(0, 128, 128));
				EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27);
				EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo.setSelected(false);
			}
		});

		EsysM.txtFuncionDeTransferencia.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtFuncionDeTransferencia.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipoM != 3) {
					EsysM.txtFuncionDeTransferencia.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipoM = 3;
				EsysM.txtTipoDeMedicion.setText("F. Transferencia");
				EsysM.txtFuncionDeTransferencia.setForeground(Color.BLACK);
				EsysM.txtRta.setForeground(new Color(0, 128, 128));
				EsysM.txtEspectrograma.setForeground(new Color(0, 128, 128));
				EsysM.TipoPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.21), 25, 160, 27);
				EsysM.txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo.setSelected(false);
			}
		});
	}
	
	private void SelecTipoMagnitud() {
		EsysM.txtdBFs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtdBFs.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipodB != 1) {
					EsysM.txtdBFs.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipodB = 1;
				EsysM.txtTipoMagnitud.setText(EsysM.txtdBFs.getText());
				EsysM.txtdBFs.setForeground(Color.BLACK);
				EsysM.txtdBspl.setForeground(new Color(0, 128, 128));
				EsysM.txtdBu.setForeground(new Color(0, 128, 128));
				EsysM.txtdBV.setForeground(new Color(0, 128, 128));
				EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
				EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo1.setSelected(false);
			}
		});

		EsysM.txtdBspl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtdBspl.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipodB != 2) {
					EsysM.txtdBspl.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipodB = 2;
				EsysM.txtTipoMagnitud.setText("N. Presi�n [dBspl]");
				EsysM.txtdBspl.setForeground(Color.BLACK);
				EsysM.txtdBFs.setForeground(new Color(0, 128, 128));
				EsysM.txtdBu.setForeground(new Color(0, 128, 128));
				EsysM.txtdBV.setForeground(new Color(0, 128, 128));
				EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
				EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo1.setSelected(false);
			}
		});
		
		EsysM.txtdBu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtdBu.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipodB != 3) {
					EsysM.txtdBu.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipodB = 3;
				EsysM.txtTipoMagnitud.setText(EsysM.txtdBu.getText());
				EsysM.txtdBu.setForeground(Color.BLACK);
				EsysM.txtdBspl.setForeground(new Color(0, 128, 128));
				EsysM.txtdBFs.setForeground(new Color(0, 128, 128));
				EsysM.txtdBV.setForeground(new Color(0, 128, 128));
				EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
				EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo1.setSelected(false);
			}
		});
		
		EsysM.txtdBV.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtdBV.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipodB != 4) {
					EsysM.txtdBV.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipodB = 4;
				EsysM.txtTipoMagnitud.setText(EsysM.txtdBV.getText());
				EsysM.txtdBV.setForeground(Color.BLACK);
				EsysM.txtdBspl.setForeground(new Color(0, 128, 128));
				EsysM.txtdBu.setForeground(new Color(0, 128, 128));
				EsysM.txtdBFs.setForeground(new Color(0, 128, 128));
				EsysM.MagPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.42), 25, 160, 27);
				EsysM.txtTipoMagnitud.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo1.setSelected(false);
			}
		});
	}
	
	private void SelecTipoEjeX() {
		EsysM.txtLineal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtLineal.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipoHz != 1) {
					EsysM.txtLineal.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipoHz = 1;
				EsysM.txtTipoEjeX.setText(EsysM.txtLineal.getText());
				EsysM.txtLineal.setForeground(Color.BLACK);
				EsysM.txtLog.setForeground(new Color(0, 128, 128));
				EsysM.EjexPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.64), 25, 160, 27);
				EsysM.txtTipoEjeX.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo2.setSelected(false);
			}
		});
		
		EsysM.txtLog.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				EsysM.txtLog.setForeground(Color.BLACK);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (TipoHz != 2) {
					EsysM.txtLog.setForeground(new Color(0, 128, 128));
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				TipoHz = 2;
				EsysM.txtTipoEjeX.setText(EsysM.txtLog.getText());
				EsysM.txtLog.setForeground(Color.BLACK);
				EsysM.txtLineal.setForeground(new Color(0, 128, 128));
				EsysM.EjexPanel.setBounds((int) (EsysM.frmEsysm.getWidth()*0.64), 25, 160, 27);
				EsysM.txtTipoEjeX.setForeground(new Color(0, 128, 128));
				EsysM.btnAbajoTipo2.setSelected(false);
			}
		});
	}
	
	private void SelectTHD() {
	    EsysM.checTHD.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		if (EsysM.checTHD.isSelected()) {
	    			EsysM.textFreqTHD.setEnabled(true);
	    			EsysM.txtTHD.setEnabled(true);
	    		}
	    		else {
	    			EsysM.textFreqTHD.setEnabled(false);
	    			EsysM.txtTHD.setEnabled(false);
	    		}
	    	}
	    });
	}
	
	private void SelectCoherence() {
	    EsysM.checCoherence.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		if (EsysM.checCoherence.isSelected()) {
	    			EsysM.textFreqCoherence.setEnabled(true);
	    			EsysM.txtCoherence.setEnabled(true);
	    		}
	    		else {
	    			EsysM.textFreqCoherence.setEnabled(false);
	    			EsysM.txtCoherence.setEnabled(false);
	    		}
	    	}
	    });
	}

	public void MenuNuevo() {
		
		EsysM.NuevaSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String nameBusqueda = "Sesion 0";

				for (Sesion sesion:EsysM.contenido.getProyecto().getSesiones()) {
					
					char data= sesion.getNombre().charAt(sesion.getNombre().length()-1);
					if (Character.isDigit(data)) {
						int dato=Integer.parseInt(""+sesion.getNombre().charAt(sesion.getNombre().length()-1))+1;
						nameBusqueda = "Sesion "+dato;
					}
					else {
						nameBusqueda = "Sesion 0";
					}
					
				}
				
				EsysM.contenido.AgregarSesion(nameBusqueda, "#000000");
				
				Sesion sesion=EsysM.contenido.getSesion(nameBusqueda);
				
				nameBusqueda = "Grupo 0";

				if (sesion.getGrupos().size() > 0) {
					for (Grupo grupo : sesion.getGrupos()) {

						char data = grupo.getNombre().charAt(grupo.getNombre().length() - 1);
						if (Character.isDigit(data)) {
							int dato = Integer
									.parseInt("" + grupo.getNombre().charAt(grupo.getNombre().length() - 1)) + 1;
							nameBusqueda = "Grupo " + dato;
						}

					}
				} else {
					nameBusqueda = "Grupo 0";
				}
					
				EsysM.contenido.AgregarGrupo(nameBusqueda, sesion, "#000000");
				
				EsysM.contenido.sincronizarProyecto();
				
				int pos=CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones());
				
				EsysM.tree.setSelectionRow(pos+1);
				
				EsysM.contenido.abrirPath(pos+1);
				
				TreePath nameTree=EsysM.tree.getPathForRow(pos+1);
				TreeCont=nameTree.getPathCount(); 
			
				DataTree(nameTree);

			}
		});
		
		EsysM.NuevoGrupo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (!EsysM.tree.isSelectionEmpty() & sesion != null & (TreeCont==2 | TreeCont==3)) {

					String nameBusqueda = "Grupo 0";

					if (sesion.getGrupos().size() > 0) {
						for (Grupo grupo : sesion.getGrupos()) {

							char data = grupo.getNombre().charAt(grupo.getNombre().length() - 1);
							if (Character.isDigit(data)) {
								int dato = Integer
										.parseInt("" + grupo.getNombre().charAt(grupo.getNombre().length() - 1)) + 1;
								nameBusqueda = "Grupo " + dato;
							}

						}
					} else {
						nameBusqueda = "Grupo 0";
					}
						
					EsysM.contenido.AgregarGrupo(nameBusqueda, sesion, "#000000");
					EsysM.contenido.sincronizarProyecto();	
					
					int pos=CarpetaControl.Buscar(sesion.getNombre(), EsysM.contenido.getSesiones())+1;
					
					int posG=CarpetaControl.Buscar(nameBusqueda, sesion.getGrupos())+1;
					
					EsysM.contenido.abrirPath(pos);
					
					EsysM.tree.setSelectionRow(pos+posG);
					
					TreePath nameTree=EsysM.tree.getPathForRow(pos+posG);
					TreeCont=nameTree.getPathCount(); 
				
					DataTree(nameTree);
					
				} else {
					Alerta alerta;
					try {
						alerta = new Alerta('S');
						alerta.Mensaje("Por favor escoge una Sesi�n para ubicar tu grupo");
						alerta.setVisible(true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} // jdialog de alerta y ayuda

				}

			}
		});
		
		
		EsysM.btnNuevaM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				nuevaM = new NuevaMedicion();
				
				nuevaM.DispositivosS.removeAllItems();// Borrar todas las filas del JcomBox DISPOSITIVOS
				nuevaM.DispositivosD1.removeAllItems();
				nuevaM.DispositivosD2.removeAllItems();
				
				nuevaM.DispositivosS.setMaximumRowCount(dispositivosCap.size() + 1);
				nuevaM.DispositivosD1.setMaximumRowCount(dispositivosCap.size() + 1);
				nuevaM.DispositivosD2.setMaximumRowCount(dispositivosCap.size() + 1);

				nuevaM.DispositivosS.addItem("Dispositivos de Captura");
				nuevaM.DispositivosD1.addItem("Dispositivos de Captura");
				nuevaM.DispositivosD2.addItem("Dispositivos de Captura");
				
				nuevaM.Sesiones.removeAllItems();
				
				nuevaM.Sesiones.addItem("Seleccionar ");
				
				for (Sesion sesion : EsysM.contenido.getSesiones()) {
					nuevaM.Sesiones.addItem(sesion.getNombre());
				}

				if (dispositivosCap.size() == 0) {

					FiltroDisp filtro = new FiltroDisp(); // Objeto tipo FiltroDisp para acceder a dispositivos de
															// Reproducci�n o fuente

					try {
						filtro.Captura();
					} catch (LineUnavailableException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					nuevaM.DispositivosS.setMaximumRowCount(dispositivosCap.size() + 1);
					nuevaM.DispositivosD1.setMaximumRowCount(dispositivosCap.size() + 1);
					nuevaM.DispositivosD2.setMaximumRowCount(dispositivosCap.size() + 1);

				}

				for (int i = 0; i < dispositivosCap.size(); i++) {
					if (dispositivosCap.get(i).isHabilitado()) {
						nuevaM.DispositivosS.addItem(dispositivosCap.get(i).getNombre());
						nuevaM.DispositivosD1.addItem(dispositivosCap.get(i).getNombre());
						nuevaM.DispositivosD2.addItem(dispositivosCap.get(i).getNombre());
					}
				}
				
				nuevaM.setVisible(true);
				
				int pos=CarpetaControl.Buscar(NuevaMControl.sesion.getNombre(), EsysM.contenido.getSesiones())+1;
				
				int posG=CarpetaControl.Buscar(NuevaMControl.grupo.getNombre(), NuevaMControl.sesion.getGrupos())+1;
		
				int posM=CarpetaControl.Buscar(NuevaMControl.nombreM, NuevaMControl.grupo.getMediciones())+1;
				
				EsysM.contenido.abrirPath(pos);
				
				EsysM.contenido.abrirPath(pos+posG);
				
				EsysM.tree.setSelectionRow(pos+posG+posM);
				
				TreePath nameTree=EsysM.tree.getPathForRow(pos+posG+posM);
				TreeCont=nameTree.getPathCount(); 
			
				DataTree(nameTree);
				
			}
		});
		
	}

	public void NuevoCap() {
		
		EsysM.btnNuevaC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nameBusqueda = "Carpeta 0";

				if (EsysM.contenidoC.getCarpetas().size() > 0) {
					for (Carpeta carpeta : EsysM.contenidoC.getCarpetas()) {

						char data = carpeta.getNombre().charAt(carpeta.getNombre().length() - 1);
						if (Character.isDigit(data)) {
							int dato = Integer
									.parseInt("" + carpeta.getNombre().charAt(carpeta.getNombre().length() - 1)) + 1;
							nameBusqueda = "Carpeta " + dato;
						}

					}
				}

				EsysM.contenidoC.AgregarCarpeta(nameBusqueda, "#000000");
				EsysM.contenidoC.sincronizarProyecto();

				int pos = CarpetaControl.Buscar(nameBusqueda, EsysM.contenidoC.getCarpetas())+1;

				EsysM.treeC.setSelectionRow(pos);

				TreePath nameTreeC = EsysM.treeC.getPathForRow(pos);
				TreeContC = nameTreeC.getPathCount();

				DataTreeC(nameTreeC);
			
			}
		});
		
		EsysM.btnCaptura.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				int pos;
				TreePath nameTreeC;
				
				if (carpeta.getCapturas().size()>0) {
				pos = CarpetaControl.Buscar(carpeta.getNombre(), EsysM.contenidoC.getCarpetas()) + 1;
				
				EsysM.contenidoC.abrirPath(pos);

				EsysM.treeC.setSelectionRow(pos);

				nameTreeC = EsysM.treeC.getPathForRow(pos);
				TreeContC = nameTreeC.getPathCount();

				DataTreeC(nameTreeC);
				}
				
				if (!EsysM.treeC.isSelectionEmpty() & carpeta != null & TreeContC==2) {
					
				String nameBusqueda = " #0";

				if (carpeta.getCapturas().size() > 0) {
					for (Captura captura : carpeta.getCapturas()) {

						char data = captura.getNombre().charAt(captura.getNombre().length() - 1);
						if (Character.isDigit(data)) {
							int dato = Integer
									.parseInt("" + captura.getNombre().charAt(captura.getNombre().length() - 1)) + 1;
							nameBusqueda = " #" + dato;
						}

					}
				} else {
					nameBusqueda = " #0";
				}

					Random rand = new Random();

					float r = rand.nextFloat();
					float g = rand.nextFloat();
					float b = rand.nextFloat();

					Color randomColor = new Color(r, g, b);

					String color = ColorFrame.toHexString(randomColor);

					EsysM.contenidoC.AgregarCaptura(nameBusqueda, carpeta, color);
					EsysM.contenidoC.sincronizarProyecto();

					pos = CarpetaControl.Buscar(carpeta.getNombre(), EsysM.contenidoC.getCarpetas())+1 ;
					
					System.out.println(pos);
					
					int posM = CarpetaControl.Buscar(nameBusqueda, carpeta.getCapturas()) + 1;

					EsysM.contenidoC.abrirPath(pos);

					EsysM.treeC.setSelectionRow(pos+posM);

					nameTreeC = EsysM.treeC.getPathForRow(pos+posM);
					TreeContC = nameTreeC.getPathCount();

					DataTreeC(nameTreeC);
				
				} else {
					Alerta alerta;
					try {
						alerta = new Alerta('C');
						alerta.Mensaje("Por favor escoge una Carpeta para ubicar tu captura");
						alerta.setVisible(true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} // jdialog de alerta y ayuda

				}
				
			}
		});
	}
	
	private void Monitor() {
		EsysM.monitorS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				iniciadoMonitor=true;

				if (disp!=null) {
					if(disp.getMidiendo()) {
						disp.Detener();
					}
					if (medicion1!=null) {
						medicion1.activar(false);
					}
				}
				
				
				if (EsysM.MostrarMS.isVisible()) {

					CapturaAudio[] dispt = (CapturaAudio[]) medicion.getDisp();
					disp = dispt[1];
					char chanel = medicion.getCanalM();

					if (EsysM.monitorS.isSelected()) {

						medicion.activar(true);

						try {
							disp.Iniciar(false, chanel, CapturaAudio.Ventana.RECTANGULAR, CapturaAudio.Magnitud.DBFS,
									EsysM.dBM, EsysM.NivelM, false);
						} catch (LineUnavailableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					else {
						disp.Detener();
						medicion.activar(false);
					}

					medicion1=medicion;
				}
			}
		});
		
		
		EsysM.monitorD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				iniciadoMonitor=true;
				
				if (dispM != null) {

					if (dispM.equals(dispR)) {
						
						dispM.Detener();
					} else {
						dispM.Detener();
						dispR.Detener();
					}
				}
				
				if (medicion2 != null) {
					medicion2.activar(false);
				}
				
				
				
				if (EsysM.MostrarMD.isVisible()) {
					
					CapturaAudio[] disp=(CapturaAudio[]) medicion.getDisp();
					dispM= disp[1];
					dispR=disp[0];
					
					if (EsysM.monitorD.isSelected()) {

						char chanelR = medicion.getCanalR();
						char chanelM = medicion.getCanalM();
						
						medicion.activar(true);

						try {
							
							
							if (dispM.equals(dispR)) {
								
								
								dispM.Iniciar(true,chanelM, CapturaAudio.Ventana.RECTANGULAR, CapturaAudio.Magnitud.DBFS,
										EsysM.dBM2, EsysM.NivelM2, false);
								
								dispM.linea2(chanelR, EsysM.dBM1, EsysM.NivelM1);
								
								
							}else {
								
								System.out.println("diferente");
								
								dispM.Iniciar(false,chanelM, CapturaAudio.Ventana.RECTANGULAR, CapturaAudio.Magnitud.DBFS,
										EsysM.dBM2, EsysM.NivelM2, false);
								
								dispR.Iniciar(false,chanelR, CapturaAudio.Ventana.RECTANGULAR, CapturaAudio.Magnitud.DBFS,
										EsysM.dBM1, EsysM.NivelM1, false);
							}

						} catch (LineUnavailableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						
						if (dispM.equals(dispR)) {
							
							dispM.Detener();
							
						} else {
							dispM.Detener();
							dispR.Detener();
						}
						
						medicion.activar(false);
					}
					
					medicion2=medicion;
				}
				
			}
		});

	}
	
}



