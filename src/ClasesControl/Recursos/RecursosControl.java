package ClasesControl.Recursos;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ClasesControl.EsysMControl;
import ClasesEntidad.Recursos.CapturaAudio;
import ClasesEntidad.Recursos.Dispositivo;
import ClasesEntidad.Recursos.FiltroDisp;
import ClasesEntidad.Recursos.FuenteAudio;
import ClasesFrontera.Recursos.Alerta;
import ClasesFrontera.Recursos.Recursos;

public class RecursosControl {

	private FiltroDisp filtro;
	private char canal;
	private CapturaAudio dispC;
	private FuenteAudio dispR;
	
	
	public void filtrar() throws LineUnavailableException {
		filtro = new FiltroDisp();
		filtro.Captura();
		filtro.Reproduccion();
	}
	
	public void MostrarDisp(boolean vacio) {	

		Recursos.SelectCaptura.addActionListener(new ActionListener() { //Acci�n sobre dispositivos tipo Captura
		    	public void actionPerformed(ActionEvent e) {
		    		
		    		Recursos.btnProbar.setEnabled(true);
		    		
		    			if (Recursos.SelectCaptura.isSelected()) {
							
				    		Recursos.btnPlus.setEnabled(false);
				    		Recursos.btnMinus.setEnabled(false);
				    		
							Mostrar('C'); //Mostrar Disositivos de Captura
							
							Recursos.nCanales.setText("--");
							Recursos.nBits.setText("--");
							Recursos.nMuestreo.setText("--");
							
							Recursos.SelectReprod.setSelected(false);

							Recursos.SelectC.setEnabled(true);
						}
						else {		
							
				    		Recursos.btnPlus.setEnabled(true);
				    		Recursos.btnMinus.setEnabled(true);
							
							Mostrar('R');
							
							Recursos.SelectReprod.setSelected(true);
							
							Recursos.nCanales.setText("--");
							Recursos.nBits.setText("--");
							Recursos.nMuestreo.setText("--");

							Recursos.SelectC.setEnabled(true);
						}
		    		
		    	}
		    });
		 
		 Recursos.SelectReprod.addActionListener(new ActionListener() { //Acci�n sobre dispositivos de tipo Reproducci�n
			 public void actionPerformed(ActionEvent e) {
				 
				 Recursos.btnProbar.setEnabled(true);	
				 
		    			if (Recursos.SelectReprod.isSelected()) {
							
				    		Recursos.btnPlus.setEnabled(true);
				    		Recursos.btnMinus.setEnabled(true);
		    				
							Mostrar('R');
							
							Recursos.SelectCaptura.setSelected(false);
							
							if (vacio==true) {
							try {
								filtrar();
							} catch (LineUnavailableException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}}
							
							Recursos.nCanales.setText("--");
							Recursos.nBits.setText("--");
							Recursos.nMuestreo.setText("--");
							
							Recursos.SelectCaptura.setSelected(false);
							
							Recursos.SelectC.setEnabled(true);
						}
						else {
						
				    		Recursos.btnPlus.setEnabled(false);
				    		Recursos.btnMinus.setEnabled(false);
				    		
							Mostrar('C');
							
							Recursos.SelectCaptura.setSelected(true);
							
							if (vacio==true) {
							try {
								filtrar();
							} catch (LineUnavailableException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}}
							
							Recursos.nCanales.setText("--");
							Recursos.nBits.setText("--");
							Recursos.nMuestreo.setText("--");
							
							Recursos.SelectC.setEnabled(true);
							
						}
		    		
		    	}
		    });
		
	}

	public void MostrarCaracter() {

		Recursos.Dispositivos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Border borde = BorderFactory.createLineBorder(Color.white, 1);// Eliminar marca de ayuda si �sta ha sido
																				// activada
				Recursos.ErrorDisp.setBorder(borde);

				Recursos.RmsNivel.setText("--"); // Limpiar seccion de texto donde se muestran niveles Rms
				dBPIX.Inicial(Recursos.Niveles); // Limpiar canvas o barra de nivel

				if (Recursos.Dispositivos.getSelectedIndex() > 0) {
					
					Dispositivo disp;
					
					if (Recursos.SelectCaptura.isSelected()) {
						
						disp = EsysMControl.dispositivosCap.get(Recursos.Dispositivos.getSelectedIndex() - 1);
						boolean estado = true;

						if (disp.isHabilitado()) {// Se compara vector de estado de dispositivo con estado Activo
							estado = true;
						} else {
							estado = false;
						}

						Recursos.btnProbar.setEnabled(estado);
						Recursos.Hab_Des.setSelected(estado);
						
					} else {
						
						disp = EsysMControl.dispositivosRep.get(Recursos.Dispositivos.getSelectedIndex() - 1);
						boolean estado = true;

						if (disp.isHabilitado()) {
							estado = true;
						} else {
							estado = false;
						}

						Recursos.btnProbar.setEnabled(estado);
						Recursos.Hab_Des.setSelected(estado);// Se visualiza el estado del dispositivo seleccionado
					}
	
					Recursos.nCanales.setText(Integer.toString(disp.getnCanales()));// Se accede a canales de
																						// dispositivo
					Recursos.nBits.setText(Integer.toString(disp.getnBits()));// Se accede a profundidad de bits de
																					// dispositivo
					Recursos.nMuestreo.setText(String.valueOf(disp.getfMuestreo()));// Se accede a frecuencia de
																						// muestreo de dispositivo
					Recursos.SelectC.removeAllItems();// Borrar todas las filas del JcomBox DISPOSITIVOS

					Recursos.SelectC.setMaximumRowCount(disp.getnCanales() + 1);

					Recursos.SelectC.addItem("Canal: R");
					Recursos.SelectC.addItem("Canal: L");		
						
				}

			}
		});
	}
	
	public void SelectCanal() {

		Recursos.SelectC.addActionListener(new ActionListener() { // Acci�n sobre canal R del dispositivo seleccionado
			public void actionPerformed(ActionEvent e) {

				Recursos.RmsNivel.setText("--");
				dBPIX.Inicial(Recursos.Niveles);// Se dibuja sobre el canvas el estado en reposo
				
				if (Recursos.SelectC.getSelectedIndex()==0) {
					canal = 'R';

					Border borde = BorderFactory.createLineBorder(Color.white, 1);// Borrar marca de ayuda si �sta se ha
																					// activado.
					Recursos.ErrorCanal.setBorder(borde);

				} else {
					
					canal = 'L';
				}

			}
		});
	}
	
	public void Probar() {

		Recursos.btnProbar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (Recursos.Dispositivos.getSelectedIndex() > 0) {// Validaci�n de que un dispositivo ha sido
																	// seleccionado

					if (Recursos.btnProbar.isSelected() && Recursos.SelectCaptura.isSelected()) {

						if (Recursos.SelectC.getSelectedIndex()==0 || Recursos.SelectC.getSelectedIndex()==1) {// Validaci�n de que un canal es seleccionado

							try {

								Capturar(true);

							} catch (LineUnavailableException e) {
								e.printStackTrace();
							}

						} else {

							try {
								Alerta alerta = new Alerta('P');// jdialog de alerta y ayuda
								alerta.CaracteristicasP(Recursos.ErrorCanal);
								alerta.Mensaje("Por favor escoge un Canal para realizar la prueba.");
								alerta.setVisible(true);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							Recursos.btnProbar.setSelected(false);
						}

					} else if (!Recursos.btnProbar.isSelected() && Recursos.SelectCaptura.isSelected()) {

						try {
							Capturar(false);
						} catch (LineUnavailableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (Recursos.btnProbar.isSelected() && Recursos.SelectReprod.isSelected()) {
						try {

							Reproducir(true);

						} catch (LineUnavailableException e) {
							e.printStackTrace();
						}
					} else if (!Recursos.btnProbar.isSelected() && Recursos.SelectReprod.isSelected()) {

						try {
							Reproducir(false);
						} catch (LineUnavailableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} else {

					try {
						Alerta alerta = new Alerta('P');
						alerta.CaracteristicasP(Recursos.ErrorDisp);
						alerta.Mensaje("Por favor escoge un dispositivo para realizar la prueba.");
						alerta.setVisible(true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Recursos.btnProbar.setSelected(false);
				}

			}
		});

	}
	
	public void On_Off() {
		Recursos.Hab_Des.addMouseListener(new MouseAdapter() { // Accion cuando el mouse ha hecho clic en habilitar y
																// deshabilitar
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (Recursos.Dispositivos.getSelectedIndex() > 0) {

					if (Recursos.SelectCaptura.isSelected() && Recursos.Hab_Des.isSelected()) {
						
						Dispositivo disp=EsysMControl.dispositivosCap.get(Recursos.Dispositivos.getSelectedIndex()-1);
						
						disp.setHabilitado(true);
						Recursos.btnProbar.setEnabled(true);
						
					} else if (Recursos.SelectCaptura.isSelected() && !Recursos.Hab_Des.isSelected()) {
						
						Dispositivo disp=EsysMControl.dispositivosCap.get(Recursos.Dispositivos.getSelectedIndex()-1);
						
						disp.setHabilitado(false);
						Recursos.btnProbar.setEnabled(false);
						
					} else if (Recursos.SelectReprod.isSelected() && Recursos.Hab_Des.isSelected()) {
						
						Dispositivo disp=EsysMControl.dispositivosRep.get(Recursos.Dispositivos.getSelectedIndex()-1);
						
						disp.setHabilitado(true);
						Recursos.btnProbar.setEnabled(true);
						Recursos.btnMinus.setEnabled(true);
						Recursos.btnPlus.setEnabled(true);
						
					} else if (Recursos.SelectReprod.isSelected() && !Recursos.Hab_Des.isSelected()) {
						
						Dispositivo disp=EsysMControl.dispositivosRep.get(Recursos.Dispositivos.getSelectedIndex()-1);
						
						disp.setHabilitado(false);
						Recursos.btnProbar.setEnabled(false);
						Recursos.btnMinus.setEnabled(false);
						Recursos.btnPlus.setEnabled(false);
					}
				} else {

					try {
						Alerta alerta = new Alerta('P');// jdialog de alerta y ayuda
						alerta.CaracteristicasP(Recursos.ErrorDisp);
						alerta.Mensaje("Por favor escoge un dispositivo para encender o apagar.");
						alerta.setVisible(true);
						Recursos.Hab_Des.setSelected(false);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void Reproducir(boolean estado) throws LineUnavailableException {
		dispR=EsysMControl.dispositivosRep.get(Recursos.Dispositivos.getSelectedIndex() - 1);
		if (estado==true) {
			dispR.Iniciar(canal);}
			else {
			dispR.Detener();
			}
		
		Recursos.btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispR.Master(1);
			}
		});
		
		 Recursos.btnMinus.addMouseListener(new MouseAdapter() {
		    	@Override
		    	public void mousePressed(MouseEvent e) {
		    			dispR.Master(-1);	
		    	}
		    });

	}
	
	private void Capturar(boolean estado) throws LineUnavailableException {
		dispC=EsysMControl.dispositivosCap.get(Recursos.Dispositivos.getSelectedIndex() - 1);
		if (estado==true) {
		dispC.Iniciar(false,canal, CapturaAudio.Ventana.RECTANGULAR,CapturaAudio.Magnitud.DBFS,Recursos.RmsNivel,Recursos.Niveles, true);}
		else {
		dispC.Detener();
		}
	}
	
	private void Mostrar(char Tipo) {

		Recursos.Dispositivos.removeAllItems();//Borrar todas las filas del JcomBox DISPOSITIVOS

		if (Tipo=='C') {
			Recursos.Dispositivos.addItem("Dispositivos de Captura"); // Primera fila
			Recursos.Dispositivos.setMaximumRowCount(EsysMControl.dispositivosCap.size()+1);//Establecer el maximo de filas que se ver�n en el JcomBox DISPOSITIVOS

			for (int i = 0; i < EsysMControl.dispositivosCap.size(); i++) {
			Recursos.Dispositivos.addItem(EsysMControl.dispositivosCap.get(i).getNombre());//Agregar items de los nombres de dispositivos almacenados a JcomBox DISPOSITIVOS
			}	
		}
		else {
			Recursos.Dispositivos.addItem("Dispositivos de Reproducci�n"); // Primera fila
			Recursos.Dispositivos.setMaximumRowCount(EsysMControl.dispositivosRep.size()+1);//Establecer el maximo de filas que se ver�n en el JcomBox DISPOSITIVOS

			for (int i = 0; i < EsysMControl.dispositivosRep.size(); i++) {
				Recursos.Dispositivos.addItem(EsysMControl.dispositivosRep.get(i).getNombre());//Agregar items de los nombres de dispositivos almacenados a JcomBox DISPOSITIVOS
			}	
		}
		
		
	}
		
	public void Cerrar() {
		
		Recursos.btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (Recursos.btnProbar.isSelected()) {
					if (Recursos.SelectCaptura.isSelected()) {
						dispC.Detener();
					} else {
						dispR.Detener();
					}
				}
				
				try {
					EsysMControl.DispReproducir();
				} catch (LineUnavailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Recursos.frmRecursosDeAudio.setVisible(false);
			}
		});

	}
	
	
}
