package ClasesControl.Recursos;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import ClasesFrontera.Recursos.Recursos;

public class dBPIX {

	private double Voltdata;
	private int pix;
	private Canvas Nivel;
	
	public dBPIX(double Voltdata, Canvas Nivel) {
		this.Voltdata=Voltdata; //Nivel en voltaje que se va a graficar en canva
		this.Nivel=Nivel; //Canva donde se va a graficar el nivel
	}

	public void PixdBFs() {
		pix=(int)(Voltdata+114); //Funci�n de conversi�n de dBFs a pix en una barra de 114pix de ancho
		niveles();
	}
	
	public void PixdBspl() {
		pix=(int) ((1.1521*Voltdata)+16.469);//Funci�n de conversi�n de dBspl a pix en una barra de 114pix de ancho
		niveles();
	}
	
	public void PixdBu() {
		pix=(int) ((1.0012*Voltdata)+88.854);//Funci�n de conversi�n de dBu a pix en una barra de 114pix de ancho
		niveles();
	}
	
	public void PixdBV() {
		pix=(int) ((1.0012*Voltdata)+91.956);//Funci�n de conversi�n de dBV a pix en una barra de 114pix de ancho
		niveles();
	}
	
	private void niveles() {	
		nivel GraficarNiveles= new nivel();	//Declarar hilo para graficar en canva
		GraficarNiveles.start();		//Inicio de hilo
	}

	private class nivel extends Thread { //Clase para graficar los niveles en el canva

		private BufferedImage Imagebuffer; //Imagen de buffer - es decir de almacenamiento corto-
		private Graphics Imagenew; 			//Parametros gr�ficos de la Imagen Buffer
		private Graphics h = Nivel.getGraphics();

		public void run() {

			int Ancho = Nivel.getWidth();
			int Alto = Nivel.getHeight();

			Imagebuffer = new BufferedImage(Ancho, Alto, BufferedImage.TYPE_3BYTE_BGR); //Genera una nueva Imagen buffer con alto y ancho del canva y de tipo RGB
			Imagenew = Imagebuffer.getGraphics(); //Se obtienen par�metros gr�ficos de la imagen Buffer

			update(h);
		}

		private void update(Graphics g) {

			int lim = 94;
			
			if (Nivel.getHeight() > Nivel.getWidth()) {
				Imagenew.setColor(Color.WHITE); // Se dibuja en los parametros graficos de la iamgen buffer
				Imagenew.fillRect(1, Nivel.getHeight()-(pix+2), Nivel.getWidth(), 2); // Rectangulo muy delgado an�logo a una l�nea de marca

				Imagenew.setColor(Color.RED);
				Imagenew.fillRect(1, Nivel.getHeight()-105, Nivel.getWidth(), 2); // Marca de nivel m�ximo

				Imagenew.setColor(Color.yellow);
				Imagenew.fillRect(1, Nivel.getHeight()-lim, Nivel.getWidth(), 2); // Marca de nivel alto

				if (pix >= (lim + 11)) {
					Imagenew.setColor(Color.RED);
					Imagenew.fillRect(1, Nivel.getHeight()-pix, Nivel.getWidth(), pix);
				} else if (pix < (lim + 11) && pix >= (lim)) {
					Imagenew.setColor(Color.YELLOW);
					Imagenew.fillRect(1, Nivel.getHeight()-pix, Nivel.getWidth(), pix);
				} else if (pix < (lim )) {
					Imagenew.setColor(Color.GREEN);
					Imagenew.fillRect(1, Nivel.getHeight()-pix , Nivel.getWidth(), pix);
				}

				g.drawImage(Imagebuffer, 0, 0, null); // Plasmar los par�metros gr�ficos dibujados en los parametros del
														// canva

			} else {
				Imagenew.setColor(Color.WHITE); // Se dibuja en los parametros graficos de la iamgen buffer
				Imagenew.fillRect(pix, 1, 2, Nivel.getHeight()); // Rectangulo muy delgado an�logo a una l�nea de marca

				Imagenew.setColor(Color.RED);
				Imagenew.fillRect(110, 1, 2, Nivel.getHeight()); // Marca de nivel m�ximo

				Imagenew.setColor(Color.yellow);
				Imagenew.fillRect(lim, 1, 2, Nivel.getHeight()); // Marca de nivel alto

				if (pix >= (lim + 16)) {
					Imagenew.setColor(Color.RED);
					Imagenew.fillRect(1, 1, pix, Nivel.getHeight());
				} else if (pix < (lim + 16) && pix >= (lim + 1)) {
					Imagenew.setColor(Color.YELLOW);
					Imagenew.fillRect(1, 1, pix, Nivel.getHeight());
				} else if (pix < (lim + 1)) {
					Imagenew.setColor(Color.GREEN);
					Imagenew.fillRect(1, 1, pix, Nivel.getHeight());
				}

				g.drawImage(Imagebuffer, 0, 0, null); // Plasmar los par�metros gr�ficos dibujados en los parametros del
														// canva
			}
		}

		
		
	}

	public static void Inicial(Canvas Nivel) {// Clase para limpiar el canva en estado de reposo
		
		BufferedImage Imagebuffer;
		Graphics Imagenew;
		Graphics h = Nivel.getGraphics();
		
		int Ancho = Nivel.getWidth();
		int Alto = Nivel.getHeight();

		Imagebuffer = new BufferedImage(Ancho, Alto, BufferedImage.TYPE_3BYTE_BGR);
		Imagenew = Imagebuffer.getGraphics();
		
		if (Nivel.getHeight() > Nivel.getWidth()) {
			Imagenew.setColor(Color.BLACK);
			Imagenew.fillRect(1, 1, 10, Nivel.getHeight());

			Imagenew.setColor(Color.RED);
			Imagenew.fillRect(1, Nivel.getHeight()-100, Nivel.getWidth(), 2); // Marca de nivel m�ximo

			Imagenew.setColor(Color.yellow);
			Imagenew.fillRect(1, Nivel.getHeight()-94, Nivel.getWidth(), 2); // Marca de nivel alto

			h.drawImage(Imagebuffer, 0, 0, null);
		} else {
			Imagenew.setColor(Color.BLACK);
			Imagenew.fillRect(1, 1, 10, Nivel.getHeight());

			Imagenew.setColor(Color.RED);
			Imagenew.fillRect(110, 1, 2, Nivel.getHeight());

			Imagenew.setColor(Color.yellow);
			Imagenew.fillRect(94, 1, 2, Nivel.getHeight());

			h.drawImage(Imagebuffer, 0, 0, null);
		}
	}
	
	
	
	
}
