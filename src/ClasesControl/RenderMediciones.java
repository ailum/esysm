package ClasesControl;

import javax.swing.tree.*;
import javax.swing.*;
import java.awt.*;

import ClasesEntidad.Medicion;
import ClasesEntidad.Proyecto;
import ClasesEntidad.Sesion;
import ClasesEntidad.Grupo;

public class RenderMediciones extends DefaultTreeCellRenderer {

	/**
	 * 
	 */
	private boolean alerta;
	private char tipo;
	private static final long serialVersionUID = 1L;

	public RenderMediciones(boolean alerta,char tipo){
		this.alerta=alerta;
		this.tipo=tipo;
	}
	/**
	 * 
	 */

	   @Override
	    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
	    boolean leaf, int row, boolean hasFocus) {
	        super.getTreeCellRendererComponent(tree, value, selected,expanded, leaf, row, hasFocus);
	        //altura de cada nodo
	        tree.setRowHeight(20);

	        setOpaque(true);     
	        //color de texto
	        setForeground( Color.black );
	        if( selected )
	            setForeground( Color.decode("#008080"));        
	        //-- Asigna iconos
	        // si value es la raiz
				if (tree.getModel().getRoot().equals((DefaultMutableTreeNode) value)) {
					// setIcon( ((Proyecto)((DefaultMutableTreeNode)
					// value).getUserObject()).getIcon() );
				} else if (((DefaultMutableTreeNode) value).getUserObject() instanceof Sesion) {
					if (tipo == 'S' & alerta == true) {
						setForeground(Color.red);
					} else {
						setForeground(Color.black);
					}
		
					if (selected)
						setForeground(Color.decode("#008080"));
		
					setIcon(((Sesion) ((DefaultMutableTreeNode) value).getUserObject()).getIcon());
				} else if (((DefaultMutableTreeNode) value).getUserObject() instanceof Grupo) {
		
					if (tipo == 'G' & alerta == true) {
						setForeground(Color.red);
					} else {
						setForeground(Color.black);
					}
		
					if (selected)
						setForeground(Color.decode("#008080"));
		
					setIcon(((Grupo) ((DefaultMutableTreeNode) value).getUserObject()).getIcon());
				} else if (((DefaultMutableTreeNode) value).getUserObject() instanceof Medicion) {
					setIcon(((Medicion) ((DefaultMutableTreeNode) value).getUserObject()).getIcon());
				}
	        
	        return this;
	}
	
}


