package ClasesFrontera;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;

public class Inicio extends JDialog {

	/**
	 * Launch the application.
	 */
	Timer t = new Timer();
	private JProgressBar progress1, progress2;
	private String dir;
	
	public static void main(String[] args) {
		try {
			Inicio dialog = new Inicio();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);			
		
			dialog.setVisible(true);
			dialog.iniciar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setDireccion(String dir) {
		this.dir=dir;
	}
	
	private void cerrar() {
		this.setVisible(false);
	}
	
	public void iniciar() {
		TimerTask task = new TimerTask() {

			int v=0;
			float value;
			@Override
			public void run() {
				if (progress1.getValue()<100 && value<1.0f) {
					progress1.setValue(v);
					progress2.setValue(v);
					
					setOpacity(value);
					
					value+=0.02f;
					
					v+=2;}
				else {
					
					EsysM esysm=new EsysM(dir);
					esysm.frmEsysm.setVisible(true);
					
					cerrar();
					t.cancel();}
			}
		};
		// Empezamos dentro de 10ms y luego lanzamos la tarea cada 1000ms
		t.schedule(task, 10, 200);
	}
	
	/**
	 * Create the dialog.
	 */
	public Inicio() {
		setResizable(false);
		setBackground(Color.WHITE);
		getContentPane().setBackground(Color.WHITE);
		setBounds(100, 100, 335, 388);
		getContentPane().setLayout(null);
		
		setUndecorated(true);
		
		JButton Inicio = new JButton("");
		Inicio.setBackground(Color.WHITE);
		Inicio.setBounds(0, 0, 335, 404);
		getContentPane().add(Inicio);
		
		
		ImageIcon img= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/LOGOprincipal.png"));
		ImageIcon imgRender=new ImageIcon(img.getImage().getScaledInstance(Inicio.getWidth(), Inicio.getHeight(), java.awt.Image.SCALE_DEFAULT));
		
		Inicio.setIcon(imgRender);
		Inicio.setBorderPainted(false);
		
		Inicio.setBorder(null);
		
		Inicio.setOpaque(false);
		
		progress1 = new JProgressBar();
		progress1.setEnabled(true);
		progress1.setBorder(null);
		progress1.setBackground(Color.WHITE);
		progress1.setBounds(4, 4, 325, 14);
		getContentPane().add(progress1);
		
		progress2 = new JProgressBar();
		progress2.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		progress2.setEnabled(true);
		progress2.setBorder(null);
		progress2.setBackground(Color.WHITE);
		progress2.setBounds(4, 379, 325, 5);
		getContentPane().add(progress2);
		
		
	}
}
