package ClasesFrontera;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Panel;
import javax.swing.JTextPane;

import ClasesControl.RenderMediciones;

import java.awt.Canvas;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;


public class Mediciones extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//A continuaci�n creamos una funci�n dentro del mismo JInternalFrame como el ejemplo siguiente:
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mediciones frame = new Mediciones();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	    
	/**
	 * Create the frame.
	 */
	public Mediciones() {
		setClosable(true);
		setBorder(null);
		setTitle("Mediciones");
		setIconifiable(true);
		setForeground(Color.WHITE);
		setFrameIcon(null);
		setBounds(100, 100, 170, 300);
		getContentPane().setLayout(null);
				
		//((javax.swing.plaf.basic.BasicInternalFrameUI)this.getUI()).setNorthPane(null);
		
		Panel porDefecto = new Panel();
		porDefecto.setBackground(new Color(255, 255, 255));
		porDefecto.setBounds(0, 0, 170, 273);
		getContentPane().add(porDefecto);
		porDefecto.setLayout(null);

		
		JPanel BarraSuperior = new JPanel();
		BarraSuperior.setBounds(0, 0, 170, 27);
		porDefecto.add(BarraSuperior);
		BarraSuperior.setBackground(new Color(0, 128, 128));
		BarraSuperior.setLayout(null);
		

		
		JButton Minimizar = new JButton("");
		Minimizar.setBackground(new Color(0, 128, 128));
		Minimizar.setIcon(new ImageIcon(Mediciones.class.getResource("/ClasesFrontera/Recursos/Imagenes/minimizar.png")));
		Minimizar.setBounds(144, 5, 16, 16);
		BarraSuperior.add(Minimizar);
		ImageIcon MiniOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/minimizar.png"));
		ImageIcon MiniEscOff=new ImageIcon( MiniOff.getImage().getScaledInstance(Minimizar.getWidth(), Minimizar.getHeight(), java.awt.Image.SCALE_SMOOTH));
		Minimizar.setIcon(MiniEscOff);
		
		JTextPane txtpnMediciones = new JTextPane();
		txtpnMediciones.setForeground(new Color(255, 255, 255));
		txtpnMediciones.setBackground(new Color(0, 128, 128));
		txtpnMediciones.setEditable(false);
		txtpnMediciones.setText("Mediciones");
		txtpnMediciones.setBounds(10, 5, 76, 20);
		BarraSuperior.add(txtpnMediciones);
	
		
		Canvas canvas = new Canvas();
		canvas.setBackground(new Color(0, 128, 128));
		canvas.setBounds(167, 33, 3, 240);
		porDefecto.add(canvas);
		
		JTree tree = new JTree();
		tree.setBounds(10, 38, 110, 106);
		
		DefaultTreeModel def = (DefaultTreeModel)tree.getModel();
		
		DefaultMutableTreeNode Principal = new DefaultMutableTreeNode("Carpeta");
		
        DefaultMutableTreeNode Sesion = new DefaultMutableTreeNode("Sesion");

        DefaultMutableTreeNode Grupo = new DefaultMutableTreeNode("Grupo");

        DefaultMutableTreeNode Medicion= new DefaultMutableTreeNode("Medicion I");

        Principal.add(Sesion);
        
        Sesion.add(Grupo);

        Grupo.add(Medicion);

        def.setRoot(Principal);
        
        tree.setModel(def);

        tree.setCellRenderer(new RenderMediciones(false,'.'));
        
		porDefecto.add(tree);

		/*DefaultTreeCellRenderer render= (DefaultTreeCellRenderer)tree.getCellRenderer();	
		render.setOpenIcon(new ImageIcon("Imagenes/carpeta.png"));
		render.setClosedIcon(new ImageIcon("Imagenes/carpeta.png"));*/

	
	}
}
