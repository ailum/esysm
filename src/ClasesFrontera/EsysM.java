package ClasesFrontera;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import java.awt.Color;
import javax.swing.JMenuBar;
import java.io.File;
import java.io.IOException;

import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.HeadlessException;

import javax.swing.JMenu;
import javax.swing.JTree;
import java.awt.CardLayout;

import ClasesControl.CapturaControl;
import ClasesControl.CarpetaControl;
import ClasesControl.EsysMControl;
import ClasesControl.RenderCapturas;
import ClasesControl.RenderMediciones;
import ClasesEntidad.Proyecto;
import ClasesEntidad.Sesion;
import ClasesFrontera.Recursos.Recursos;

import javax.swing.JDesktopPane;
import javax.swing.JButton;

import java.awt.Canvas;
import javax.swing.border.LineBorder;
import javax.swing.JComboBox;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import javax.swing.border.MatteBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.Scrollbar;
import java.awt.Toolkit;

import javax.swing.ScrollPaneConstants;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

public class EsysM {

	public static JFrame frmEsysm;
	public static JDesktopPane Escritorio;
	public static Canvas ColorM;
	public static String[] coloresM= new String[500];
	public static JButton Minimizar;
	public static JPanel porDefecto ;
	public static JTree tree;
	public static CarpetaControl contenido;
	public static JPanel MostrarMS;
	public static JMenuBar menuBar;
	public static int anchoIni,altoIni, posMostrarx,posMostrary,widMostrar,heMostrar ;
	public static JPanel Barradivision1 ;
	public static JPanel GeneradorPanel;
	public static JButton btnGenerador;
	public static JPanel BarraGenerador;
	public static JComboBox<Object> DispGenerador;
	public static JToggleButton btnAbajoTipo;
	public static JPanel TipoPanel;
	public static JPanel TipoPanelOpcion;
	public static JTextPane txtTipoDeMedicion;
	public static JTextPane txtTipoMagnitud;
	public static JToggleButton btnAbajoTipo1;
	public static  JPanel MagPanel;
	public static JPanel MagPanelOpcion;
	public static  JPanel EjexPanel;
	public static JTextPane txtTipoEjeX;
	public static JToggleButton btnAbajoTipo2;
	public static JPanel HzPanelOpcion;
	public static JTextPane txtRta,txtFuncionDeTransferencia,txtEspectrograma;
	public static JTextPane txtdBFs,txtdBspl,txtdBu,txtdBV;
	public static JTextPane txtLineal, txtLog;
	public static JPanel Grafica;
	public static JPanel MostrarMD;
	public static Canvas ColorM2;
	public static JTextField NombreM;
	public static JTextField NombreMD;
	public static JScrollPane qPane;
	public static JPanel Monitoreo;
	public static JTextField txtNombreSG;
	public static JButton btnBorrarSG;
	public static JPanel SesionGrupoPanel;
	public static JPanel THDpanel;
	public static JCheckBox checTHD;
	public static JTextPane txtTHD;
	public static JTextField textFreqTHD;
	public static JPanel CoherencePanel;
	public static JCheckBox checCoherence;
	public static JTextField textFreqCoherence;
	public static JTextPane txtCoherence;
	public static Canvas ColorSG;
	public static JTree treeC;
	public static JScrollPane qPaneC;
	public static JPanel BarrasupDerecha;
	public static JMenuItem NuevaSesion;
	public static JMenuItem NuevoGrupo;
	public static JButton btnNuevaC;
	public static CapturaControl contenidoC;
	public static JMenuItem btnNuevaM;
	public static JButton btnCaptura;
	public static JTextPane ngrupostxt;
	public static JTextPane nmedicionestxt;
	public static JButton BorrarCap;
	public static JButton ColorC;
	public static JTextPane DispositivoM, ReferenciaData;
	public static JTextPane CanalM;
	public static JTextPane MedicionData;
	public static JButton borrarMS,borrarMD;
	public static JToggleButton monitorS, monitorD ;
	public static Canvas NivelM, NivelM1, NivelM2;
	public static JTextPane dBM,dBM1,dBM2;
	
	private boolean newproyect=true;
	
	int n = 0;
	public static String direccion;
	
	private EsysM window;


	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EsysM window = new EsysM("");
					EsysM.frmEsysm.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EsysM(String dir) {
		direccion=dir;
		initialize();
	}
	
	private void cerrar() {
		EsysM.frmEsysm.setVisible(false);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		coloresM[0]="#000000";
		
		frmEsysm = new JFrame();
		frmEsysm.setForeground(new Color(0, 128, 128));
		frmEsysm.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		frmEsysm.setTitle("EsysM 2.1");
		frmEsysm.setBounds(100, 100, 1024, 542);
		frmEsysm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEsysm.getContentPane().setLayout(new CardLayout(0, 0));
		
		anchoIni=frmEsysm.getWidth();
		altoIni=frmEsysm.getHeight();
		
		Escritorio = new JDesktopPane();
		Escritorio.setBackground(Color.WHITE);
		frmEsysm.getContentPane().add(Escritorio, "name_99334876517543");
		
		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1008, 26);
		Escritorio.add(menuBar);
		menuBar.setBackground(Color.WHITE);
		
		ImageIcon MaxOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/mas.png"));
	    ImageIcon PlusOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/plus.png"));
	    ImageIcon MinusOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/minus.png"));	
		ImageIcon PlayOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/Play.png"));
		ImageIcon PlayOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/Stop.png"));
	    ImageIcon MiniOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/minimizar.png"));
	    ImageIcon AbajoOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/AbajoAbrir.png"));
	    ImageIcon ArribaOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/ArribaCerrar.jpg"));
	    ImageIcon AbajoOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/AbajoOver.png"));
	    ImageIcon Eliminar= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/eliminar.png"));
	    ImageIcon IniciarMed= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonIniciar.png"));
	    ImageIcon DetenerMed= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonDetenerMed.png"));
	    ImageIcon NuevaC= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/nuevacarpeta.png"));
	    ImageIcon Captura= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/captura.png"));
	    ImageIcon CapturaOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/capturaOn.png"));
	    ImageIcon AdministraSGOFF= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/menuOff.png"));
	    ImageIcon AdministraSGON= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/menuOn.png"));
	    ImageIcon IniciarMedOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonIniciarOn.png"));
	    ImageIcon EditarcapOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/editarOff.png"));
	    ImageIcon EditarcapOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/editar.png"));
	    ImageIcon playmonitor= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/Play.png"));
	    ImageIcon stopmonitor= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/Stop.png"));
	    
		
		JMenu mnArchivo = new JMenu("Archivo");		
		mnArchivo.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/carpeta.png")));
		mnArchivo.setBackground(Color.WHITE);
		mnArchivo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		menuBar.add(mnArchivo);
		
		
		JMenu mnAbrir = new JMenu("Abrir");
		//mnAbrir.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/Abrir.png")));
		mnAbrir.setOpaque(true);		
		mnAbrir.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mnAbrir.setBackground(Color.WHITE);
		mnArchivo.add(mnAbrir);
		
		JMenuItem msesion = new JMenuItem("Sesi\u00F3n de Medici\u00F3n");
		msesion.setOpaque(true);		
		msesion.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		msesion.setBackground(Color.WHITE);
		mnAbrir.add(msesion);
		
		JMenuItem mmedicion = new JMenuItem("Medici\u00F3n Individual");
		mmedicion.setOpaque(true);
		mmedicion.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mmedicion.setBackground(Color.WHITE);
		mnAbrir.add(mmedicion);
		
		JMenuItem mGuardar = new JMenuItem("Guardar");
		//mGuardar.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/guardar.png")));
		mGuardar.setOpaque(true);
		mGuardar.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mGuardar.setBackground(Color.WHITE);
		mnArchivo.add(mGuardar);
		
		JMenuItem mGuardarComo = new JMenuItem("Guardar como...");
		//mGuardarComo.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/guardar como.png")));
		mGuardarComo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mGuardarComo.setBackground(Color.WHITE);
		mnArchivo.add(mGuardarComo);
		
		JMenuItem mAdministrador = new JMenuItem("Administrador");
		mAdministrador.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mAdministrador.setBackground(Color.WHITE);
		mnArchivo.add(mAdministrador);
		
		JMenuItem mSalir = new JMenuItem("Salir");
		mSalir.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mSalir.setBackground(Color.WHITE);
		mnArchivo.add(mSalir);
		
		JMenu mnNuevo = new JMenu("Nuevo");
		mnNuevo.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/nuevo.png")));
		mnNuevo.setBackground(Color.WHITE);
		mnNuevo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		menuBar.add(mnNuevo);
		
		JMenuItem mntmProyecto = new JMenuItem("Proyecto");
		mntmProyecto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CarpetaInicio iniciar;
				try {
					iniciar = new CarpetaInicio(newproyect, direccion);
					Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
					Dimension ventana = iniciar.getSize();
					iniciar.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);

					iniciar.setVisible(true);
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				cerrar();
			}
		});
		mntmProyecto.setBackground(new Color(255, 255, 255));
		mntmProyecto.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mnNuevo.add(mntmProyecto);
		
		NuevaSesion = new JMenuItem("Sesi\u00F3n de Medici\u00F3n");
		NuevaSesion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		NuevaSesion.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		NuevaSesion.setBackground(Color.WHITE);
		mnNuevo.add(NuevaSesion);
		
		NuevoGrupo = new JMenuItem("Grupo de Mediciones");
		NuevoGrupo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
		NuevoGrupo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		NuevoGrupo.setBackground(Color.WHITE);
		mnNuevo.add(NuevoGrupo);
		
		btnNuevaM = new JMenuItem("Medicion Nueva");
		btnNuevaM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		btnNuevaM.setBackground(Color.WHITE);
		
		btnNuevaM.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mnNuevo.add(btnNuevaM);
		
		JMenuItem mntmCalibracin = new JMenuItem("Nueva Calibraci\u00F3n");
		mntmCalibracin.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mntmCalibracin.setBackground(Color.WHITE);
		mnNuevo.add(mntmCalibracin);
		
		JMenu mnOpciones = new JMenu("Opciones");
		mnOpciones.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/Opciones.png")));
		mnOpciones.setBackground(Color.WHITE);
		mnOpciones.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		menuBar.add(mnOpciones);
		
		JMenuItem mntmConfiguracinGeneral = new JMenuItem("Configuraci\u00F3n General");
		mntmConfiguracinGeneral.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mntmConfiguracinGeneral.setBackground(Color.WHITE);
		mnOpciones.add(mntmConfiguracinGeneral);
		
		JMenuItem mntmAdministradorDeCapturas = new JMenuItem("Administrador de Capturas");
		mntmAdministradorDeCapturas.setBackground(Color.WHITE);
		mntmAdministradorDeCapturas.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mnOpciones.add(mntmAdministradorDeCapturas);
		
		JMenuItem mntmAdministradorDeMediciones = new JMenuItem("Administrador de Mediciones");
		mntmAdministradorDeMediciones.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mntmAdministradorDeMediciones.setBackground(Color.WHITE);
		mnOpciones.add(mntmAdministradorDeMediciones);
		
		JMenuItem mntmAdministradorDeCalibraciones = new JMenuItem("Administrador de Calibraciones");
		mntmAdministradorDeCalibraciones.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mntmAdministradorDeCalibraciones.setBackground(Color.WHITE);
		mnOpciones.add(mntmAdministradorDeCalibraciones);
		
		JMenu mnUtilidades = new JMenu("Utilidades");
		mnUtilidades.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/Utilidades.png")));
		mnUtilidades.setBackground(Color.WHITE);
		mnUtilidades.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		menuBar.add(mnUtilidades);
		
		JMenuItem mntmRecursosDeAudio = new JMenuItem("Recursos de Audio");
		mntmRecursosDeAudio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Recursos rec;
				try {
					rec = new Recursos();
					rec.frmRecursosDeAudio.setVisible(true);
				} catch (HeadlessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});

		mntmRecursosDeAudio.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		mntmRecursosDeAudio.setBackground(Color.WHITE);
		mnUtilidades.add(mntmRecursosDeAudio);
		
		JPanel BarraSuperiorMed = new JPanel();
		BarraSuperiorMed.setBounds(0, 25, 180, 27);
		Escritorio.add(BarraSuperiorMed);
		BarraSuperiorMed.setLayout(null);
		BarraSuperiorMed.setBackground(new Color(0, 128, 128));
		
		porDefecto = new JPanel();
		porDefecto.setBackground(new Color(255, 255, 255));
		porDefecto.setBounds(0, 52, 180, 451);
		Escritorio.add(porDefecto);
		porDefecto.setLayout(null);
				
		JTextPane txtpnMedicionesGenerador = new JTextPane();
		txtpnMedicionesGenerador.setText("Carpeta / Generador");
		txtpnMedicionesGenerador.setForeground(Color.WHITE);
		txtpnMedicionesGenerador.setEditable(false);
		txtpnMedicionesGenerador.setBackground(new Color(0, 128, 128));
		txtpnMedicionesGenerador.setBounds(10, 5, 130, 20);
		BarraSuperiorMed.add(txtpnMedicionesGenerador);
				

				
		Barradivision1 = new JPanel();
		Barradivision1.setBackground(new Color(0, 128, 128));
		Barradivision1.setBounds(0, 257, 180, 5);
		porDefecto.add(Barradivision1);
	    
		MostrarMS = new JPanel();
		MostrarMS.setBounds(0, 262, 180, 163);
	    porDefecto.add(MostrarMS);
		MostrarMS.setBackground(Color.WHITE);
		MostrarMS.setLayout(null);
		
				MostrarMS.setVisible(false);
				
				NivelM = new Canvas();
				NivelM.setBounds(145, 5, 6, 114);
				MostrarMS.add(NivelM);
				NivelM.setBackground(new Color(0, 0, 0));
				
				JTextPane NombreMt = new JTextPane();
				NombreMt.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				NombreMt.setEditable(false);
				NombreMt.setBounds(10, 11, 22, 20);
				MostrarMS.add(NombreMt);
				NombreMt.setText("Id: ");
				
				JComboBox<String> VentanaM = new JComboBox<String>();
				VentanaM.setForeground(new Color(47, 79, 79));
				VentanaM.setBackground(Color.WHITE);
				VentanaM.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				VentanaM.setModel(new DefaultComboBoxModel(new String[] {"Ventana FFT", "Rectangular", "Blackman", "Hanning", "Haming", "FlatTop", "Bartlett", "Tukey"}));
				VentanaM.setBounds(10, 40, 116, 20);
				MostrarMS.add(VentanaM);
				
				DispositivoM = new JTextPane();
				DispositivoM.setFont(new Font("Century Gothic", Font.PLAIN, 9));
				DispositivoM.setBounds(10, 68, 120, 34);
				MostrarMS.add(DispositivoM);
				DispositivoM.setText("Dispositivo: ");
				
				CanalM = new JTextPane();
				CanalM.setFont(new Font("Century Gothic", Font.PLAIN, 9));
				CanalM.setBounds(10, 102, 84, 20);
				MostrarMS.add(CanalM);
				CanalM.setText("Canal: ");
				
				JPanel ColorPanel = new JPanel();
				ColorPanel.setBounds(20, 140, 22, 10);
				MostrarMS.add(ColorPanel);
				ColorPanel.setBackground(Color.WHITE);
				ColorPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
				ColorPanel.setLayout(null);
				
				ColorM = new Canvas();
				ColorM.setBounds(1, 1, 20, 8);
				ColorPanel.add(ColorM);
				ColorM.setBackground(Color.decode(coloresM[0]));
				
				dBM = new JTextPane();
				dBM.setBounds(119, 115, 51, 20);
				MostrarMS.add(dBM);
				dBM.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				dBM.setText("dB --");
				
				JButton MasMs = new JButton("");
				MasMs.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/menuoff.png")));
				MasMs.setBackground(Color.WHITE);
				MasMs.setBounds(50, 135, 16, 16);
				MostrarMS.add(MasMs);
				MasMs.setBorder(null);
				ImageIcon AdministraSGOffEscD=new ImageIcon(AdministraSGOFF.getImage().getScaledInstance(MasMs.getWidth(), MasMs.getHeight(), java.awt.Image.SCALE_SMOOTH));
			    ImageIcon AdministraSGOnEscD=new ImageIcon(AdministraSGON.getImage().getScaledInstance(MasMs.getWidth(), MasMs.getHeight(), java.awt.Image.SCALE_SMOOTH));
			    MasMs.setRolloverIcon(AdministraSGOnEscD);
			    
			    NombreM = new JTextField();
			    NombreM.setBackground(Color.WHITE);
			    NombreM.setFont(new Font("Century Gothic", Font.PLAIN, 11));
			    NombreM.setBounds(35, 11, 91, 20);
			    MostrarMS.add(NombreM);
			    NombreM.setColumns(10);
			    
			    borrarMS = new JButton("");
			    borrarMS.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/eliminar.png")));
			    borrarMS.setBorder(null);
			    borrarMS.setBackground(Color.WHITE);
			    borrarMS.setBounds(78, 136, 16, 16);
			    MostrarMS.add(borrarMS);
			    
			    monitorS = new JToggleButton("");	
			    monitorS.setBounds(140, 135, 20, 20);
			   
			    monitorS.setBorder(null);
			    
			    ImageIcon monitorplay=new ImageIcon(playmonitor.getImage().getScaledInstance(monitorS.getWidth(), monitorS.getHeight(), java.awt.Image.SCALE_SMOOTH));
			    ImageIcon monitorstop=new ImageIcon(stopmonitor.getImage().getScaledInstance(monitorS.getWidth(), monitorS.getHeight(), java.awt.Image.SCALE_SMOOTH));
			   
			    monitorS.setSelectedIcon(monitorstop);
			    monitorS.setIcon(monitorplay);

			    MostrarMS.add(monitorS);
	    
	    MostrarMD = new JPanel();
	    MostrarMD.setBounds(0, 262, 180, 163);
	    porDefecto.add(MostrarMD);
	    MostrarMD.setBackground(new Color(255, 255, 255));
	    MostrarMD.setLayout(null);
	    
	    MostrarMD.setVisible(false);
	    
	    JTextPane NombreMDt = new JTextPane();
	    NombreMDt.setEditable(false);
	    NombreMDt.setText("Id: ");
	    NombreMDt.setBounds(10, 11, 22, 20);
	    MostrarMD.add(NombreMDt);
	    
	    JComboBox<String> VentanaM2 = new JComboBox<String>();
	    VentanaM2.setBackground(new Color(255, 255, 255));
	    VentanaM2.setForeground(new Color(47, 79, 79));
	    VentanaM2.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    VentanaM2.setModel(new DefaultComboBoxModel(new String[] {"Ventana FFT", "Rectangular", "Blackman", "Hanning", "Haming", "FlatTop", "Bartlett", "Tukey"}));
	    VentanaM2.setBounds(10, 38, 116, 20);
	    MostrarMD.add(VentanaM2);
	    
	    NivelM1 = new Canvas();
	    NivelM1.setBackground(Color.BLACK);
	    NivelM1.setBounds(140, 5, 6, 114);
	    MostrarMD.add(NivelM1);
	    
	    NivelM2 = new Canvas();
	    NivelM2.setBackground(Color.BLACK);
	    NivelM2.setBounds(160, 5, 6, 114);
	    MostrarMD.add(NivelM2);
	    
	    ReferenciaData = new JTextPane();
	    ReferenciaData.setFont(new Font("Century Gothic", Font.PLAIN, 9));
	    ReferenciaData.setEditable(false);
	    ReferenciaData.setText("Ref:\r\nCanal: ");
	    ReferenciaData.setBounds(10, 69, 116, 30);
	    MostrarMD.add(ReferenciaData);
	    
	    JPanel ColorPanel2 = new JPanel();
	    ColorPanel2.setLayout(null);
	    ColorPanel2.setBorder(new LineBorder(new Color(0, 0, 0)));
	    ColorPanel2.setBackground(Color.WHITE);
	    ColorPanel2.setBounds(20, 140, 22, 10);
	    MostrarMD.add(ColorPanel2);
	    
	    ColorM2 = new Canvas();
	    ColorM2.setBackground(Color.BLACK);
	    ColorM2.setBounds(1, 1, 20, 8);
	    ColorPanel2.add(ColorM2);
	    
	    dBM2 = new JTextPane();
	    dBM2.setText("dB");
	    dBM2.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    dBM2.setBounds(154, 117, 22, 20);
	    MostrarMD.add(dBM2);
	    
	    JButton MasMD = new JButton("");
	    MasMD.setBackground(new Color(255, 255, 255));
	    MasMD.setBounds(50, 135, 16, 16);
	    MasMD.setBorder(null);
	    AdministraSGOffEscD=new ImageIcon(AdministraSGOFF.getImage().getScaledInstance(MasMD.getWidth(), MasMD.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    AdministraSGOnEscD=new ImageIcon(AdministraSGON.getImage().getScaledInstance(MasMD.getWidth(), MasMD.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    MasMD.setRolloverIcon(AdministraSGOnEscD);
	    MasMD.setIcon(AdministraSGOffEscD);
	    MostrarMD.add(MasMD);
	    
	    MedicionData = new JTextPane();
	    MedicionData.setText("Med:\r\nCanal:");
	    MedicionData.setFont(new Font("Century Gothic", Font.PLAIN, 9));
	    MedicionData.setEditable(false);
	    MedicionData.setBounds(8, 97, 118, 32);
	    MostrarMD.add(MedicionData);
	    
	    NombreMD = new JTextField();
	    NombreMD.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    NombreMD.setBackground(Color.WHITE);
	    NombreMD.setEnabled(true);
	    NombreMD.setEditable(true);
	    NombreMD.setText("");
	    NombreMD.setBounds(32, 11, 94, 20);
	    MostrarMD.add(NombreMD);
	    NombreMD.setColumns(10);
	    
	    borrarMD = new JButton("");
	    borrarMD.setIcon(new ImageIcon(EsysM.class.getResource("/ClasesFrontera/Recursos/Imagenes/eliminar.png")));
	    borrarMD.setBorder(null);
	    borrarMD.setBackground(Color.WHITE);
	    borrarMD.setBounds(71, 136, 16, 16);
	    MostrarMD.add(borrarMD);
	    
	    monitorD = new JToggleButton("");
	    monitorD.setBorder(null);
	    monitorD.setBounds(142, 137, 20, 20);
	    
	    monitorD.setSelectedIcon(monitorstop);
	    monitorD.setIcon(monitorplay);
	    
	    MostrarMD.add(monitorD);
	    
	    dBM1 = new JTextPane();
	    dBM1.setText("dB");
	    dBM1.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    dBM1.setBounds(132, 117, 22, 20);
	    MostrarMD.add(dBM1);
	    
	    
		tree = new JTree();
		tree.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		tree.setShowsRootHandles(true);
		tree.setBounds(0, 0, 160, 245);    
	    tree.setCellRenderer( new RenderMediciones(false,'.'));
		porDefecto.add(tree);	
		
		qPane = new JScrollPane(tree,
			      ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
			      ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		qPane.setBounds(2, 10, 175, 245);  
		porDefecto.add(qPane);
		
		contenido= new CarpetaControl();
		contenido.CrearProyecto(direccion);
	    contenido.AgregarSesion("Default","#000000");
		Sesion s1=contenido.getSesion("Default");
		contenido.AgregarGrupo("Default", s1,"#000000");
		
		contenido.sincronizarProyecto();
		
	    GeneradorPanel = new JPanel();
	    GeneradorPanel.setBounds(0, 425, 180, 26);
	    porDefecto.add(GeneradorPanel);
	    GeneradorPanel.setBackground(Color.WHITE);
	    GeneradorPanel.setLayout(null);
	    
	    BarraGenerador = new JPanel();
	    BarraGenerador.setBounds(0, 0, 180, 27);
	    GeneradorPanel.add(BarraGenerador);
	    BarraGenerador.setLayout(null);
	    BarraGenerador.setBackground(new Color(0, 128, 128));
	    
	    JTextPane txtpnGenerador = new JTextPane();
	    txtpnGenerador.setText("Generador");
	    txtpnGenerador.setForeground(Color.WHITE);
	    txtpnGenerador.setEditable(false);
	    txtpnGenerador.setBackground(new Color(0, 128, 128));
	    txtpnGenerador.setBounds(10, 5, 76, 20);
	    BarraGenerador.add(txtpnGenerador);
	    
	    btnGenerador = new JButton("");
	    btnGenerador.setBorder(null);
	    btnGenerador.setBackground(new Color(0, 128, 128));
	    btnGenerador.setBounds(124, 5, 16, 16);
	    BarraGenerador.add(btnGenerador);
	    ImageIcon MaxEscOff=new ImageIcon(MaxOff.getImage().getScaledInstance(btnGenerador.getWidth(), btnGenerador.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnGenerador.setIcon(MaxEscOff);
	        
	    JComboBox<Object> TipoSe�al = new JComboBox<Object>();
	    TipoSe�al.setForeground(new Color(47, 79, 79));
	    TipoSe�al.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    TipoSe�al.setBackground(Color.WHITE);
	    TipoSe�al.setModel(new DefaultComboBoxModel<Object>(new String[] {"Tipo de Se\u00F1al", "Tono Puro", "Ruido Blanco", "Ruido Rosa", "Barrido Tonal"}));
	    TipoSe�al.setBounds(10, 38, 150, 20);
	    GeneradorPanel.add(TipoSe�al);
	    
	    DispGenerador = new JComboBox<Object>();
	    DispGenerador.setForeground(new Color(47, 79, 79));
	    DispGenerador.setBackground(Color.WHITE);
	    DispGenerador.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    DispGenerador.setModel(new DefaultComboBoxModel<Object>(new String[] {"Dispositivo de Salida"}));
	    DispGenerador.setBounds(10, 72, 150, 20);
	    GeneradorPanel.add(DispGenerador);
	    
	    JButton btnPlus = new JButton("");
	    btnPlus.setBounds(74, 140, 20, 20);
	    GeneradorPanel.add(btnPlus);
	    btnPlus.setBorder(null);
	    ImageIcon PlusEscOff=new ImageIcon(PlusOff.getImage().getScaledInstance(btnPlus.getWidth(), btnPlus.getHeight(), java.awt.Image.SCALE_SMOOTH));	
	    btnPlus.setIcon(PlusEscOff);
	    
	    JButton btnMinus = new JButton("");
	    btnMinus.setBounds(44, 141, 20, 20);
	    GeneradorPanel.add(btnMinus);
	    btnMinus.setBorder(null);
	    ImageIcon MinusEscOff=new ImageIcon(MinusOff.getImage().getScaledInstance(btnMinus.getWidth(), btnMinus.getHeight(), java.awt.Image.SCALE_SMOOTH));	
	    btnMinus.setIcon(MinusEscOff);		
	    
	    JToggleButton btnPlay = new JToggleButton("");
	    btnPlay.setBounds(20, 104, 25, 25);
	    GeneradorPanel.add(btnPlay);
	    btnPlay.setBorder(null);
	    
	    ImageIcon PlayEscOff=new ImageIcon( PlayOff.getImage().getScaledInstance(btnPlay.getWidth(), btnPlay.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon PlayOnEsc=new ImageIcon( PlayOn.getImage().getScaledInstance(btnPlay.getWidth(), btnPlay.getHeight(), java.awt.Image.SCALE_AREA_AVERAGING));   
		btnPlay.setIcon(PlayEscOff);
		btnPlay.setSelectedIcon(PlayOnEsc);
		
		Minimizar = new JButton("");
		Minimizar.setBackground(new Color(0, 128, 128));
		Minimizar.setBounds(140, 5, 16, 16);
		Minimizar.setBorder(null);
		BarraSuperiorMed.add(Minimizar);

		ImageIcon MiniEscOff=new ImageIcon( MiniOff.getImage().getScaledInstance(Minimizar.getWidth(), Minimizar.getHeight(), java.awt.Image.SCALE_SMOOTH));
		Minimizar.setIcon(MiniEscOff);

	    JPanel NivelG = new JPanel();
	    NivelG.setBackground(Color.BLACK);
	    NivelG.setBounds(60, 112, 100, 6);
	    GeneradorPanel.add(NivelG);
	    
	    SesionGrupoPanel = new JPanel();
	    SesionGrupoPanel.setBackground(Color.WHITE);
	    SesionGrupoPanel.setBounds(0, 262, 180, 164);
	    porDefecto.add(SesionGrupoPanel);
	    SesionGrupoPanel.setLayout(null);
	    
	    JTextPane txtId = new JTextPane();
	    txtId.setBackground(Color.WHITE);
	    txtId.setEditable(false);
	    txtId.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtId.setText("Id:");
	    txtId.setBounds(10, 22, 20, 20);
	    SesionGrupoPanel.add(txtId);
	    
	    txtNombreSG = new JTextField();
	    txtNombreSG.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtNombreSG.setBackground(Color.WHITE);
	    txtNombreSG.setBounds(40, 22, 130, 20);
	    SesionGrupoPanel.add(txtNombreSG);
	    txtNombreSG.setColumns(10);
	    
	    btnBorrarSG = new JButton("");
	    btnBorrarSG.setBackground(Color.WHITE);
	    btnBorrarSG.setBounds(145, 124, 16, 16);
	    ImageIcon EiminarEsc=new ImageIcon(Eliminar.getImage().getScaledInstance(btnBorrarSG.getWidth(), btnBorrarSG.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnBorrarSG.setIcon(EiminarEsc);
	    btnBorrarSG.setBorder(null);
	    SesionGrupoPanel.add(btnBorrarSG);
	    
	    JButton btnAdministradorSG = new JButton("");
	    btnAdministradorSG.setBackground(Color.WHITE);
	    btnAdministradorSG.setBounds(120, 124, 16, 16);
	    btnAdministradorSG.setBorder(null);
	    ImageIcon AdministraSGOffEscS=new ImageIcon(AdministraSGOFF.getImage().getScaledInstance(btnAdministradorSG.getWidth(), btnAdministradorSG.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon AdministraSGOnEscS=new ImageIcon(AdministraSGON.getImage().getScaledInstance(btnAdministradorSG.getWidth(), btnAdministradorSG.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnAdministradorSG.setRolloverIcon(AdministraSGOnEscS);
	    btnAdministradorSG.setIcon(AdministraSGOffEscS);
	    SesionGrupoPanel.add(btnAdministradorSG);
	    
	    JPanel ColorPanelSG = new JPanel();
	    ColorPanelSG.setLayout(null);
	    ColorPanelSG.setBorder(new LineBorder(new Color(0, 0, 0)));
	    ColorPanelSG.setBackground(Color.WHITE);
	    ColorPanelSG.setBounds(34, 124, 22, 10);
	    SesionGrupoPanel.add(ColorPanelSG);
	    
	    ColorSG = new Canvas();
	    ColorSG.setBounds(0, 0, 22, 10);
	    ColorPanelSG.add(ColorSG);
	    
	    ngrupostxt = new JTextPane();
	    ngrupostxt.setEditable(false);
	    ngrupostxt.setText("N\u00B0 Grupos");
	    ngrupostxt.setBounds(10, 53, 160, 20);
	    SesionGrupoPanel.add(ngrupostxt);
	    
	    nmedicionestxt = new JTextPane();
	    nmedicionestxt.setEditable(false);
	    nmedicionestxt.setText("N\u00B0 Mediciones");
	    nmedicionestxt.setBounds(10, 84, 160, 20);
	    SesionGrupoPanel.add(nmedicionestxt);
	    
	    TipoPanel = new JPanel();
	    TipoPanel.setBorder(null);
	    TipoPanel.setLayout(null);
	    TipoPanel.setBackground(new Color(255, 255, 255));
	    TipoPanel.setBounds(212, 25, 160, 27);
	    Escritorio.add(TipoPanel);
	    
	    txtTipoDeMedicion = new JTextPane();
	    txtTipoDeMedicion.setEditable(false);
	    txtTipoDeMedicion.setForeground(new Color(0, 128, 128));
	    txtTipoDeMedicion.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtTipoDeMedicion.setText("Tipo de Medici\u00F3n");
	    txtTipoDeMedicion.setBounds(5, 3, 110, 20);
	    TipoPanel.add(txtTipoDeMedicion);
	    
	    btnAbajoTipo = new JToggleButton("");
	    btnAbajoTipo.setBackground(new Color(255, 255, 255));
	    btnAbajoTipo.setBounds(120, 10, 10, 10);
	    ImageIcon AbajoEscOff=new ImageIcon(AbajoOff.getImage().getScaledInstance(btnAbajoTipo.getWidth(), btnAbajoTipo.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon ArribaEscOff=new ImageIcon(ArribaOff.getImage().getScaledInstance(btnAbajoTipo.getWidth(), btnAbajoTipo.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon AbajoEscOn=new ImageIcon(AbajoOn.getImage().getScaledInstance(btnAbajoTipo.getWidth(), btnAbajoTipo.getHeight(), java.awt.Image.SCALE_SMOOTH));	    
	    btnAbajoTipo.setIcon(AbajoEscOff);
	    btnAbajoTipo.setSelectedIcon(ArribaEscOff);
	    btnAbajoTipo.setRolloverIcon(AbajoEscOn);
	    btnAbajoTipo.setBorder(null);
	    TipoPanel.add(btnAbajoTipo);
	    	    
	    TipoPanelOpcion = new JPanel();
	    TipoPanelOpcion.setBackground(new Color(255, 255, 255));
	    TipoPanelOpcion.setBounds(5, 34, 150, 101);
	    TipoPanel.add(TipoPanelOpcion);
	    SpringLayout sl_TipoPanelOpcion = new SpringLayout();
	    TipoPanelOpcion.setLayout(sl_TipoPanelOpcion);
	    
	    txtRta = new JTextPane();
	    txtRta.setBackground(new Color(255, 255, 255));
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.WEST, txtRta, 0, SpringLayout.WEST, TipoPanelOpcion);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.SOUTH, txtRta, -81, SpringLayout.SOUTH, TipoPanelOpcion);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.EAST, txtRta, 150, SpringLayout.WEST, TipoPanelOpcion);
	    txtRta.setEditable(false);
	    txtRta.setForeground(new Color(0, 128, 128));
	    txtRta.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtRta.setText("RTA");
	    TipoPanelOpcion.add(txtRta);
	    
	    txtEspectrograma = new JTextPane();
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.NORTH, txtEspectrograma, 6, SpringLayout.SOUTH, txtRta);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.WEST, txtEspectrograma, 0, SpringLayout.WEST, txtRta);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.EAST, txtEspectrograma, 0, SpringLayout.EAST, txtRta);
	    txtEspectrograma.setEditable(false);
	    txtEspectrograma.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtEspectrograma.setForeground(new Color(0, 128, 128));
	    txtEspectrograma.setText("Espectrograma");
	    TipoPanelOpcion.add(txtEspectrograma);
	    
	    txtFuncionDeTransferencia = new JTextPane();
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.NORTH, txtFuncionDeTransferencia, 8, SpringLayout.SOUTH, txtEspectrograma);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.WEST, txtFuncionDeTransferencia, 0, SpringLayout.WEST, txtRta);
	    sl_TipoPanelOpcion.putConstraint(SpringLayout.EAST, txtFuncionDeTransferencia, 0, SpringLayout.EAST, txtRta);
	    txtFuncionDeTransferencia.setEditable(false);
	    txtFuncionDeTransferencia.setForeground(new Color(0, 128, 128));
	    txtFuncionDeTransferencia.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtFuncionDeTransferencia.setText("Funci\u00F3n de Transferencia");
	    TipoPanelOpcion.add(txtFuncionDeTransferencia);
	    
	    MagPanel = new JPanel();
	    MagPanel.setBorder(null);
	    MagPanel.setLayout(null);
	    MagPanel.setBackground(new Color(255, 255, 255));
	    MagPanel.setBounds(428, 25, 160, 27);
	    Escritorio.add(MagPanel);
	    
	    txtTipoMagnitud = new JTextPane();
	    txtTipoMagnitud.setText("Unidades Magnitud");
	    txtTipoMagnitud.setForeground(new Color(0, 128, 128));
	    txtTipoMagnitud.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtTipoMagnitud.setEditable(false);
	    txtTipoMagnitud.setBounds(5, 3, 120, 20);
	    MagPanel.add(txtTipoMagnitud);
	    
	    btnAbajoTipo1 = new JToggleButton("");
	    btnAbajoTipo1.setBorder(null);
	    btnAbajoTipo1.setBackground(Color.WHITE);
	    btnAbajoTipo1.setBounds(130, 10, 10, 10);
	    btnAbajoTipo1.setIcon(AbajoEscOff);
	    btnAbajoTipo1.setSelectedIcon(ArribaEscOff);
	    btnAbajoTipo1.setRolloverIcon(AbajoEscOn);
	    btnAbajoTipo1.setBorder(null);
	    MagPanel.add(btnAbajoTipo1);
	    
	    MagPanelOpcion = new JPanel();
	    MagPanelOpcion.setBackground(Color.WHITE);
	    MagPanelOpcion.setBounds(5, 34, 150, 101);
	    MagPanel.add(MagPanelOpcion);
	    SpringLayout sl_MagPanelOpcion = new SpringLayout();
	    MagPanelOpcion.setLayout(sl_MagPanelOpcion);
	    
	    txtdBFs = new JTextPane();
	    sl_MagPanelOpcion.putConstraint(SpringLayout.NORTH, txtdBFs, 0, SpringLayout.NORTH, MagPanelOpcion);
	    sl_MagPanelOpcion.putConstraint(SpringLayout.WEST, txtdBFs, 0, SpringLayout.WEST, MagPanelOpcion);
	    sl_MagPanelOpcion.putConstraint(SpringLayout.EAST, txtdBFs, 150, SpringLayout.WEST, MagPanelOpcion);
	    txtdBFs.setForeground(new Color(0, 128, 128));
	    txtdBFs.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtdBFs.setEditable(false);
	    MagPanelOpcion.add(txtdBFs);
	    txtdBFs.setText("Full Scale [dBFs]");
	    
	    txtdBspl = new JTextPane();
	    txtdBspl.setForeground(new Color(0, 128, 128));
	    txtdBspl.setEditable(false);
	    txtdBspl.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    sl_MagPanelOpcion.putConstraint(SpringLayout.NORTH, txtdBspl, 22, SpringLayout.NORTH, MagPanelOpcion);
	    MagPanelOpcion.add(txtdBspl);
	    txtdBspl.setText("Nivel de Presi\u00F3n [dBspl]");
	    
	    txtdBu = new JTextPane();
	    txtdBu.setText("Nivel El\u00E9ctrico [dBu]");
	    txtdBu.setForeground(new Color(0, 128, 128));
	    txtdBu.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtdBu.setEditable(false);
	    MagPanelOpcion.add(txtdBu);
	    
	    txtdBV = new JTextPane();
	    sl_MagPanelOpcion.putConstraint(SpringLayout.NORTH, txtdBV, 69, SpringLayout.NORTH, MagPanelOpcion);
	    sl_MagPanelOpcion.putConstraint(SpringLayout.WEST, txtdBV, 0, SpringLayout.WEST, MagPanelOpcion);
	    sl_MagPanelOpcion.putConstraint(SpringLayout.WEST, txtdBu, 0, SpringLayout.WEST, txtdBV);
	    sl_MagPanelOpcion.putConstraint(SpringLayout.SOUTH, txtdBu, -3, SpringLayout.NORTH, txtdBV);
	    txtdBV.setText("Nivel El\u00E9ctrico [dBV]");
	    txtdBV.setForeground(new Color(0, 128, 128));
	    txtdBV.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtdBV.setEditable(false);
	    MagPanelOpcion.add(txtdBV);
	    
	    EjexPanel = new JPanel();
	    EjexPanel.setBorder(null);
	    EjexPanel.setLayout(null);
	    EjexPanel.setBackground(new Color(255, 255, 255));
	    EjexPanel.setBounds(645, 25, 160, 27);
	    Escritorio.add(EjexPanel);
	    
	    txtTipoEjeX = new JTextPane();
	    txtTipoEjeX.setText("Visualizaci\u00F3n Hz");
	    txtTipoEjeX.setForeground(new Color(0, 128, 128));
	    txtTipoEjeX.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtTipoEjeX.setEditable(false);
	    txtTipoEjeX.setBounds(5, 3, 100, 20);
	    EjexPanel.add(txtTipoEjeX);
	    
	    btnAbajoTipo2 = new JToggleButton("");
	    btnAbajoTipo2.setBounds(120, 10, 10, 10);
	    EjexPanel.add(btnAbajoTipo2);
	    btnAbajoTipo2.setIcon(AbajoEscOff);
	    btnAbajoTipo2.setSelectedIcon(ArribaEscOff);
	    btnAbajoTipo2.setRolloverIcon(AbajoEscOn);
	    btnAbajoTipo2.setBorder(null);
	    btnAbajoTipo2.setBackground(Color.WHITE);
	    
	    HzPanelOpcion = new JPanel();
	    HzPanelOpcion.setBackground(Color.WHITE);
	    HzPanelOpcion.setBounds(5, 34, 150, 57);
	    EjexPanel.add(HzPanelOpcion);
	    SpringLayout sl_HzPanelOpcion = new SpringLayout();
	    HzPanelOpcion.setLayout(sl_HzPanelOpcion);
	    
	    txtLineal = new JTextPane();
	    sl_HzPanelOpcion.putConstraint(SpringLayout.NORTH, txtLineal, 0, SpringLayout.NORTH, HzPanelOpcion);
	    sl_HzPanelOpcion.putConstraint(SpringLayout.WEST, txtLineal, 0, SpringLayout.WEST, HzPanelOpcion);
	    sl_HzPanelOpcion.putConstraint(SpringLayout.EAST, txtLineal, 0, SpringLayout.EAST, HzPanelOpcion);
	    txtLineal.setText("Lineal");
	    txtLineal.setForeground(new Color(0, 128, 128));
	    txtLineal.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtLineal.setEditable(false);
	    HzPanelOpcion.add(txtLineal);
	    
	    txtLog = new JTextPane();
	    sl_HzPanelOpcion.putConstraint(SpringLayout.NORTH, txtLog, 6, SpringLayout.SOUTH, txtLineal);
	    sl_HzPanelOpcion.putConstraint(SpringLayout.WEST, txtLog, 0, SpringLayout.WEST, txtLineal);
	    sl_HzPanelOpcion.putConstraint(SpringLayout.EAST, txtLog, 0, SpringLayout.EAST, txtLineal);
	    txtLog.setText("Logar\u00EDtmica");
	    txtLog.setForeground(new Color(0, 128, 128));
	    txtLog.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtLog.setEditable(false);
	    HzPanelOpcion.add(txtLog);
	    
	    Grafica = new JPanel();
	    Grafica.setBorder(new LineBorder(new Color(47, 79, 79), 2));
	    Grafica.setBackground(Color.WHITE);
	    Grafica.setBounds(181, 52, 657, 445);
	    Escritorio.add(Grafica);
	    Grafica.setLayout(null);
	    
	    Monitoreo = new JPanel();
	    Monitoreo.setBackground(new Color(255, 255, 255));
	    Monitoreo.setBounds(838, 25, 170, 478);
	    Escritorio.add(Monitoreo);
	    Monitoreo.setLayout(null);
	    
	    BarrasupDerecha = new JPanel();
	    BarrasupDerecha.setBounds(0, 0, 170, 26);
	    Monitoreo.add(BarrasupDerecha);
	    BarrasupDerecha.setLayout(null);
	    BarrasupDerecha.setBackground(new Color(0, 128, 128));
	    
	    JTextPane InicioCaptura = new JTextPane();
	    InicioCaptura.setText("Inicio / Captura");
	    InicioCaptura.setForeground(Color.WHITE);
	    InicioCaptura.setEditable(false);
	    InicioCaptura.setBackground(new Color(0, 128, 128));
	    InicioCaptura.setBounds(40, 5, 100, 20);
	    BarrasupDerecha.add(InicioCaptura);
	    
	    JToggleButton btnIniciar = new JToggleButton("");
	    btnIniciar.setBackground(Color.WHITE);
	    btnIniciar.setBounds(36, 80, 100, 40);
	    btnIniciar.setBorder(null);
	    ImageIcon MedOff=new ImageIcon(IniciarMed.getImage().getScaledInstance(btnIniciar.getWidth(), btnIniciar.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon MedOn=new ImageIcon(DetenerMed.getImage().getScaledInstance(btnIniciar.getWidth(), btnIniciar.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon MedOnRoll=new ImageIcon(IniciarMedOn.getImage().getScaledInstance(btnIniciar.getWidth(), btnIniciar.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnIniciar.setSelectedIcon(MedOn);
	    btnIniciar.setIcon(MedOff);
	    btnIniciar.setRolloverIcon(MedOnRoll);
	    Monitoreo.add(btnIniciar);
	    
	    JComboBox comboBox = new JComboBox();
	    comboBox.setBackground(new Color(255, 255, 255));
	    comboBox.setForeground(new Color(47, 79, 79));
	    comboBox.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    comboBox.setModel(new DefaultComboBoxModel(new String[] {"2", "8", "16", "1 Seg", "2 Seg"}));
	    comboBox.setBounds(82, 45, 65, 20);
	    Monitoreo.add(comboBox);
	    
	    JComboBox comboBox_1 = new JComboBox();
	    comboBox_1.setBackground(new Color(255, 255, 255));
	    comboBox_1.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    comboBox_1.setForeground(new Color(47, 79, 79));
	    comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Sin Filtro", "Octavas", "1/3 de Octava", "1/6 de Octava", "1/12 de Octava", "1/24 de Octava", "1/48 de Octava"}));
	    comboBox_1.setBounds(20, 145, 127, 20);
	    Monitoreo.add(comboBox_1);
	    
	    THDpanel = new JPanel();
	    THDpanel.setBackground(Color.WHITE);
	    THDpanel.setBounds(10, 180, 150, 98);
	    Monitoreo.add(THDpanel);
	    THDpanel.setVisible(false);
	    THDpanel.setLayout(null);
	    
	    checTHD = new JCheckBox("THD");
	    checTHD.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    checTHD.setBackground(Color.WHITE);
	    checTHD.setBounds(48, 10, 57, 23);
	    THDpanel.add(checTHD);
	    
	    textFreqTHD = new JTextField();
	    textFreqTHD.setEnabled(false);
	    textFreqTHD.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    textFreqTHD.setText("0");
	    textFreqTHD.setBounds(77, 42, 63, 20);
	    THDpanel.add(textFreqTHD);
	    textFreqTHD.setColumns(10);
	    
	    JTextPane txtpnFreqhz = new JTextPane();
	    txtpnFreqhz.setEditable(false);
	    txtpnFreqhz.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtpnFreqhz.setText("Freq [Hz]");
	    txtpnFreqhz.setBounds(10, 40, 57, 20);
	    THDpanel.add(txtpnFreqhz);
	    
	    JTextPane txtpnThd = new JTextPane();
	    txtpnThd.setText("THD [%]");
	    txtpnThd.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtpnThd.setEditable(false);
	    txtpnThd.setBounds(10, 70, 57, 20);
	    THDpanel.add(txtpnThd);
	    
	    txtTHD = new JTextPane();
	    txtTHD.setEnabled(false);
	    txtTHD.setText("--");
	    txtTHD.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtTHD.setEditable(false);
	    txtTHD.setBounds(77, 70, 63, 20);
	    THDpanel.add(txtTHD);
	    
	    CoherencePanel = new JPanel();
	    CoherencePanel.setBounds(10, 180, 150, 98);
	    Monitoreo.add(CoherencePanel);
	    CoherencePanel.setVisible(false);
	    CoherencePanel.setLayout(null);
	    CoherencePanel.setBackground(Color.WHITE);
	    
	    checCoherence = new JCheckBox("Coherencia");
	    checCoherence.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    checCoherence.setBackground(Color.WHITE);
	    checCoherence.setBounds(28, 10, 92, 23);
	    CoherencePanel.add(checCoherence);
	    
	    textFreqCoherence = new JTextField();
	    textFreqCoherence.setText("0");
	    textFreqCoherence.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    textFreqCoherence.setEnabled(false);
	    textFreqCoherence.setColumns(10);
	    textFreqCoherence.setBounds(77, 42, 63, 20);
	    CoherencePanel.add(textFreqCoherence);
	    
	    JTextPane textPane = new JTextPane();
	    textPane.setText("Freq [Hz]");
	    textPane.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    textPane.setEditable(false);
	    textPane.setBounds(10, 40, 57, 20);
	    CoherencePanel.add(textPane);
	    
	    JTextPane txtpnCo = new JTextPane();
	    txtpnCo.setText("Co [%]");
	    txtpnCo.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtpnCo.setEditable(false);
	    txtpnCo.setBounds(10, 70, 57, 20);
	    CoherencePanel.add(txtpnCo);
	    
	    txtCoherence = new JTextPane();
	    txtCoherence.setText("--");
	    txtCoherence.setFont(new Font("Century Gothic", Font.PLAIN, 11));
	    txtCoherence.setEnabled(false);
	    txtCoherence.setEditable(false);
	    txtCoherence.setBounds(77, 70, 63, 20);
	    CoherencePanel.add(txtCoherence);
	    
	    JPanel BarraDivision2 = new JPanel();
	    BarraDivision2.setBackground(new Color(0, 128, 128));
	    BarraDivision2.setBounds(0, 283, 170, 5);
	    Monitoreo.add(BarraDivision2);
	    
	    btnCaptura = new JButton("");
	    btnCaptura.setBorder(null);
	    btnCaptura.setBackground(Color.WHITE);
	    btnCaptura.setBounds(10, 300, 65, 23);
	    ImageIcon CapturaN=new ImageIcon(Captura.getImage().getScaledInstance(btnCaptura.getWidth(), btnCaptura.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ImageIcon CapturaNOn=new ImageIcon(CapturaOn.getImage().getScaledInstance(btnCaptura.getWidth(), btnCaptura.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnCaptura.setRolloverIcon(CapturaNOn);
	    btnCaptura.setIcon(CapturaN);
	    Monitoreo.add(btnCaptura);
	    
	    btnNuevaC = new JButton("");
	    btnNuevaC.setBackground(new Color(255, 255, 255));
	    btnNuevaC.setBounds(120, 303, 16, 16);
	    btnNuevaC.setBorder(null);
	    ImageIcon NuevaCap=new ImageIcon(NuevaC.getImage().getScaledInstance(btnNuevaC.getWidth(), btnNuevaC.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    btnNuevaC.setIcon(NuevaCap);
	    Monitoreo.add(btnNuevaC);
	    
	    BorrarCap = new JButton("");
	    BorrarCap.setBackground(Color.WHITE);
	    BorrarCap.setBounds(140, 303, 16, 16);
	    ImageIcon EiminarCap=new ImageIcon(Eliminar.getImage().getScaledInstance(btnBorrarSG.getWidth(), btnBorrarSG.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    BorrarCap.setIcon(EiminarCap);
	    BorrarCap.setBorder(null);
	    Monitoreo.add(BorrarCap);
	    
    
	    treeC = new JTree();
	    treeC.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		treeC.setShowsRootHandles(true); 
	    treeC.setCellRenderer( new RenderCapturas("#000000"));
		//porDefecto.add(tree);	
		
	    qPaneC = new JScrollPane(treeC,
			      ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
			      ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		qPaneC.setBounds(10, 334, 150, 133); 
		Monitoreo.add(qPaneC);
		
		JTextPane txtpnPromedio = new JTextPane();
		txtpnPromedio.setForeground(new Color(47, 79, 79));
		txtpnPromedio.setEditable(false);
		txtpnPromedio.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtpnPromedio.setBackground(new Color(255, 255, 255));
		txtpnPromedio.setText("Promedio");
		txtpnPromedio.setBounds(20, 45, 60, 20);
		Monitoreo.add(txtpnPromedio);
		
		JTextPane txtpnFiltrado = new JTextPane();
		txtpnFiltrado.setText("Filtrado");
		txtpnFiltrado.setForeground(new Color(47, 79, 79));
		txtpnFiltrado.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtpnFiltrado.setEditable(false);
		txtpnFiltrado.setBackground(Color.WHITE);
		txtpnFiltrado.setBounds(20, 126, 60, 20);
		Monitoreo.add(txtpnFiltrado);
		
		ColorC = new JButton();
		ColorC.setLayout(null);
		ColorC.setBorder(new LineBorder(new Color(0, 0, 0)));
		ColorC.setBackground(Color.WHITE);
		ColorC.setBounds(90, 303, 16, 16);
		ImageIcon EditarcapOffP=new ImageIcon(EditarcapOff.getImage().getScaledInstance(ColorC.getWidth(), ColorC.getHeight(), java.awt.Image.SCALE_SMOOTH));
		ImageIcon EditarcapOnP=new ImageIcon(EditarcapOn.getImage().getScaledInstance(ColorC.getWidth(), ColorC.getHeight(), java.awt.Image.SCALE_SMOOTH));
	    ColorC.setIcon(EditarcapOffP);
	    ColorC.setRolloverIcon(EditarcapOnP);
	    ColorC.setBorder(null);
		Monitoreo.add(ColorC);
		
		contenidoC= new CapturaControl();
		contenidoC.CrearProyecto(direccion);
		contenidoC.AgregarCarpeta("Default", "#000000");
		contenidoC.sincronizarProyecto();
		 
		EsysMControl control=new EsysMControl();
		control.dimensiones();
		control.colorF();
		control.MenuNuevo();
		control.NuevoCap();
		
	}
}
