package ClasesFrontera.Recursos;

import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.JTextPane;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.border.LineBorder;

import ClasesFrontera.EsysM;
import ClasesEntidad.Captura;
import ClasesEntidad.Carpeta;
import ClasesEntidad.Grupo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Alerta extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private final JPanel contentPanel = new JPanel();
	public static JTextPane Instructivo;
	public static JButton Muestrame;
	private JComponent panel;
	private ArrayList<Carpeta> cap;
	private String mensaje;
	private JButton Entendido;
	private boolean estado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Alerta dialog = new Alerta('P');
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void CaracteristicasP(JComponent panel) {
		this.panel = panel;	
	}
	
	public void Mensaje(String mensaje) {
		Instructivo.setText(mensaje);	
	}
	
	public boolean confirmar() {
		return estado;
	}
	

	/**
	 * Create the dialog.
	 * 
	 * @throws IOException
	 */
	public Alerta(char Tipo) throws IOException {
		setResizable(false);
		setModal(true);
		setTitle("Dispositivo");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Alerta.class.getResource("/ClasesFrontera/Recursos/Imagenes/IconoAlerta.png")));
		setBounds(100, 100, 450, 173);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 444, 144);
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setBorder(new LineBorder(new Color(0, 128, 128), 2));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);

		Instructivo = new JTextPane();
		Instructivo.setEditable(false);
		Instructivo.setToolTipText("");
		Instructivo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		Instructivo.setText(mensaje);
		Instructivo.setBounds(89, 27, 310, 49);
		contentPanel.add(Instructivo);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(Color.WHITE);
			buttonPane.setBounds(20, 100, 414, 33);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				Muestrame = new JButton("Muestrame");
				Muestrame.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (Tipo == 'P') {
							
							Border borde = BorderFactory.createLineBorder(Color.RED, 1);
							panel.setBorder(borde); 
							
						} else if (Tipo == 'C') {

							EsysM.contenidoC.SincronizarColor("#FF0000");
						} else if (Tipo == 'S') {

							EsysM.contenido.SincronizarColor(true,Tipo);
						} else if (Tipo == 'G') {

							EsysM.contenido.SincronizarColor(true,Tipo);
						}
						
						estado=false;
						dispose();
					}
				});
				Muestrame.setForeground(Color.WHITE);
				Muestrame.setFont(new Font("Century Gothic", Font.BOLD, 12));
				Muestrame.setBackground(new Color(0, 128, 128));
				Muestrame.setActionCommand("Cancel");
				buttonPane.add(Muestrame);
			}
			{
				Entendido = new JButton("Entendido");
				Entendido.setForeground(Color.WHITE);
				Entendido.setFont(new Font("Century Gothic", Font.BOLD, 12));
				Entendido.setBackground(new Color(0, 128, 128));
				Entendido.setActionCommand("Cancel");
				buttonPane.add(Entendido);
			}
			{
				Entendido.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						estado=true;
						dispose();
					}
				});
			}
		}

		JLabel alertaIcon = new JLabel("");
		alertaIcon.setBounds(35, 27, 38, 33);
		contentPanel.add(alertaIcon);

		ImageIcon Alertaimage = new ImageIcon(this.getClass().getResource("Imagenes/Alerta.png"));
		ImageIcon AlertaEsc = new ImageIcon(Alertaimage.getImage().getScaledInstance(alertaIcon.getWidth(),alertaIcon.getHeight(), java.awt.Image.SCALE_DEFAULT));
		alertaIcon.setIcon(AlertaEsc);

	}
}
