package ClasesFrontera.Recursos;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JToggleButton;
import javax.swing.JTextPane;
import java.awt.Canvas;
import java.awt.Toolkit;

import ClasesControl.EsysMControl;
import ClasesControl.Recursos.RecursosControl;
import ClasesFrontera.EsysM;

import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


public class Recursos {

	public static JDialog frmRecursosDeAudio;
	public static JButton btnAceptar;
	public static JRadioButton SelectReprod;
	public static JRadioButton SelectCaptura;
	public static JComboBox<String> Dispositivos;
	public static JToggleButton Hab_Des; 
	public static JTextPane nCanales;
	public static JTextPane nMuestreo;
	public static JTextPane nBits;
	public static Canvas Niveles;
	public static JTextPane RmsNivel;
	public static JToggleButton btnProbar;
	public static JPanel ErrorCanal;
	public static JPanel ErrorDisp;
	public static JComboBox SelectC;
	public static JButton btnPlus;
	public static  JButton btnMinus;
	private boolean vacio=true;
	
	/**
	 * Launch the application.
	 */

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Recursos window = new Recursos();
					window.frmRecursosDeAudio.setVisible(true);			
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws LineUnavailableException 
	 * @throws HeadlessException 
	 */
	public Recursos() throws HeadlessException, LineUnavailableException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws LineUnavailableException 
	 * @throws HeadlessException 
	 */
	private void initialize() throws LineUnavailableException, HeadlessException {
		frmRecursosDeAudio = new JDialog();
		frmRecursosDeAudio.setModal(true);
		frmRecursosDeAudio.setAlwaysOnTop(true);
		frmRecursosDeAudio.setResizable(false);
		frmRecursosDeAudio.setTitle("Recursos de Audio");
		frmRecursosDeAudio.setIconImage(Toolkit.getDefaultToolkit().getImage(Recursos.class.getResource("/ClasesFrontera/Recursos/Imagenes/IconoRecursos.png")));
		frmRecursosDeAudio.getContentPane().setBackground(Color.WHITE);
		frmRecursosDeAudio.setBounds(100, 100, 413, 316);
		frmRecursosDeAudio.setUndecorated(false);
		frmRecursosDeAudio.getContentPane().setLayout(null);	

		
		ImageIcon PlusOff= new ImageIcon(this.getClass().getResource("Imagenes/plus.png"));
		ImageIcon MinusOff= new ImageIcon(this.getClass().getResource("Imagenes/minus.png"));	
		
		
		//Panel de Color sobre el Frame
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(new Color(0, 128, 128), 2));
		panel.setBounds(0, 0, 407, 287);
		frmRecursosDeAudio.getContentPane().add(panel);
		panel.setLayout(null);

		//----------------------------------------------
		
		
		//Boton de Selecci�n de Dispositivo de Captura
		SelectCaptura = new JRadioButton("Dispositivos de Captura");
		SelectCaptura.setBounds(30, 25, 159, 25);
		panel.add(SelectCaptura);
		SelectCaptura.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		SelectCaptura.setBackground(Color.WHITE);
		//--------------------------------------------------
		
		
		//Boton de Selecci�n de Dispositivo de Reproducci�n
		SelectReprod = new JRadioButton("Dispositivos de Reproducci\u00F3n");
		SelectReprod.setBounds(202, 25, 194, 25);
		panel.add(SelectReprod);
		SelectReprod.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		SelectReprod.setBackground(Color.WHITE);
	    //----------------------------------------------------
	    
	    
		//Boton de ACEPTAR
	    btnAceptar = new JButton("");
	    btnAceptar.setBounds(319, 240, 77, 36);
	    panel.add(btnAceptar);
	    btnAceptar.setBackground(Color.WHITE);
	    btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 5));

	    
	    int ancho=btnAceptar.getWidth();
	    int alto=btnAceptar.getHeight();
	    //----------------------------------------------------------------------
	    
		//Acceso a Imagen de ACEPTAR y Escala
		ImageIcon imgAceptarOff= new ImageIcon(this.getClass().getResource("Imagenes/BotonAceptar.png"));
		ImageIcon imgAceptarOn= new ImageIcon(this.getClass().getResource("Imagenes/BotonAceptarOn.png"));
		
		ImageIcon imgAceptarEsc=new ImageIcon(imgAceptarOff.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_DEFAULT));
		ImageIcon imgAceptarOnEsc=new ImageIcon(imgAceptarOn.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_AREA_AVERAGING));
		//--------------------------------------
	    
	    btnAceptar.setIcon(imgAceptarEsc);
	    btnAceptar.setBorderPainted(false);
	    btnAceptar.setRolloverIcon(imgAceptarOnEsc);
	    btnAceptar.setSelectedIcon(imgAceptarOnEsc);
	    btnAceptar.setBorder(null);
	    //----------------------------------------------------------------------
	    
	    
	    //Boton de Habilitar o deshabilitar
	    Hab_Des = new JToggleButton(""); 
	    Hab_Des.setSelected(true);
	    Hab_Des.setBounds(30, 87, 34, 13);
	    Hab_Des.setBorder(null);
	    panel.add(Hab_Des);
	    //----------------------------------------------------------------------
	    
		//Acceso a Imagen de HABILITAR y Escala
		ImageIcon Hab_DesOff= new ImageIcon(this.getClass().getResource("Imagenes/HabDispOff.png"));
		ImageIcon Hab_DesOn= new ImageIcon(this.getClass().getResource("Imagenes/HabDisp.png"));
		
		ImageIcon Hab_DesEscOff=new ImageIcon(Hab_DesOff.getImage().getScaledInstance(Hab_Des.getWidth(), Hab_Des.getHeight(), java.awt.Image.SCALE_AREA_AVERAGING));
		ImageIcon Hab_DesOnEsc=new ImageIcon(Hab_DesOn.getImage().getScaledInstance(Hab_Des.getWidth(), Hab_Des.getHeight(), java.awt.Image.SCALE_AREA_AVERAGING));
		//--------------------------------------
	    
		Hab_Des.setIcon(Hab_DesEscOff);
		Hab_Des.setBorderPainted(false);
		Hab_Des.setSelectedIcon(Hab_DesOnEsc);
		Hab_Des.setSelected(false);
		//----------------------------------------------------------------------
		
		
		//Boton PROBAR
		btnProbar = new JToggleButton("");
		btnProbar.setEnabled(false);
		btnProbar.setBounds(30, 228, 71, 30);
		btnProbar.setBorderPainted(false);
		btnProbar.setBackground(Color.WHITE);
		btnProbar.setBorder(null);
		panel.add(btnProbar);
		
		
		ImageIcon probarOff= new ImageIcon(this.getClass().getResource("Imagenes/BotonProbar.png"));
		ImageIcon probarOn= new ImageIcon(this.getClass().getResource("Imagenes/BotonProbarOn.png"));
		ImageIcon probarSele= new ImageIcon(this.getClass().getResource("Imagenes/BotonDetener.png"));
		
		ImageIcon probarEscOff=new ImageIcon( probarOff.getImage().getScaledInstance(btnProbar.getWidth(), btnProbar.getHeight(), java.awt.Image.SCALE_SMOOTH));
		ImageIcon probarOnEsc=new ImageIcon( probarOn.getImage().getScaledInstance(btnProbar.getWidth(), btnProbar.getHeight(), java.awt.Image.SCALE_AREA_AVERAGING));
		ImageIcon probarOnSele=new ImageIcon( probarSele.getImage().getScaledInstance(btnProbar.getWidth(), btnProbar.getHeight(), java.awt.Image.SCALE_AREA_AVERAGING));
	    
		btnProbar.setIcon(probarEscOff);
		btnProbar.setBorderPainted(false);
	    btnProbar.setRolloverIcon(probarOnEsc);
		btnProbar.setSelectedIcon(probarOnSele);
		//----------------------------------------------------------------------
		
		
		//texto ON/OFF
		JTextPane txtpnOnoff = new JTextPane();
		txtpnOnoff.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtpnOnoff.setText("On/Off");
		txtpnOnoff.setBounds(30, 66, 48, 20);
		panel.add(txtpnOnoff);
		//----------------------------------------------------------------------
		
		//Texto DBFS
		JTextPane txtpnDbfs = new JTextPane();
		txtpnDbfs.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtpnDbfs.setText("dBFs");
		txtpnDbfs.setBounds(89, 188, 25, 20);
		panel.add(txtpnDbfs);
		//----------------------------------------------------------------------
		
		//Texto n�mero de canales
		JTextPane txtpnNmeroDeCanales = new JTextPane();
		txtpnNmeroDeCanales.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtpnNmeroDeCanales.setText("N\u00FAmero de Canales");
		txtpnNmeroDeCanales.setBounds(169, 119, 126, 20);
		panel.add(txtpnNmeroDeCanales);
		
		nCanales = new JTextPane();
		nCanales.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		nCanales.setText("--");
		nCanales.setBounds(326, 119, 57, 20);
		panel.add(nCanales);
		//----------------------------------------------------------------------
		
		//texto profundidad bits
		JTextPane txtpnProfundidadDeBits = new JTextPane();
		txtpnProfundidadDeBits.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtpnProfundidadDeBits.setText("Profundidad de Bits");
		txtpnProfundidadDeBits.setBounds(169, 150, 126, 20);
		panel.add(txtpnProfundidadDeBits);
		
		nBits = new JTextPane();
		nBits.setText("--");
		nBits.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		nBits.setBounds(326, 150, 57, 20);
		panel.add(nBits);
		//----------------------------------------------------------------------
		
		//Texto frcuencia de muestreo
		JTextPane txtpnFrecuenciaDeMuestreo = new JTextPane();
		txtpnFrecuenciaDeMuestreo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtpnFrecuenciaDeMuestreo.setText("Frecuencia de Muestreo");
		txtpnFrecuenciaDeMuestreo.setBounds(169, 180, 151, 20);
		panel.add(txtpnFrecuenciaDeMuestreo);
		
		nMuestreo = new JTextPane();
		nMuestreo.setText("--");
		nMuestreo.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		nMuestreo.setBounds(326, 180, 57, 20);
		panel.add(nMuestreo);
		//----------------------------------------------------------------------
		
		//canvas para graficar niveles
		Niveles = new Canvas();
		Niveles.setBackground(new Color(0, 0, 0));
		Niveles.setBounds(30, 214, 114, 6);
		panel.add(Niveles);
		//----------------------------------------------------------------------
		
		//texto para mostrar valor Rms de nivel
		RmsNivel = new JTextPane();
		RmsNivel.setText("--");
		RmsNivel.setBounds(41, 188, 46, 20);
		panel.add(RmsNivel);
		//----------------------------------------------------------------------

		//Paneles de ayuda
		ErrorCanal = new JPanel();
		ErrorCanal.setBackground(Color.WHITE);
		ErrorCanal.setBounds(30, 124, 106, 63);
		panel.add(ErrorCanal);
		ErrorCanal.setLayout(null);
		
		ErrorDisp = new JPanel();
		ErrorDisp.setBackground(Color.WHITE);
		ErrorDisp.setBounds(97, 57, 239, 41);
		panel.add(ErrorDisp);
		ErrorDisp.setLayout(null);
		//----------------------------------------------------------------------
		
		//Selecci�n de canal
		JTextPane txtpnCanales = new JTextPane();
		txtpnCanales.setBounds(22, 5, 52, 21);
		ErrorCanal.add(txtpnCanales);
		txtpnCanales.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtpnCanales.setText("Canales");
		
		SelectC = new JComboBox();
		SelectC.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		SelectC.setForeground(new Color(0, 128, 128));
		SelectC.setBounds(10, 32, 86, 20);
		ErrorCanal.add(SelectC);
		//----------------------------------------------------------------------		    
		
		
		//Selecci�nd de dispositivos
		Dispositivos = new JComboBox<String>();
		Dispositivos.setBounds(10, 11, 219, 21);
		ErrorDisp.add(Dispositivos);
		
		Dispositivos.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		Dispositivos.setForeground(new Color(0, 128, 128));
		Dispositivos.setBackground(Color.WHITE);
		Dispositivos.addItem("Dispositivos");
		
		btnPlus = new JButton("");
		btnPlus.setBounds(130, 228, 16, 16);
		panel.add(btnPlus);
	    btnPlus.setBorder(null);
	    ImageIcon PlusEscOff=new ImageIcon(PlusOff.getImage().getScaledInstance(btnPlus.getWidth(), btnPlus.getHeight(), java.awt.Image.SCALE_SMOOTH));	
	    btnPlus.setIcon(PlusEscOff);
	    
	    btnMinus = new JButton("");
	    btnMinus.setBorder(null);
	    btnMinus.setBounds(111, 228, 16, 16);
	    panel.add(btnMinus);
	    ImageIcon MinusEscOff=new ImageIcon(MinusOff.getImage().getScaledInstance(btnMinus.getWidth(), btnMinus.getHeight(), java.awt.Image.SCALE_SMOOTH));	
	    btnMinus.setIcon(MinusEscOff);

		//--------------------------------------------------------------------------
		
		//Clase de control
		RecursosControl control= new RecursosControl();	
		control.Cerrar();
		if (EsysMControl.dispositivosRep.size()==0 && EsysMControl.dispositivosCap.size()==0) {
			control.filtrar();
			vacio=true;
		}else {
			vacio=false;
		}
		control.MostrarDisp(vacio);
		control.MostrarCaracter();
		control.SelectCanal();
		control.Probar();
		control.On_Off();
	    //-------------------------------------------------------------------------
		
	}
}
