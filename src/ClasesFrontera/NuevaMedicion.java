package ClasesFrontera;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import java.awt.Button;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

import ClasesControl.EsysMControl;
import ClasesControl.NuevaMControl;
import ClasesEntidad.Sesion;
import ClasesEntidad.Grupo;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import java.awt.Canvas;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.JColorChooser;
import java.awt.Panel;


public class NuevaMedicion extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Launch the application.

	 */

	public static JPanel SGError, SimplePanel, DoblePanel;
	
	public static JComboBox<String>  DispositivosS,DispositivosD1, DispositivosD2, 
									 Sesiones, Grupos, ChanelD2, ChanelD1, ChanelS;
	public static JRadioButton btnSimple, btnDoble;
	
	public static JButton btnAceptar;
	
	public static JTextField txtNombre, txtNombreD;
	
	public static Canvas ColorD,ColorS;
	
	public static JTextPane txtcanal1, txtcanal2, txtDispositvos,
							txtCanal, txtMedicion,txtReferencia;
	
	public static void main(String[] args) {
		try {
			NuevaMedicion dialog = new NuevaMedicion();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	
	
	public NuevaMedicion() {
		setModal(true);
		setResizable(false);
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setLayout(null);
		
		btnSimple = new JRadioButton("Simple");
		btnSimple.setForeground(new Color(0, 128, 128));
		btnSimple.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		btnSimple.setBackground(Color.WHITE);
		btnSimple.setBounds(23, 35, 81, 23);
		getContentPane().add(btnSimple);
		
		btnDoble = new JRadioButton("Doble");
		btnDoble.setForeground(new Color(0, 128, 128));
		btnDoble.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		btnDoble.setBackground(Color.WHITE);
		btnDoble.setBounds(23, 95, 81, 23);
		getContentPane().add(btnDoble);
		
		
		SimplePanel = new JPanel();
		SimplePanel.setBorder(new LineBorder(new Color(0, 128, 128), 1, true));
		SimplePanel.setBackground(new Color(255, 255, 255));
		SimplePanel.setBounds(110, 23, 283, 171);
		getContentPane().add(SimplePanel);
		SimplePanel.setLayout(null);
		
				txtNombre = new JTextField();
				txtNombre.setBounds(92, 23, 164, 20);
				SimplePanel.add(txtNombre);
				txtNombre.setColumns(10);
				
				JTextPane txtpnNombreid = new JTextPane();
				txtpnNombreid.setBounds(10, 23, 72, 20);
				SimplePanel.add(txtpnNombreid);
				txtpnNombreid.setEditable(false);
				txtpnNombreid.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				txtpnNombreid.setBackground(Color.WHITE);
				txtpnNombreid.setText("Nombre (Id)");
				
				txtDispositvos = new JTextPane();
				txtDispositvos.setEditable(false);
				txtDispositvos.setText("Entrada de Audio");
				txtDispositvos.setBounds(10, 54, 221, 20);
				SimplePanel.add(txtDispositvos);
		
				DispositivosS = new JComboBox();
				DispositivosS.setForeground(new Color(0, 0, 0));
				DispositivosS.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				DispositivosS.setBounds(35, 85, 221, 20);
				SimplePanel.add(DispositivosS);
				
				txtCanal = new JTextPane();
				txtCanal.setEditable(false);
				txtCanal.setText("Canal");
				txtCanal.setBounds(118, 116, 48, 20);
				SimplePanel.add(txtCanal);
				
				ChanelS = new JComboBox();
				ChanelS.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				ChanelS.setBounds(176, 116, 80, 20);
				SimplePanel.add(ChanelS);
				
				JPanel panel = new JPanel(); //Panel de color
				panel.setBackground(new Color(0, 0, 0));
				panel.setBounds(2, 152, 24, 16);
				SimplePanel.add(panel);
				panel.setLayout(null);
				
						ColorS = new Canvas();
						ColorS.setBounds(0, 0, 24, 16);
						panel.add(ColorS);
				
				JTextPane txtpnColor = new JTextPane();
				txtpnColor.setText("Color");
				txtpnColor.setEditable(false);
				txtpnColor.setBounds(29, 150, 37, 20);
				SimplePanel.add(txtpnColor);
				

			
		DoblePanel = new JPanel();
		DoblePanel.setLayout(null);
		DoblePanel.setBorder(new LineBorder(new Color(0, 128, 128), 1, true));
		DoblePanel.setBackground(Color.WHITE);
		DoblePanel.setBounds(110, 8, 283, 214);
		getContentPane().add(DoblePanel);
		
		
				JTextPane txtpnNombreid_1 = new JTextPane();
				txtpnNombreid_1.setBounds(10, 11, 70, 20);
				DoblePanel.add(txtpnNombreid_1);
				txtpnNombreid_1.setText("Nombre (Id)");
				
				txtNombreD = new JTextField();
				txtNombreD.setBounds(89, 11, 177, 20);
				DoblePanel.add(txtNombreD);
				txtNombreD.setColumns(10);
				
				
				DispositivosD1 = new JComboBox();
				DispositivosD1.setForeground(Color.BLACK);
				DispositivosD1.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				DispositivosD1.setBounds(45, 72, 221, 20);
				DoblePanel.add(DispositivosD1);
				
				txtReferencia = new JTextPane();
				txtReferencia.setText("Entrada de Audio - Referencia");
				txtReferencia.setEditable(false);
				txtReferencia.setBounds(10, 44, 221, 20);
				DoblePanel.add(txtReferencia);
		
				txtcanal1 = new JTextPane();
				txtcanal1.setText("Canal");
				txtcanal1.setEditable(false);
				txtcanal1.setBounds(146, 102, 37, 20);
				DoblePanel.add(txtcanal1);
				
				ChanelD1 = new JComboBox();
				ChanelD1.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				ChanelD1.setBounds(184, 102, 82, 20);
				DoblePanel.add(ChanelD1);
				
				txtMedicion = new JTextPane();
				txtMedicion.setText("Entrada de Audio - Medici\u00F3n");
				txtMedicion.setEditable(false);
				txtMedicion.setBounds(10, 128, 221, 20);
				DoblePanel.add(txtMedicion);
				
				DispositivosD2 = new JComboBox();
				DispositivosD2.setForeground(Color.BLACK);
				DispositivosD2.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				DispositivosD2.setBounds(45, 152, 221, 20);
				DoblePanel.add(DispositivosD2);
				
				txtcanal2 = new JTextPane();
				txtcanal2.setText("Canal");
				txtcanal2.setEditable(false);
				txtcanal2.setBounds(146, 180, 37, 20);
				DoblePanel.add(txtcanal2);
				
				ChanelD2 = new JComboBox();
				ChanelD2.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				ChanelD2.setBounds(184, 180, 82, 20);
				DoblePanel.add(ChanelD2);
				
		
				JPanel panel_2 = new JPanel(); //Panel de color
				panel_2.setBackground(Color.BLACK);
				panel_2.setBounds(2, 195, 24, 16);
				DoblePanel.add(panel_2);
				panel_2.setLayout(null);
				
						ColorD = new Canvas();
						ColorD.setBounds(0, 0, 24, 16);
						panel_2.add(ColorD);
				
				JTextPane textPane_2 = new JTextPane();
				textPane_2.setText("Color");
				textPane_2.setEditable(false);
				textPane_2.setBounds(25, 193, 37, 20);
				DoblePanel.add(textPane_2);
		
		
		
		btnAceptar = new JButton("");
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setBounds(304, 241, 77, 36);
		getContentPane().add(btnAceptar);
		
	    int ancho=btnAceptar.getWidth();
	    int alto=btnAceptar.getHeight();
		
		ImageIcon imgAceptarOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptar.png"));
		ImageIcon imgAceptarOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptarOn.png"));
		
		ImageIcon imgAceptarEsc=new ImageIcon(imgAceptarOff.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_DEFAULT));
		ImageIcon imgAceptarOnEsc=new ImageIcon(imgAceptarOn.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_AREA_AVERAGING));
	    
	    btnAceptar.setIcon(imgAceptarEsc);
	    btnAceptar.setBorderPainted(false);
	    btnAceptar.setRolloverIcon(imgAceptarOnEsc);
	    btnAceptar.setSelectedIcon(imgAceptarOnEsc);
	    btnAceptar.setBorder(null);
	    
		
		SGError = new JPanel(); //Panel mostrar error en sesi�n y grupo
		SGError.setBackground(new Color(255, 255, 255));
		SGError.setBounds(10, 228, 276, 59);
		getContentPane().add(SGError);
		SGError.setLayout(null);
		
				JTextPane txtpnSesion = new JTextPane();
				txtpnSesion.setBounds(10, 0, 48, 20);
				SGError.add(txtpnSesion);
				txtpnSesion.setText("Sesi\u00F3n");
				txtpnSesion.setEditable(false);
				
				Sesiones = new JComboBox();
				Sesiones.setBounds(10, 28, 111, 20);
				SGError.add(Sesiones);
				Sesiones.setForeground(new Color(47, 79, 79));
				Sesiones.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				Sesiones.setModel(new DefaultComboBoxModel(new String[] {"Default"}));
				
				JTextPane txtpnGrupo = new JTextPane();
				txtpnGrupo.setBounds(142, 0, 48, 20);
				SGError.add(txtpnGrupo);
				txtpnGrupo.setText("Grupo");
				txtpnGrupo.setEditable(false);
		
				Grupos = new JComboBox();
				Grupos.setBounds(143, 28, 111, 20);
				SGError.add(Grupos);
				Grupos.setForeground(new Color(47, 79, 79));
				Grupos.setFont(new Font("Century Gothic", Font.PLAIN, 11));
				setIconImage(Toolkit.getDefaultToolkit().getImage(NuevaMedicion.class.getResource("/ClasesFrontera/Recursos/Imagenes/IconoNuevo.png")));
				setTitle("Medicion Nueva");
				setBounds(100, 100, 419, 327);
				
		
		
		
		//Estados Iniciales
		btnSimple.setSelected(true);
		btnDoble.setSelected(false);
		SimplePanel.setVisible(true);
		DoblePanel.setVisible(false);	
		
		
		//Clase de control de la interfas
		NuevaMControl control=new NuevaMControl();
		control.Guardar_Cerrar();

	}
}
