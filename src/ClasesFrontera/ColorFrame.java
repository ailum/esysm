package ClasesFrontera;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Point;

import javax.swing.JColorChooser;
import javax.swing.JInternalFrame;

import ClasesControl.CarpetaControl;
import ClasesControl.EsysMControl;
import ClasesEntidad.Medicion;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.util.Random;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import ClasesFrontera.EsysM;
import ClasesFrontera.Recursos.Alerta;
import ClasesEntidad.Captura;
import ClasesEntidad.Carpeta;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.tree.TreePath;
import javax.swing.JTextPane;
import javax.swing.JTextField;

public class ColorFrame extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Point ubicacionInicial;
    private boolean estado=true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColorFrame frame = new ColorFrame(Color.decode("#000000"), 'N', null);
					frame.setVisible(true);
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public static String toHexString(Color colour) {
		String hexColour=Integer.toHexString(colour.getRGB() & 0xffffff);
		if (hexColour.length()<6) {
			hexColour="000000".substring(0, 6-hexColour.length())+hexColour;
		}
		return "#"+hexColour;
	}
	

	/**
	 * Create the frame.
	 */
	public ColorFrame(Color color, char tipe, Canvas canvas) {

		
		setBackground(Color.WHITE);
		setBorder(null);
		setBounds(100, 100, 533, 408);
		getContentPane().setLayout(null);
		
		JColorChooser selectColor=new JColorChooser();
		selectColor.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		selectColor.setBorder(null);
		selectColor.setBounds(0, 16, 533, 329);
		selectColor.setOpaque(true);
		selectColor.setBackground(Color.white);	
		AbstractColorChooserPanel[] paneles=selectColor.getChooserPanels();
		selectColor.setChooserPanels(new AbstractColorChooserPanel[] { paneles[0], paneles[3] });
		getContentPane().add(selectColor);
		
		JTextField txtId = new JTextField();
		txtId.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtId.setBounds(350, 352, 86, 20);
		getContentPane().add(txtId);
		txtId.setColumns(10);
		
		
		JTextPane txtnId = new JTextPane();
		txtnId.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtnId.setEditable(false);
		txtnId.setText("Id");
		txtnId.setBounds(332, 352, 16, 20);
		getContentPane().add(txtnId);
		
		
		if (tipe=='C') {
			if (EsysMControl.TreeContC==3) {
				
				if (EsysMControl.captura.getNombre().length()>3) {
					txtId.setText(EsysMControl.captura.getNombre().substring(0, EsysMControl.captura.getNombre().length()-3));
				}else {
					txtId.setText("");
				}
				
				
			}else if (EsysMControl.TreeContC==2) {
				
				if (EsysMControl.carpeta.getNombre().length()>2) {
					txtId.setText(EsysMControl.carpeta.getNombre().substring(0, EsysMControl.carpeta.getNombre().length()-2));
				}else {
					txtId.setText("");
				}

			}
		}
		
		JButton btnAceptar = new JButton("");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Color newColor = selectColor.getColor();
				
				if (tipe == 'M') {

					canvas.setBackground(newColor);

					if (EsysMControl.TreeCont == 4) {
						EsysMControl.medicion.setColor(toHexString(newColor));
					} else if (EsysMControl.TreeCont == 3) {
						EsysMControl.grupo.setColor(toHexString(newColor));

					} else if (EsysMControl.TreeCont == 2) {
						EsysMControl.sesion.setColor(toHexString(newColor));

					}

					EsysM.contenido.SincronizarColor(false, '.');
					dispose();
				} else if (tipe == 'C') {

					if (EsysMControl.TreeContC == 3) {
						
						
						String name=txtId.getText()+EsysMControl.captura.getNombre().substring(EsysMControl.captura.getNombre().length()-3, EsysMControl.captura.getNombre().length());
							
						EsysMControl.captura.setNombre(name);
						EsysMControl.captura.setColor(toHexString(newColor));
						
						int pos = CarpetaControl.Buscar(EsysMControl.carpeta.getNombre(), EsysM.contenidoC.getCarpetas()) + 1;

						EsysM.contenidoC.abrirPath(pos);

						EsysM.treeC.setSelectionRow(pos);

						TreePath nameTreeC = EsysM.treeC.getPathForRow(pos);
						EsysMControl.TreeContC = nameTreeC.getPathCount();

						EsysMControl.DataTreeC(nameTreeC);
					
						EsysM.contenidoC.SincronizarColor("#000000");
						dispose();
						
						
					} else if (EsysMControl.TreeContC == 2) {
						
						
						String name=txtId.getText()+EsysMControl.carpeta.getNombre().substring(EsysMControl.carpeta.getNombre().length()-2, EsysMControl.carpeta.getNombre().length());
						
							EsysMControl.carpeta.setNombre(name);
							EsysMControl.carpeta.setColor(toHexString(newColor));
							
							int pos = CarpetaControl.Buscar(EsysMControl.carpeta.getNombre(), EsysM.contenidoC.getCarpetas()) + 1;

							EsysM.treeC.setSelectionRow(pos);

							TreePath nameTreeC = EsysM.treeC.getPathForRow(pos);
							EsysMControl.TreeContC = nameTreeC.getPathCount();

							EsysMControl.DataTreeC(nameTreeC);
					
							EsysM.contenidoC.SincronizarColor("#000000");
							dispose();
					}
					
				} else {
					canvas.setBackground(newColor);
					dispose();
					EsysMControl.nuevaM.setVisible(true);
				}
				
			}
		});
		
		selectColor.setColor(color);
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setBounds(446, 345, 77, 36);
		btnAceptar.setBorder(null);
		getContentPane().add(btnAceptar);

		((javax.swing.plaf.basic.BasicInternalFrameUI)this.getUI()).setNorthPane(null);
		
	    int ancho=btnAceptar.getWidth();
	    int alto=btnAceptar.getHeight();
	    //----------------------------------------------------------------------
	    
		//Acceso a Imagen de ACEPTAR y Escala
		ImageIcon imgAceptarOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptar.png"));
		ImageIcon imgAceptarOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptarOn.png"));
		
		ImageIcon imgAceptarEsc=new ImageIcon(imgAceptarOff.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_DEFAULT));
		ImageIcon imgAceptarOnEsc=new ImageIcon(imgAceptarOn.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_AREA_AVERAGING));
		//--------------------------------------
	    
	    btnAceptar.setIcon(imgAceptarEsc);
	    btnAceptar.setBorderPainted(false);
	    btnAceptar.setRolloverIcon(imgAceptarOnEsc);
	    btnAceptar.setSelectedIcon(imgAceptarOnEsc);
	    
		JButton btnMover = new JButton("");
		btnMover.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int thisX = getLocation().x;
	            int thisY = getLocation().y;

	            // Determine how much the mouse moved since the initial click
	            int xMoved = e.getX() - ubicacionInicial.x;
	            int yMoved = e.getY() - ubicacionInicial.y;

	            // Move window to this position
	            int X = thisX + xMoved;
	            int Y = thisY + yMoved;
	            setLocation(X, Y);
			}
		});
		btnMover.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				ubicacionInicial=e.getPoint();
			}
		});
		btnMover.setBackground(Color.WHITE);
		btnMover.setBounds(490, 0, 16, 16);
		getContentPane().add(btnMover);

		ImageIcon moverOff = new ImageIcon(this.getClass().getResource("Recursos/Imagenes/movimiento.png"));
		ImageIcon moverEsc = new ImageIcon(moverOff.getImage().getScaledInstance(btnMover.getWidth(), btnMover.getHeight(), java.awt.Image.SCALE_SMOOTH));
		
		btnMover.setIcon(moverEsc);		
		btnMover.setBorder(null);
		
		JButton cerrar = new JButton("");
		cerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (tipe == 'M' | tipe == 'C') {
					dispose();
				}
				else {
				dispose();
				EsysMControl.nuevaM.setVisible(true);}
				
			}
		});
		cerrar.setBorder(null);
		cerrar.setBackground(Color.WHITE);
		cerrar.setBounds(516, 0, 16, 16);
		getContentPane().add(cerrar);
		
		ImageIcon cerrarOff = new ImageIcon(this.getClass().getResource("Recursos/Imagenes/cancelar.png"));
		ImageIcon cerrarEsc = new ImageIcon(cerrarOff.getImage().getScaledInstance(cerrar.getWidth(), cerrar.getHeight(), java.awt.Image.SCALE_SMOOTH));
		
		cerrar.setIcon(cerrarEsc);
		

		
		

	}
}
