package ClasesFrontera;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.AbstractColorChooserPanel;

import java.awt.Color;
import javax.swing.JColorChooser;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class ColorDialog extends JDialog {

	/**
	 * Launch the application.
	 * 
	 */
	
	private Color newColor;
    private Point ubicacionInicial;
	
	public static void main(String[] args) {
		try {
			ColorDialog dialog = new ColorDialog(Color.decode("#000000"));
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static String toHexString(Color colour) {
		String hexColour=Integer.toHexString(colour.getRGB() & 0xffffff);
		if (hexColour.length()<6) {
			hexColour="000000".substring(0, 6-hexColour.length())+hexColour;
		}
		return "#"+hexColour;
	}
	
	public String getColor() {
		return toHexString(newColor);
	}

	/**
	 * Create the dialog.
	 */
	public ColorDialog(Color color) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ColorDialog.class.getResource("/ClasesFrontera/Recursos/Imagenes/colorchooser.png")));
		setTitle("Color de Nueva Medici\u00F3n");

		setModal(true);
		setResizable(false);
		getContentPane().setBackground(Color.WHITE);
		setBounds(100, 100, 531, 398);
		getContentPane().setLayout(null);
		
		newColor=color;
		
		{
			
			ImageIcon cerrarOff = new ImageIcon(this.getClass().getResource("Recursos/Imagenes/cancelar.png"));
		}
		
			JColorChooser selectColor = new JColorChooser();
			selectColor.setFont(new Font("Century Gothic", Font.PLAIN, 12));
			selectColor.setBorder(null);
			selectColor.setBounds(0, 0, 533, 329);
			selectColor.setOpaque(true);
			selectColor.setBackground(Color.white);	
			AbstractColorChooserPanel[] paneles=selectColor.getChooserPanels();
			selectColor.setChooserPanels(new AbstractColorChooserPanel[] { paneles[0], paneles[3] });
			getContentPane().add(selectColor);
			
			selectColor.setColor(color);
			
		
		{
			JButton btnAceptar = new JButton("");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					newColor=selectColor.getColor();
					dispose();
				}
			});
			btnAceptar.setBorderPainted(false);
			btnAceptar.setBorder(null);
			btnAceptar.setBackground(Color.WHITE);
			btnAceptar.setBounds(448, 330, 77, 36);
			getContentPane().add(btnAceptar);
			
			
			int ancho=btnAceptar.getWidth();
		    int alto=btnAceptar.getHeight();
		    //----------------------------------------------------------------------
		    
			//Acceso a Imagen de ACEPTAR y Escala
			ImageIcon imgAceptarOff= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptar.png"));
			ImageIcon imgAceptarOn= new ImageIcon(this.getClass().getResource("Recursos/Imagenes/BotonAceptarOn.png"));
			
			ImageIcon imgAceptarEsc=new ImageIcon(imgAceptarOff.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_DEFAULT));
			ImageIcon imgAceptarOnEsc=new ImageIcon(imgAceptarOn.getImage().getScaledInstance(ancho, alto, java.awt.Image.SCALE_AREA_AVERAGING));
			//--------------------------------------
		    
		    btnAceptar.setIcon(imgAceptarEsc);
		    btnAceptar.setBorderPainted(false);
		    btnAceptar.setRolloverIcon(imgAceptarOnEsc);
		    btnAceptar.setSelectedIcon(imgAceptarOnEsc);
		}
		{
			
			ImageIcon moverOff = new ImageIcon(this.getClass().getResource("Recursos/Imagenes/movimiento.png"));

		}
	}

}
