package ClasesFrontera;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import javax.swing.InputMap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class CarpetaInicio extends JFrame {

	private JPanel contentPane;
	Timer t = new Timer();
	private JTextField dir;
	private static CarpetaInicio frame;
	private String direccion="";
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					frame = new CarpetaInicio(false, "");
					
					Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
					Dimension ventana = frame.getSize();
					frame.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);

					frame.setVisible(true);			

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	private void cerrar() {
		this.setVisible(false);
	}
	

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public CarpetaInicio(boolean newproject, String direct) throws IOException {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(CarpetaInicio.class.getResource("/ClasesFrontera/Recursos/Imagenes/Opciones.png")));
		setAlwaysOnTop(true);
		setAutoRequestFocus(false);
		setTitle("EsysM 2.0");
		
		setUndecorated(true);
		
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 415, 154);
		contentPane = new JPanel();
			contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(new Color(0, 128, 128), 2, true));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		
		File miDir = new File (".");
		direccion=miDir.getCanonicalPath().toString();

		
		dir = new JTextField();
		dir.setForeground(new Color(0, 128, 128));
		dir.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		dir.setBounds(120, 70, 269, 20);
		contentPane.add(dir);
		dir.setColumns(10);
		
		dir.setText(direccion);
		
		JTextPane txtpnIngreseLaDireccin = new JTextPane();
		txtpnIngreseLaDireccin.setForeground(new Color(47, 79, 79));
		txtpnIngreseLaDireccin.setEditable(false);
		txtpnIngreseLaDireccin.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtpnIngreseLaDireccin.setText("Porfavor, ingrese la direcci\u00F3n del proyecto");
		txtpnIngreseLaDireccin.setBounds(10, 39, 311, 20);
		contentPane.add(txtpnIngreseLaDireccin);
		
		JButton btnExplorar = new JButton("Explorar");
		btnExplorar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fileChooser.setApproveButtonText("Establecer Carpeta");
				
				
				int seleccion = fileChooser.showOpenDialog(frame);

				
				if(seleccion == JFileChooser.APPROVE_OPTION) {
				       direccion=fileChooser.getSelectedFile().getAbsolutePath();
				       dir.setText(direccion);
				}

	
			}
		});
		btnExplorar.setForeground(new Color(47, 79, 79));
		btnExplorar.setBackground(new Color(255, 255, 255));
		btnExplorar.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		btnExplorar.setBounds(10, 69, 100, 23);
		contentPane.add(btnExplorar);
		
		JButton btnContinuar = new JButton("Continuar");	
		
		
		
		
		
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Inicio iniciar=new Inicio();
				
				Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension ventana = iniciar.getSize();
				iniciar.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
				
				iniciar.setVisible(true);
				
				iniciar.iniciar();
				iniciar.setDireccion(direccion);
				
				cerrar();
				
			}
		});

		
		btnContinuar.setForeground(new Color(0, 128, 128));
		btnContinuar.setBackground(new Color(255, 255, 255));
		btnContinuar.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		btnContinuar.setBounds(300, 120, 89, 23);
		contentPane.add(btnContinuar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (newproject==true) {
					EsysM esysm=new EsysM(direct);
					esysm.frmEsysm.setVisible(true);
					cerrar();
				}else {
					System.exit(0);
				}
				
			}
		});
		btnCancelar.setForeground(new Color(0, 128, 128));
		btnCancelar.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		btnCancelar.setBackground(Color.WHITE);
		btnCancelar.setBounds(201, 120, 89, 23);
		contentPane.add(btnCancelar);
		
		JTextPane txtpnEsysm = new JTextPane();
		txtpnEsysm.setText("EsysM 2.0");
		txtpnEsysm.setForeground(new Color(47, 79, 79));
		txtpnEsysm.setFont(new Font("Century Gothic", Font.PLAIN, 11));
		txtpnEsysm.setEditable(false);
		txtpnEsysm.setBounds(357, 3, 55, 20);
		contentPane.add(txtpnEsysm);

		

	}
}
