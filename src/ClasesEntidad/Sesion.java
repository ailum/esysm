package ClasesEntidad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import ClasesControl.CarpetaControl;

public class Sesion {

    private String nombre, color;  
    private ArrayList<Grupo> grupos = new ArrayList<Grupo>();
    private ImageIcon icon;

    public Sesion(String nombre, String color) {
    	  this.nombre = nombre;
    	  this.icon=iconSesion(color);
    	  this.color=color;
    }
    

    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
    	this.nombre=nombre;
    }
    
    public void addGrupo(Grupo grupo)
    {
        grupos.add(grupo);
    }

    public ArrayList<Grupo> getGrupos() {
        return grupos;
    }    
    
    public Grupo getGrupo(int n) {
        return grupos.get(n);
    }  
    
    public void ClearGrupo(String grupo) {
    	grupos.remove(CarpetaControl.Buscar(grupo, grupos));
    }

    public ImageIcon getIcon() {
            return icon; 
    }

    public Color getColor() {
    	return Color.decode(color);
    }
    
    public void setColor(String color) {
    	this.color=color;
    	this.icon=iconSesion(color);
    }
    
	private ImageIcon iconSesion(String color) {

		Color iconoColorS=Color.decode(color);
		JPanel iconoSesion = new JPanel();
		iconoSesion.setBounds(0, 0, 10, 10);
		iconoSesion.setBackground(iconoColorS);
		
		int wS = iconoSesion.getWidth();
		int hS = iconoSesion.getHeight();
		BufferedImage biS = new BufferedImage(wS, hS, BufferedImage.TYPE_INT_RGB);
		Graphics2D gS = biS.createGraphics();
		iconoSesion.print(gS);
		gS.dispose();
		
		return new ImageIcon(biS);	
	}

    @Override
    public String toString() {
        return "S: "+nombre;
    }

}
