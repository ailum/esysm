package ClasesEntidad.Recursos;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;

import ClasesFrontera.Recursos.Recursos;

public class Dispositivo {
	
	protected DataLine Linea;
	protected int nCanales, nBits;
	protected float fMuestreo;
	protected String nombre;
	protected boolean habilitado=true;

	public Dispositivo(DataLine dispositivo, String nombre) {
		this.Linea=dispositivo;	
		nBits=dispositivo.getFormat().getSampleSizeInBits();//Obtener profundidad de bits de la linea
		nCanales=dispositivo.getFormat().getChannels(); //Obtener canales de la linea en formato DataLine 
		fMuestreo=dispositivo.getFormat().getSampleRate(); //obtener frecuencia de muestreo de la l�nea
		this.nombre=nombre;
	}
	
	public DataLine getLinea() {
		return Linea;
	}

	public void setLinea(DataLine linea) {
		Linea = linea;
	}
	
	public String getNombre() {
		return nombre;
	}

	public int getnBits() {
		return nBits;
	}
	
	public int getnCanales() {
		return nCanales;
	}

	public float getfMuestreo() {
		return fMuestreo;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	@Override
	public String toString() {
		return "Dispositivo [nombre=" + nombre + ", habilitado=" + habilitado + ", nCanales=" + nCanales + ", nBits="
				+ nBits + ", fMuestreo=" + fMuestreo + "]";
	}

	
	
}
