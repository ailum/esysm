package ClasesEntidad.Recursos;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LecturaAudioDato {

	private byte[] AudioDato;
	private int n=0; //conteo de dato dentro del array de bytes de audio leidos 
	private char Chanel;
	
	
	public LecturaAudioDato(byte[] dato, int n, char chanel2) {
		AudioDato=dato;
		this.n=n;
		this.Chanel=chanel2;
	}
	
	
	public static double ByteVolt(int byteDato){//conversi�n de cualquier byte a voltaje
		double volt =0.0004*(byteDato)-(5.*Math.pow(10., -15.));
		return Math.round(volt*100d)/100d;
	}
	
	
	public double DatoCanal() {
		
	double dato;	
	short byteCompacto=0;	
		
		if(Chanel=='L') {//Validaci�n de canal
			
			ByteArrayOutputStream datoByte=new ByteArrayOutputStream();//Creaci�n de outstream tipo byte
			datoByte.write(AudioDato[n]); //se escribe byte 1 y byte 2 para profundidad de bits de 16 canal R
			datoByte.write(AudioDato[n+1]);
			
			byte[] byteArray=datoByte.toByteArray();//se genera un array de bytes con los bytes escritor en el streambyte
			
			byteCompacto=ByteBuffer.wrap(byteArray).order(ByteOrder.BIG_ENDIAN).getShort();// se compactan los bytes y se obtiene el dato tipo short con formato big Endian
			
		}
		else if(Chanel=='R'){
			
			ByteArrayOutputStream datoByte=new ByteArrayOutputStream();
			datoByte.write(AudioDato[n+2]);//se escribe byte 3 y byte 4 para profundidad de bits de 16 canal L
			datoByte.write(AudioDato[n+3]);
			
			byte[] byteArray=datoByte.toByteArray();
			
			byteCompacto=ByteBuffer.wrap(byteArray).order(ByteOrder.BIG_ENDIAN).getShort();
			
		}
		
		dato=ByteVolt((int)byteCompacto);
		
		return dato;
	}

	
}
