package ClasesEntidad.Recursos;

import java.util.ArrayList;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JComboBox;

import ClasesControl.EsysMControl;

import javax.sound.sampled.Mixer.Info;
import javax.sound.sampled.Port;
import javax.sound.sampled.SourceDataLine;

public class FiltroDisp {
	
	//Variables de Recursos de Audio del sistema por Libreria DATALINE
	private Info[] infos=AudioSystem.getMixerInfo();
	public Mixer[] AudioRecursosCap=new Mixer[infos.length];
	public Mixer[] AudioRecursosRep=new Mixer[infos.length];
	//--------------------------------------------------
	

	public void Captura() throws LineUnavailableException {

		int p=0; //Conteo exterior que aumenta si la condicion es TRUE
		
		boolean sinControladorSonido=false;

		for (int i = 0; i < infos.length; i++) {

			Mixer mixer = AudioSystem.getMixer(infos[i]);//Acceso a Mixer desde la informacion de Dispositivos de Audio.

			Line.Info[] infomixer = mixer.getTargetLineInfo(new Line.Info(TargetDataLine.class));//Acceso a información de Dispositivos tipo TARGETDATALINE o Captura	

			if (infomixer.length > 0 && mixer.getClass().getName().equals("com.sun.media.sound.DirectAudioDevice")) { //condicional para accerder a puertos de Captura
				
				
				if (sinControladorSonido==false) {
					i++;
					sinControladorSonido=true;
				}
				
				DataLine linea=(DataLine) mixer.getLine(infomixer[0]);

				CapturaAudio nuevo=new CapturaAudio((TargetDataLine)linea, infos[i].getName()); 
				
				EsysMControl.dispositivosCap.add(nuevo);

			}

			if (infomixer.length > 0 || (mixer.isLineSupported(Port.Info.LINE_IN))
					|| (mixer.isLineSupported(Port.Info.MICROPHONE))) {//Condicional para acceder a lineas soportadas tipo MIC o LINEA
				
				AudioRecursosCap[p] = mixer;//Almacenamiento de mixer que soporta la linea
				p++;
			}
	
		}

	}
	
	
	public void Reproduccion() throws LineUnavailableException {

		int p=0;

		for (int i = 0; i < infos.length; i++) {

			Mixer mixer = AudioSystem.getMixer(infos[i]);

			Line.Info[] infomixer = mixer.getSourceLineInfo();//Acceso a información de Dispositivos tipo SOURCEDATALINE o Reproducción

			if (infomixer.length > 0 && mixer.getClass().getName().equals("com.sun.media.sound.DirectAudioDevice")) {//condicional para accerder a puertos de Reproducción
				
				DataLine linea=(SourceDataLine) mixer.getLine(infomixer[0]);//Almacenamiento de DATALINE para acceso a puertos
				
				FuenteAudio nuevo=new FuenteAudio((SourceDataLine)linea, infos[i].getName()); 
				
				EsysMControl.dispositivosRep.add(nuevo);

			}

			if (infomixer.length > 0 || (mixer.isLineSupported(Port.Info.LINE_OUT))
					|| (mixer.isLineSupported(Port.Info.SPEAKER))) {//Condicional para acceder a lineas soportadas tipo SPEAK o LINEA

				AudioRecursosRep[p] = mixer;//Almacenamiento de mixer que soporta la linea
				p++;
			}

		}

	}
	
}
