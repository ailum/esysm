package ClasesEntidad.Recursos;

import java.awt.Canvas;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.Control;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JTextPane;

import ClasesControl.Recursos.dBPIX;
import ClasesFrontera.Recursos.Recursos;


public class CapturaAudio extends Dispositivo {

	private TargetDataLine linea;
	private boolean stopped=false; //control del hilo de prueba
	private AudioFormat format = new AudioFormat(44100.0F, 16, 2, true, true);// formato de audio a 16bit de profundidad, con signo y big endian(bit de mayor peso)
	private int countStopped=0; //variable contador de la prueba
	private char Chanel1, Chanel2;
	private JTextPane texto1, texto2;
	private Canvas nivelVisual1, nivelVisual2;
	private Ventana TipoV;
	private Magnitud TipoM;
	private boolean prueba=false;
	private boolean mismaLinea=false;
	
	public enum Ventana{
		RECTANGULAR,
		HAMMING,
		HANNING,
		BLACKMAN,
		BLACKMANHARRIS,
		FLATTOP,
		TUKEY,
		BARTTLET;
	}
	
	public enum Magnitud{
		DBFS,
		DBU,
		DBV,
		DBSPL;
	}
	
	public CapturaAudio(TargetDataLine linea, String nombre) {
		super(linea, nombre);
		this.linea=linea;
	}

	public void Iniciar(boolean mismaLinea,char c, Ventana TipoV, Magnitud TipoM, JTextPane texto, Canvas nivelVisual, boolean prueba) throws LineUnavailableException {
		this.Chanel1 = c;
		this.TipoV=TipoV;
		this.TipoM=TipoM;
		this.texto1=texto;
		this.nivelVisual1=nivelVisual;
		this.prueba=prueba;
		this.mismaLinea=mismaLinea;
		
		stopped=false;
		CapturaOn captura = new CapturaOn();
		captura.start();// inicio del hilo de captura
	}

	public void linea2(char c, JTextPane texto, Canvas nivelVisual) {
		this.Chanel2 = c;
		this.texto2=texto;
		this.nivelVisual2=nivelVisual;
	}
	
	public void Detener() {
	
		CapturaOff capturaoff= new CapturaOff();
		capturaoff.start();	// inicio del hilo para detener la captura
		linea.stop();
	}

	private class CapturaOn extends Thread {

		private double sumvoz = 0;// variable que almacena el voltaje Rms
		private double sumvoz2=0;
		private int BufferlengthChanel = 4096;

		private int bufferSize = (int) BufferlengthChanel * format.getFrameSize();// el tama�o del frame es de 4 bits (que se dividen entre 2 de R y dos de L para 16bits)
		private byte[] AudioBytes = new byte[bufferSize];// Variable donde se almacenan los bites de captura.

		private int cont=0;

		public void run() {

			try {

				linea.open(format);
				linea.start();
				countStopped=0;

				while (!stopped) {

					sumvoz = 0;
					sumvoz2 = 0;
	
					cont = linea.read(AudioBytes, 0, AudioBytes.length);

					for (int n = 0; n < bufferSize; n += 4) {

						LecturaAudioDato AudioVolt = new LecturaAudioDato(AudioBytes, n, Chanel1);// clase encargada de la conversion a voltaje
						sumvoz = sumvoz + Math.pow(AudioVolt.DatoCanal(), 2); // suma de cada voltaje elevado a una potencia de 2
						
						if (mismaLinea==true) {
						LecturaAudioDato AudioVolt2 = new LecturaAudioDato(AudioBytes, n, Chanel2);
						sumvoz2 = sumvoz2 + Math.pow(AudioVolt2.DatoCanal(), 2);}
					}

					AudiodB dBdato = new AudiodB(Math.sqrt(sumvoz / (bufferSize / 4))); // C�lculo de dB del voltaje Rms
					dBPIX Pixels = new dBPIX(dBdato.dBFs(), nivelVisual1); // Envio del dato en dB a conversi�n de pixeles
					
					Pixels.PixdBFs();
					
					
					String dato1;
					
					if (dBdato.dBFs()>-114) {
						dato1=Double.toString(dBdato.dBFs());
					}
					else {
						dato1="-inf";
					}
					
					texto1.setText(dato1);
					
					if (mismaLinea==true) {
					AudiodB dBdato2 = new AudiodB(Math.sqrt(sumvoz2 / (bufferSize / 4)));
					dBPIX Pixels2 = new dBPIX(dBdato2.dBFs(), nivelVisual2);
					Pixels2.PixdBFs();
					
						String dato2;

						if (dBdato2.dBFs() > -114) {
							dato2 = Double.toString(dBdato2.dBFs());
						} else {
							dato2 = "-inf";
						}
					
					texto2.setText(dato2);
					}
					

					linea.flush();// descarga de linea

					Thread.sleep(100);// espera del thread

					if (countStopped < 100 && prueba==true) {
						countStopped++; // Limitacion de tiempo de prueba
					} else if (countStopped >= 100 && prueba==true){
						Detener();
						dBPIX.Inicial(nivelVisual1);
						if (mismaLinea==true) {
						dBPIX.Inicial(nivelVisual2);}
						Recursos.btnProbar.setSelected(false);
					}

				}

				dBPIX.Inicial(nivelVisual1);
				if (mismaLinea==true) {
				dBPIX.Inicial(nivelVisual2);}

			} catch (LineUnavailableException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
	
	private class CapturaOff extends Thread {
		public void run() {
			stopped = true;
		}
	}
	
	public void suspender(boolean estado) {

		if (estado==false) {
			stopped=false;
			CapturaOn captura = new CapturaOn();
			captura.start();
		}else {
			Detener();
		}
			
	}
	
	public boolean getMidiendo() {
		if (stopped==true) {
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public String toString() {
		return super.getNombre();
	}
	  
	
}
