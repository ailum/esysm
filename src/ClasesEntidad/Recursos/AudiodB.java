package ClasesEntidad.Recursos;

import java.text.DecimalFormat;

public class AudiodB {

	private double Volt;
	
	public AudiodB(double Volt) {
		this.Volt=Volt;//nivel de voltaje;
	}
		
	public double dBFs() {
		double dB=(10.156*Math.log(Volt))-21.986; //conversión a dBFs
		return Math.round(dB*100d)/100d;
	}
	
	public double dBspl(double refer) {
		double dB=94.+20*Math.log10(Volt/refer); //conversión a dBspl
		return dB;
	}
	
	public double dBu(double refer) {
		double dB=20*Math.log10(Volt/refer); //conversión a dBu
		return dB;
	}
	
	public double dBV() {
		double dB=20*Math.log10(Volt); //conversión a dBV
		return dB;
	}
}
