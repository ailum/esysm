package ClasesEntidad.Recursos;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import ClasesControl.Recursos.dBPIX;
import ClasesFrontera.Recursos.Recursos;

public class FuenteAudio extends Dispositivo {

	private SourceDataLine linea;
	private boolean stopped=false; //control del hilo de prueba
	private AudioFormat format = new AudioFormat(44100.0F, 16, 2, true, true);// formato de audio a 16bit de profundidad, con signo y big endian(bit de mayor peso)
	private int countStopped = 0;
	private char Chanel;
	private FloatControl volCtrl;
	private short NivelShort=0;

	public FuenteAudio(SourceDataLine linea, String nombre) {
		super(linea, nombre);
		this.linea = linea;
		
	}

	public void Iniciar(char canal) throws LineUnavailableException {
		this.Chanel=canal;
		stopped=false;
		ReprodOn Reprod = new ReprodOn();
		Reprod.start();// inicio del hilo de captura
	}

	public void Detener() {
		linea.flush();
		linea.stop();
		ReprodOff reprodoff = new ReprodOff();
		reprodoff.start(); // inicio del hilo para detener la captura
	}

	public void Master(float value) {

		if (volCtrl.getValue()<5 && value>0) {
		volCtrl.setValue(volCtrl.getValue()+value);
		//System.out.println(volCtrl.getValue());
		}
		else if (volCtrl.getValue()<=6 && value<0) {
		volCtrl.setValue(volCtrl.getValue()+value);	
		//System.out.println(volCtrl.getValue());
		}

		
	}
	
	private class ReprodOn extends Thread {

		private double Frecuencia = 1000. / (2*format.getFrameRate()); // frecuencia en radianes f/fs
		private double conteoCiclo = 0; // contador donde se almacena datos de aumento +Frecuencia
		
		public void run() {

			try {

				linea.open(format);
				linea.start();
				
				volCtrl = (FloatControl) linea.getControl(FloatControl.Type.MASTER_GAIN);
				if (NivelShort==0) {
				volCtrl.setValue(-18);// se establece nivel inicial de la linea
				}
								
				ByteBuffer byteFuente = ByteBuffer.allocate(linea.getBufferSize());//Crear un buffer tipo byte de capacidad igual al buffer size de la linea
				
				while (!stopped ) {

					NivelShort=(short) Math.pow(10.,(90.+volCtrl.getValue())/20.);
					
					byteFuente.clear();

					int TotalDato = linea.available() / format.getFrameSize();// Obtener cantidad total de datos de un solo canal

					for (int n = 0; n < TotalDato*2; n++) {

						if (Chanel=='R') {

							if (n % 2 != 0) { //Valores de n correspondientes a posiciones impares equivalen a canal R

								byteFuente.putShort((short) (NivelShort * Math.sin(2 * Math.PI * conteoCiclo)));// Escribir sobre el buffer tipo byte una funcion
																											// sinusoide de amplitud de 4000 tipo short
							} else {
								byteFuente.putShort((short) 0);// Posiciones pares se envia un 0 (Canal L)
							}
							
						} else {
							if (n % 2 == 0) {//Valores de n correspondientes a posiciones pares equivalen a canal L

								byteFuente.putShort((short) (NivelShort * Math.sin(2 * Math.PI * conteoCiclo)));// Escribir sobre el buffer tipo byte una funcion
																											// sinusoide de amplitud de 4000 tipo short
							} else {
								byteFuente.putShort((short) 0);//Posiciones impares se envia un 0 (Canal R)
							}
						}
						if (!stopped ) { //Evalua el estado de la prueba 
							conteoCiclo += Frecuencia;// aumenta el conteo de ciclio en +Frecuencia
							if (conteoCiclo > 1) {// condicional que reinicia el conteo para reiniciar sinusoide
								conteoCiclo -= 1;
							}
						} else {
							break;
						}
					}

					double volt = LecturaAudioDato.ByteVolt(NivelShort);// covnersion de 4000 tipo short a voltaje

					AudiodB dBdato = new AudiodB(volt / Math.sqrt(2)); // C�lculo de dB del voltaje Rms
					dBPIX Pixels = new dBPIX(dBdato.dBFs(), Recursos.Niveles); // Envio del dato en dB a conversi�n de pixeles

					Pixels.PixdBFs();

					DecimalFormat df = new DecimalFormat("#.00");
					
					Recursos.RmsNivel.setText(df.format(dBdato.dBFs()));//mostrar dato dB en seccion de texto
	
					linea.write(byteFuente.array(), 0, byteFuente.position()); //Se escribe el buffer tipo byte en la linea

					Thread.sleep(50);

					if (countStopped < 100) {
						countStopped++;
					} else {
						Detener();
						dBPIX.Inicial(Recursos.Niveles);
						Recursos.btnProbar.setSelected(false);
					}

				}

				dBPIX.Inicial(Recursos.Niveles);

			} catch (LineUnavailableException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class ReprodOff extends Thread {
		public void run() {
			stopped = true;
			countStopped = 0;
		}
	}
}
