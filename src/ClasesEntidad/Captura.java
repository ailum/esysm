package ClasesEntidad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.sound.sampled.DataLine;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Captura {

    private String nombre, color;
    private ImageIcon icon;

    public Captura(String nombre, String color){
    	this.nombre=nombre;
    	this.color=color;
    	this.icon=iconCaptura(color);
    }    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public ImageIcon getIcon() {
        return icon;
    }
    
    public Color getColor() {
    	return Color.decode(color);
    }
    
    public void setColor(String color) {
    	this.color=color;
    	this.icon=iconCaptura(color);
    }
    
    
	private ImageIcon iconCaptura(String color){
		
		Color iconoColorM=Color.decode(color);
		JPanel iconoMedicion = new JPanel();
		iconoMedicion.setBounds(0, 0, 10, 10);
		iconoMedicion.setBackground(iconoColorM);
		
		int wM = iconoMedicion.getWidth();
		int hM = iconoMedicion.getHeight();
		BufferedImage biM = new BufferedImage(wM, hM, BufferedImage.TYPE_INT_RGB);
		Graphics2D gM = biM.createGraphics();
		iconoMedicion.print(gM);
		gM.dispose();
		
		return new ImageIcon(biM);
	}


    @Override
    public String toString() {
        return "Img: "+nombre;
    }

}
