package ClasesEntidad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.charset.Charset;

import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import ClasesEntidad.Recursos.CapturaAudio;
import ClasesEntidad.Recursos.Dispositivo;

public class Medicion {

	    private String nombre, color;
	    private ImageIcon icon;
	    private DataLine lineaM,lineaR;
	    private char canalR, canalM;
	    private CapturaAudio[] Dispositivo;
	    private boolean activo=false;

	    public Medicion(String nombre, String color, CapturaAudio[] Dispositivo, char[] canal){
	        
	    	this.nombre = nombre;
	        this.icon=iconMedicion(color);    
	        
			if (Dispositivo[0] != null) {
				this.lineaR = Dispositivo[0].getLinea();
				this.canalR = canal[0];
			} else {
				this.lineaR = null;
				this.canalR = '.';
			}
			
			this.lineaM = Dispositivo[1].getLinea();
			this.canalM = canal[1];
			this.Dispositivo = Dispositivo;
			
	    }    
  
		public Dispositivo[] getDisp() {
			return Dispositivo;
		}

		public void setDisp(CapturaAudio[] disp) {
			this.Dispositivo = disp;
		}

		public String getNombre() {
	        return nombre;
	    }

	    public void setNombre(String Nombre) {
	        this.nombre = Nombre;
	    }

	    public DataLine getLineaM() {
			return lineaM;
		}

		public void setLineaM(DataLine lineaM) {
			this.lineaM = lineaM;
		}

		public DataLine getLineaR() {
			return lineaR;
		}

		public void setLineaR(DataLine lineaR) {
			this.lineaR = lineaR;
		}
		
		public char getCanalR() {
			return canalR;
		}

		public void setCanalR(char canalR) {
			this.canalR = canalR;
		}

		public char getCanalM() {
			return canalM;
		}

		public void setCanalM(char canalM) {
			this.canalM = canalM;
		}

		public void activar(boolean act) {
			this.activo=act;
		}
		
		public boolean getActivo() {
			return activo;
		}
		
		public Color getColor() {
	    	return Color.decode(color);
	    }
	    
	    public void setColor(String color) {
	    	this.color=color;
	    	this.icon=iconMedicion(color);
	    }
	    
	    
	    public ImageIcon getIcon() {
	    	return icon;
	    }
	    
		private ImageIcon iconMedicion(String color){
			
	        this.color=color;
	        
			Color iconoColorM=Color.decode(color);
			JPanel iconoMedicion = new JPanel();
			iconoMedicion.setBounds(0, 0, 10, 10);
			iconoMedicion.setBackground(iconoColorM);
			
			int wM = iconoMedicion.getWidth();
			int hM = iconoMedicion.getHeight();
			BufferedImage biM = new BufferedImage(wM, hM, BufferedImage.TYPE_INT_RGB);
			Graphics2D gM = biM.createGraphics();
			iconoMedicion.print(gM);
			gM.dispose();
			
			return new ImageIcon(biM);
		}


		public DataLine getLinea(int MR) {
			
			if (MR == 0) {
				return lineaR;
			} else {
	
				return lineaM;
			}
		}

		public void setLinea(TargetDataLine linea, int MR) {
			
			if (MR == 0) {
				this.lineaR = linea;
			} else {
	
				this.lineaM = linea;
			}
			
		}

		@Override
	    public String toString() {
	        return "M: " + nombre;
	    }

}
