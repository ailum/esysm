package ClasesEntidad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import ClasesControl.CarpetaControl;

public class Carpeta {

	   private String nombre, color;  
	    private ArrayList<Captura> capturas = new ArrayList<Captura>();
	    private ImageIcon icon;

	    public Carpeta(String nombre, String color) {
	    	  this.nombre = nombre;
	    	  this.icon=iconCarpeta(color);
	    	  this.color=color;
	    }
	    

	    public String getNombre() {
	        return nombre;
	    }
	    
	    public void setNombre(String nombre) {
	    	this.nombre=nombre;
	    }

	    public void addCaptura(Captura captura)
	    {
	        capturas.add(captura);
	    }

	    public ArrayList<Captura> getCapturas() {
	        return capturas;
	    }    
	    
	    public Captura getCaptura(int n) {
	        return capturas.get(n);
	    }  

	    public void ClearCaptura(String captura) {
	    	capturas.remove(CarpetaControl.Buscar(captura, capturas));
	    }
	    
	    public ImageIcon getIcon() {
	            return icon; 
	    }

	    public Color getColor() {
	    	return Color.decode(color);
	    }
	    
	    public void setColor(String color) {
	    	this.color=color;
	    	this.icon=iconCarpeta(color);
	    }
	    
		private ImageIcon iconCarpeta(String color) {

			Color iconoColorS=Color.decode(color);
			JPanel iconoSesion = new JPanel();
			iconoSesion.setBounds(0, 0, 10, 10);
			iconoSesion.setBackground(iconoColorS);
			
			int wS = iconoSesion.getWidth();
			int hS = iconoSesion.getHeight();
			BufferedImage biS = new BufferedImage(wS, hS, BufferedImage.TYPE_INT_RGB);
			Graphics2D gS = biS.createGraphics();
			iconoSesion.print(gS);
			gS.dispose();
			
			return new ImageIcon(biS);	
		}

	    @Override
	    public String toString() {
	        return "Carpt: "+nombre;
	    }

}
