package ClasesEntidad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import ClasesControl.CarpetaControl;

public class Grupo {

	   private String nombre, color;  
	    private ArrayList<Medicion> mediciones = new ArrayList<Medicion>();
	    private ImageIcon icon;

	    public Grupo(String nombre, String color) {
	    	  this.nombre = nombre;
	    	  this.icon=iconGrupo(color);
	    	  this.color=color;
	    }
	    

	    public String getNombre() {
	        return nombre;
	    }
	    
	    public void setNombre(String nombre) {
	    	this.nombre=nombre;
	    }

	    public void addMedicion(Medicion medicion)
	    {
	        mediciones.add(medicion);
	    }

	    public ArrayList<Medicion> getMediciones() {
	        return mediciones;
	    }       

	    public Medicion getMedicion(int n) {
			return mediciones.get(n);
	    }
	    
	    public void ClearMedicion(String medicion) {
	    	mediciones.remove(CarpetaControl.Buscar(medicion, mediciones));
	    }
	    
	    public ImageIcon getIcon() {
	            return icon; 
	    }

	    public Color getColor() {
	    	return Color.decode(color);
	    }

	    public void setColor(String color) {
	    	this.color=color;
	    	this.icon=iconGrupo(color);
	    }
	    
		private ImageIcon iconGrupo(String color) {
			
			Color iconoColorG=Color.decode(color);
			JPanel iconoGrupo = new JPanel();
			iconoGrupo.setBounds(0, 0, 10, 10);
			iconoGrupo.setBackground(iconoColorG);
			
			int wG = iconoGrupo.getWidth();
			int hG = iconoGrupo.getHeight();
			BufferedImage biG = new BufferedImage(wG, hG, BufferedImage.TYPE_INT_RGB);
			Graphics2D gG = biG.createGraphics();
			iconoGrupo.print(gG);
			gG.dispose();
			
			return new ImageIcon(biG);
		}

	    @Override
	    public String toString() {
	        return "Grp: "+nombre;
	    }

}
