package ClasesEntidad;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import ClasesControl.CarpetaControl;

public class ProyectoC {

 	private String Nombre;
    private ArrayList<Carpeta> carpetas = new ArrayList<Carpeta>();
    private ImageIcon icon;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void addCarpeta(Carpeta carpeta){
        carpetas.add(carpeta);
    }

    public ArrayList<Carpeta> getCarpetas() {
        return carpetas;
    }
    
    public Carpeta getCarpeta(int n) {
        return carpetas.get(n);
    }
    

    public void ClearCarpeta(String carpeta) {
    	carpetas.remove(CarpetaControl.Buscar(carpeta, carpetas));
    }
    
    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }    

    @Override
    public String toString() {
        return "Proyecto: " + Nombre ;
    }  

}
