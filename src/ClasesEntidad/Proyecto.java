package ClasesEntidad;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import ClasesControl.CarpetaControl;

public class Proyecto {

	 	private String Nombre;
	    private ArrayList<Sesion> Sesiones = new ArrayList<Sesion>();
	    private ImageIcon icon;

	    public String getNombre() {
	        return Nombre;
	    }

	    public void setNombre(String Nombre) {
	        this.Nombre = Nombre;
	    }

	    public void addSesion(Sesion sesion){
	        Sesiones.add(sesion);
	    }

	    public ArrayList<Sesion> getSesiones() {
	        return Sesiones;
	    }
	    
	    public Sesion getSesion(int n) {
	    	if (n==-1) {
	    		return null;
	    	}else {
	        return Sesiones.get(n);}
	    }
	        
	    public void ClearSesion(String sesion) {
	    	Sesiones.remove(CarpetaControl.Buscar(sesion, Sesiones));
	    }

	    public ImageIcon getIcon() {
	        return icon;
	    }

	    public void setIcon(ImageIcon icon) {
	        this.icon = icon;
	    }    

	    @Override
	    public String toString() {
	        return "Proyecto: " + Nombre ;
	    }  
}
